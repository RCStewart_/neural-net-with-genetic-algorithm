#include "Engine/Language/Noun.hpp"
#include "Core2D.hpp"

ComponentRegistration Core2D::s_core2DComponentRegistration("CORE_2D_COMPONENT", &Core2D::createComponent);

//----------------------------------------------------------------------------------
Component* Core2D::createComponent(const std::string& name)
{
	return new Core2D();
}

//----------------------------------------------------------------------------------
Component* Core2D::createComponent(const std::string& name, TiXmlElement* componentData)
{
	return new Core2D(componentData);
}

//----------------------------------------------------------------------------------
Core2D::Core2D(TiXmlElement* componentData)
{
	m_name = "Core2D";
	m_position = Vec2(0.0, 0.0);
	m_size = Vec2(1.0, 1.0);
	m_rotation = Vec2(0.0, 0.0);
	m_color = RGBAColors(255, 255, 255, 255);
	m_lifeSpan = 0.0f;
	m_currentHealth = 100.0f;
	m_maxHealth = 100.0f;
	m_radius = 1.0f;

	const char* attrib = nullptr;

	//Position
	attrib = componentData->Attribute("POSITION_X");
	if (attrib != nullptr)
		m_position.x = std::stof(attrib);

	attrib = componentData->Attribute("POSITION_Y");
	if (attrib != nullptr)
		m_position.y = std::stof(attrib);

	//Size
	attrib = componentData->Attribute("SIZE_X");
	if (attrib != nullptr)
		m_size.x = std::stof(attrib);

	attrib = componentData->Attribute("SIZE_Y");
	if (attrib != nullptr)
		m_size.y = std::stof(attrib);

	//Rotation
	attrib = componentData->Attribute("ROTATION_X");
	if (attrib != nullptr)
		m_rotation.x = std::stof(attrib);

	attrib = componentData->Attribute("ROTATION_Y");
	if (attrib != nullptr)
		m_rotation.y = std::stof(attrib);

	//Color
	attrib = componentData->Attribute("COLOR_R");
	if (attrib != nullptr)
		m_color.m_red = std::stoi(attrib);

	attrib = componentData->Attribute("COLOR_G");
	if (attrib != nullptr)
		m_color.m_green = std::stoi(attrib);

	attrib = componentData->Attribute("COLOR_B");
	if (attrib != nullptr)
		m_color.m_blue = std::stoi(attrib);

	attrib = componentData->Attribute("COLOR_A");
	if (attrib != nullptr)
		m_color.m_alpha = std::stoi(attrib);

	//Life Span
	attrib = componentData->Attribute("LIFE_SPAN");
	if (attrib != nullptr)
		m_lifeSpan = std::stof(attrib);

	//Current Health
	attrib = componentData->Attribute("CURRENT_HEALTH");
	if (attrib != nullptr)
		m_currentHealth = std::stof(attrib);

	//Max Health
	attrib = componentData->Attribute("MAX_HEALTH");
	if (attrib != nullptr)
		m_maxHealth = std::stof(attrib);

	//Radius
	attrib = componentData->Attribute("RADIUS");
	if (attrib != nullptr)
		m_radius = std::stof(attrib);
}

//----------------------------------------------------------------------------------
Core2D::Core2D()
{
	m_name = "Core2D";
	m_position = Vec2(0.0, 0.0);
	m_size = Vec2(1.0,1.0);
	m_rotation = Vec2(0.0, 0.0);
	m_color = RGBAColors(255,255,255,255);
	m_lifeSpan = 0.0f;
	m_currentHealth = 100.0f;
	m_maxHealth = 100.0f;
	m_radius = .5f;
	m_referenceToOwner = nullptr;
}

//----------------------------------------------------------------------------------
Core2D::Core2D(Core2D& toCopy)
{
	m_name = "Core2D";
	m_position = toCopy.m_position;
	m_size = toCopy.m_size;
	m_rotation = toCopy.m_rotation;
	m_color = toCopy.m_color;
	m_lifeSpan = toCopy.m_lifeSpan;
	m_currentHealth = toCopy.m_currentHealth;
	m_maxHealth = toCopy.m_maxHealth;
	m_radius = toCopy.m_radius;
	m_referenceToOwner = toCopy.m_referenceToOwner;
}

//----------------------------------------------------------------------------------
Core2D::~Core2D()
{

}

//----------------------------------------------------------------------------------
void Core2D::placeAttributesIntoGameEntity(Noun* owner)
{
	//owner->m_attributes.set<Vec2>("POSITION", m_position);
	//owner->m_attributes.set<Vec2>("SIZE", m_size);
	owner->m_attributes.set<Vec2>("ROTATION", m_rotation);
	owner->m_attributes.set<RGBAColors>("COLOR", m_color);
	owner->m_attributes.set<float>("LIFE_SPAN", m_lifeSpan);
	owner->m_attributes.set<float>("CURRENT_HEALTH", m_currentHealth);
	owner->m_attributes.set<float>("MAX_HEALTH", m_maxHealth);
	//owner->m_attributes.set<float>("RADIUS", m_radius);

	m_referenceToOwner = owner;
}

//----------------------------------------------------------------------------------
Core2D* Core2D::getClone(AnyDataTypeMap& withTheseAttributeModifications)
{
	return new Core2D(*this);
}

//----------------------------------------------------------------------------------
Core2D* Core2D::getClone()
{
	return new Core2D(*this);
}
