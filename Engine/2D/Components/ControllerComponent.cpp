#include "ControllerComponent.hpp"
#include "Engine/Core/WindowsWrapper.hpp"

ComponentRegistration ControllerComponent::s_controllerComponentRegistration("CONTROLLER_COMPONENT", &ControllerComponent::createComponent);

//----------------------------------------------------------------------------------
Component* ControllerComponent::createComponent(const std::string& name, TiXmlElement* componentData)
{
	return new ControllerComponent(componentData);
}

//----------------------------------------------------------------------------------
Component* ControllerComponent::createComponent(const std::string& name)
{
	return new ControllerComponent();
}

//----------------------------------------------------------------------------------
ControllerComponent::ControllerComponent(ControllerComponent& toCopy)
{
	runLoop = false;
	m_referenceToOwner = nullptr;

	m_inputsThatCanTrigger = toCopy.m_inputsThatCanTrigger;
}

//----------------------------------------------------------------------------------
ControllerComponent::ControllerComponent(TiXmlElement* componentData)
{
	for (TiXmlElement* inputToAdd = componentData->FirstChildElement(); inputToAdd != NULL; inputToAdd = inputToAdd->NextSiblingElement())
	{
		AnyDataTypeMap verbAttributes;
		Verb* baseVerb;
		std::string nameOfBehavior = inputToAdd->Attribute("name");

		baseVerb = Verb::s_generatedVerbs[nameOfBehavior]->getClone();
		baseVerb->fillAnyDataMapWithAttributesFromXMLElement(inputToAdd, verbAttributes);

		verbAttributes.fillTargetMapWithDataFromSelf(baseVerb->m_attributes);

		std::string input = inputToAdd->Attribute("INPUT");

		m_inputsThatCanTrigger.push_back(InputTrigger(input[0], baseVerb));
	}

	runLoop = false;
}

//----------------------------------------------------------------------------------
ControllerComponent::ControllerComponent()
{
	m_name = "ControllerComponent";
	runLoop = true;
}

//----------------------------------------------------------------------------------
void ControllerComponent::init()
{
	runLoop = true;
}

//----------------------------------------------------------------------------------
ControllerComponent::~ControllerComponent()
{

}

//----------------------------------------------------------------------------------
void ControllerComponent::update(float delta)
{
	if (runLoop)
	{
		for (int i = 0; i < m_inputsThatCanTrigger.size(); ++i)
		{
			if (WindowWrapperIsInputActive(m_inputsThatCanTrigger[i].input))
			{
				std::vector<GameEntity2D*> testData;
				m_inputsThatCanTrigger[i].verbToCall->doThis(static_cast<GameEntity2D*>(m_referenceToOwner), testData);
			}
		}
	}
}

//----------------------------------------------------------------------------------
void ControllerComponent::placeAttributesIntoGameEntity(Noun* owner)
{
	m_referenceToOwner = owner;
}

//----------------------------------------------------------------------------------
void ControllerComponent::registerInput(int input, const std::string& nameOfVerb, AnyDataTypeMap& verbAttributes)
{
	Verb* toAdd = Verb::s_generatedVerbs[nameOfVerb]->getClone(verbAttributes);
	toAdd->setAttributes(verbAttributes);

	InputTrigger newInput = InputTrigger(input, toAdd);
	m_inputsThatCanTrigger.push_back(newInput);
}

//----------------------------------------------------------------------------------
ControllerComponent* ControllerComponent::getClone(AnyDataTypeMap& withTheseAttributeModifications)
{
	return new ControllerComponent(*this);
}


//----------------------------------------------------------------------------------
ControllerComponent* ControllerComponent::getClone()
{
	return new ControllerComponent(*this);
}
