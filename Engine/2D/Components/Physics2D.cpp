#include "Physics2D.hpp"
#include "../GameEntity2D.hpp"
#include "Core2D.hpp"
#include <cmath>
#include "Engine/Core/UtilityFunctions.hpp"
#include "Engine/Core/GameCommon.hpp"
#include <algorithm>
#include "Engine/Language/Noun.hpp"

#include <iostream>

std::vector<Physics2D*> Physics2D::s_dynamicPhysicsObjects;
std::vector<Physics2D*> Physics2D::s_staticPhysicsObjects;
std::deque<Physics2D*> Physics2D::s_queueOfObjectsThatCollided;

float Physics2D::s_windForce = 0.0f;
Vec2 Physics2D::s_windDirection = Vec2(0.0f, 0.0f);

ComponentRegistration Physics2D::s_physics2DComponentRegistration("PHYSICS_2D_COMPONENT", &Physics2D::createComponent);

//----------------------------------------------------------------------------------
Component* Physics2D::createComponent(const std::string& name, TiXmlElement* componentData)
{
	return new Physics2D(componentData);
}

//----------------------------------------------------------------------------------
Component* Physics2D::createComponent(const std::string& name)
{
	return new Physics2D();
}

//----------------------------------------------------------------------------------
Physics2D::Physics2D()
{
	m_name = "Physics2D";
}

//----------------------------------------------------------------------------------
Physics2D::Physics2D(TiXmlElement* componentData)
	:m_gravity(Vec2(0.0f, 9.83f))
{
	m_name = "Physics2D";
	m_touchedGroundLastFrame = false;
	m_touchedGroundThisFrame = false;
	m_isGroundedTo = nullptr;
	bool isAStaticObject = false;

	const char* attrib = nullptr;

	m_physicsVariables = new Physics2DMemberVariables(PHYSICS_2D_STATE_DYNAMIC, 1.0f, .5f, Vec2(0.0f, 0.0f), Vec2(0.0f, 0.0f));

	//Static or Dynamic
	attrib = componentData->Attribute("IS_A_STATIC_OBJECT");
	if (attrib != nullptr)
		isAStaticObject = std::stoi(attrib);

	//State
	attrib = componentData->Attribute("PHYSICS_STATE");
	if (attrib != nullptr)
		m_physicsVariables->m_state = static_cast<Physics2DState>(std::stoi(attrib));

	//Mass
	attrib = componentData->Attribute("MASS");
	if (attrib != nullptr)
		m_physicsVariables->m_mass = std::stof(attrib);

	//Bounciness
	attrib = componentData->Attribute("BOUNCINESS");
	if (attrib != nullptr)
		m_physicsVariables->m_bounciness = std::stof(attrib);

	//Force
	attrib = componentData->Attribute("FORCE_X");
	if (attrib != nullptr)
		m_physicsVariables->m_force.x = std::stof(attrib);

	attrib = componentData->Attribute("FORCE_Y");
	if (attrib != nullptr)
		m_physicsVariables->m_force.y = std::stof(attrib);

	//Velocity
	attrib = componentData->Attribute("VELOCITY_X");
	if (attrib != nullptr)
		m_physicsVariables->m_velocity.x = std::stof(attrib);

	attrib = componentData->Attribute("VELOCITY_Y");
	if (attrib != nullptr)
		m_physicsVariables->m_velocity.y = std::stof(attrib);

}

//----------------------------------------------------------------------------------
void Physics2D::init()
{
	if (m_physicsVariables->m_state == PHYSICS_2D_STATE_STATIC)
	{
		s_staticPhysicsObjects.push_back(this);
	}
	else
	{
		s_dynamicPhysicsObjects.push_back(this);
	}
}

//----------------------------------------------------------------------------------
Physics2D::Physics2D(Physics2DMemberVariables* initWith, bool isAStaticObject)
	:m_gravity(Vec2(0.0f, 9.83f))
{
	m_physicsVariables = initWith;
	m_touchedGroundLastFrame = false;
	m_touchedGroundThisFrame = false;
	m_isGroundedTo = nullptr;
	
	if (isAStaticObject)
	{
		s_staticPhysicsObjects.push_back(this);
	}
	else
	{
		s_dynamicPhysicsObjects.push_back(this);
	}
	
	m_name = "Physics2D";
}

//----------------------------------------------------------------------------------
Physics2D::~Physics2D()
{
	for (int i = 0; i < s_dynamicPhysicsObjects.size(); ++i)
	{
		if (s_dynamicPhysicsObjects[i]->m_owner->m_ID == m_owner->m_ID)
		{
			s_dynamicPhysicsObjects.erase(s_dynamicPhysicsObjects.begin() + i);
			return;
		}
	}

	for (int i = 0; i < s_staticPhysicsObjects.size(); ++i)
	{
		if (s_staticPhysicsObjects[i]->m_owner->m_ID == m_owner->m_ID)
		{
			s_staticPhysicsObjects.erase(s_staticPhysicsObjects.begin() + i);
			return;
		}
	}

	delete m_physicsVariables;
}

//----------------------------------------------------------------------------------
void Physics2D::update(float delta)
{

}

//Assume collisions are simple circles for now, add other types of collisions later
//----------------------------------------------------------------------------------
void Physics2D::detectCollisions(CollisionResult& out_result, const Vec2& startPos, const Vec2& endPos)
{
	bool collidedBelow = false;
	detectEntityBoundsCollisions(out_result, startPos, endPos, collidedBelow);
}


//----------------------------------------------------------------------------------
void Physics2D::detectEntityBoundsCollisions(CollisionResult& out_result, const Vec2& startPos, const Vec2& endPos, bool& out_collidedBelow)
{
	//Raycasting
	//----------------------------------------------------------------------------------
	Vec2 size		= m_owner->m_coreComponent->m_size;//m_owner->m_attributes.getRawData<Vec2>("SIZE");
	Vec2 pos		= m_owner->m_coreComponent->m_position;//m_owner->m_attributes.getRawData<Vec2>("POSITION");
	float radius	= m_owner->m_coreComponent->m_radius;//m_owner->m_attributes.getRawData<float>("RADIUS");

	m_touchedGroundThisFrame = false;

	//Valid numbers should only be positive
	float distanceActuallyCanTravel = -1.f;

	//Check every corner 
	Vec2 NW = Vec2(startPos.x, startPos.y + size.y);
	Vec2 NE = Vec2(startPos.x + size.x, startPos.y + size.y);
	Vec2 SW = Vec2(startPos.x, startPos.y);
	Vec2 SE = Vec2(startPos.x + size.x, startPos.y);
	
	std::vector<Vec2> cornerPos;

	cornerPos.push_back(NW);
	cornerPos.push_back(NE);
	cornerPos.push_back(SW);
	cornerPos.push_back(SE);

	Vec2 start = startPos;
	Vec2 distanceAndDirection = Vec2(endPos.x - startPos.x, endPos.y - startPos.y);
	float lengthX = abs(distanceAndDirection.x);
	float lengthY = abs(distanceAndDirection.y);

	std::vector<Physics2D*> closeObjects;
	gatherCloseObjects(closeObjects, cornerPos, distanceAndDirection.getMagnetude());

	RayTraceResult traceResult = RayTraceResult();
	rayTraceDistanceCanTravel(distanceAndDirection, pos, traceResult.sideCollidedWith, traceResult, closeObjects);

	//Get lowest X axis
	int indexOfLowestX = -1;
	if (traceResult.length.x < lengthX)
	{
		indexOfLowestX = 0;
	}

	//Get lowest Y axis
	int indexOfLowestY = -1;
	if (traceResult.length.y < lengthY)
	{
		indexOfLowestY = 0;
	}

	RayTraceResult actuallyUse = RayTraceResult();
	Vec2 finalDistance = distanceAndDirection;

	float sign = 1.0f;
	if (indexOfLowestX != -1)
	{
		if (finalDistance.x < 0.f)
			sign = -1.f;
		finalDistance.x = traceResult.length.x * sign;
	}

	sign = 1.0f;
	if (indexOfLowestY != -1)
	{
		if (finalDistance.y < 0.f)
			sign = -1.f;
		finalDistance.y = traceResult.length.y * sign;
	}
	actuallyUse = traceResult;

	float x = startPos.x + finalDistance.x +radius;
	float y = startPos.y + finalDistance.y +radius;
	{
		if (actuallyUse.xEntityCollidedWith && (indexOfLowestX != -1)
			&& actuallyUse.xEntityCollidedWith->m_owner->m_attributes.getRawData<Physics2DState>("PHYSICS_STATE") == PHYSICS_2D_STATE_DYNAMIC)
		{
			Physics2D* theirPhy = actuallyUse.xEntityCollidedWith;
			Noun* them = theirPhy->m_owner;
			Vec2 theirVel = them->m_attributes.getRawData<Vec2>("VELOCITY");
			float theirBounce = them->m_attributes.getRawData<float>("BOUNCE");
			float theirMass = them->m_attributes.getRawData<float>("MASS");

			float myMass = m_owner->m_attributes.getRawData<float>("MASS");
			Vec2 myVel = m_owner->m_attributes.getRawData<Vec2>("VELOCITY");
			float myBounce = m_owner->m_attributes.getRawData<float>("BOUNCE");

			Vec2 tempTheirVelocity = theirVel;
			Vec2 tempMyVelocity = myVel;

			float myMassRatio = 1.0f;
			float theirMassRatio = 1.0f;
			if (myMass <= theirMass)
			{
				myMassRatio = myMass / theirMass;
				theirMassRatio = (1.f - myMassRatio);
			}
			else
			{
				theirMassRatio = theirMass / myMass;
				myMassRatio = (1.0f - theirMassRatio);
			}

			if (abs(traceResult.sideCollidedWith.x - 1.0f) <= 0.001f)
			{
				myVel.x = (tempTheirVelocity.x * (theirMassRatio) - (tempMyVelocity.x * myMassRatio));
				myVel.x *= myBounce;

				theirVel.x = -(tempTheirVelocity.x * (theirMassRatio)) + (tempMyVelocity.x * (myMassRatio));
				theirVel.x *= theirBounce;

				m_owner->m_attributes.set<Vec2>("VELOCITY", myVel);
				them->m_attributes.set<Vec2>("VELOCITY", theirVel);
			}


		}
		else if (indexOfLowestX != -1)
		{
			Vec2 myVel = m_owner->m_attributes.getRawData<Vec2>("VELOCITY");
			float myBounce = m_owner->m_attributes.getRawData<float>("BOUNCE");
			myVel.x = -myVel.x * myBounce;
			m_owner->m_attributes.set<Vec2>("VELOCITY", myVel);
		}

		if (actuallyUse.yEntityCollidedWith && (indexOfLowestY != -1) 
			&& actuallyUse.yEntityCollidedWith->m_owner->m_attributes.getRawData<Physics2DState>("PHYSICS_STATE") == PHYSICS_2D_STATE_DYNAMIC)
		{
			Physics2D* theirPhy = actuallyUse.yEntityCollidedWith;
			Noun* them = theirPhy->m_owner;
			Vec2 theirVel = them->m_attributes.getRawData<Vec2>("VELOCITY");
			float theirBounce = them->m_attributes.getRawData<float>("BOUNCE");
			float theirMass = them->m_attributes.getRawData<float>("MASS");

			float myMass = m_owner->m_attributes.getRawData<float>("MASS");
			Vec2 myVel = m_owner->m_attributes.getRawData<Vec2>("VELOCITY");
			float myBounce = m_owner->m_attributes.getRawData<float>("BOUNCE");

			Vec2 tempTheirVelocity = theirVel;
			Vec2 tempMyVelocity = myVel;

			float myMassRatio = 1.0f;
			float theirMassRatio = 1.0f;
			if (myMass <= theirMass)
			{
				myMassRatio = myMass / theirMass;
				theirMassRatio = (1.f - myMassRatio);
			}
			else
			{
				theirMassRatio = theirMass / myMass;
				myMassRatio = (1.0f - theirMassRatio);
			}

			if (abs(traceResult.sideCollidedWith.y - 1.0f) <= 0.001f)
			{
				myVel.y = (tempTheirVelocity.y * (theirMassRatio) - (tempMyVelocity.y * (myMassRatio)));
				theirVel.y = -(tempTheirVelocity.y * (theirMassRatio)) + (tempMyVelocity.y * (myMassRatio));

				myVel.y *= myBounce;
				theirVel.y *= theirBounce;

				m_owner->m_attributes.set<Vec2>("VELOCITY", myVel);
				them->m_attributes.set<Vec2>("VELOCITY", theirVel);
			}

			//float theirY = them->m_attributes.getRawData<Vec2>("POSITION").y;
			//float theirRadius = them->m_attributes.getRawData<float>("RADIUS");

		}
		else if (indexOfLowestY != -1)
		{
			Vec2 myVel = m_owner->m_attributes.getRawData<Vec2>("VELOCITY");
			float myBounce = m_owner->m_attributes.getRawData<float>("BOUNCE");
			myVel.y = -myVel.y * myBounce;
			m_owner->m_attributes.set<Vec2>("VELOCITY", myVel);
		}
	}

	x -= radius;
	y -= radius;
	m_owner->m_coreComponent->m_position = Vec2(x, y);//m_owner->m_attributes.set<Vec2>("POSITION", Vec2(x, y));

}

//----------------------------------------------------------------------------------
void Physics2D::updatePhysics(float delta, CollisionResult& out_result, float percentDistanceToTravel)
{
	m_touchedGroundLastFrame = m_touchedGroundThisFrame;
	Vec2 nextPos;
	Vec2 nextVelocity;

	Vec2 force = m_owner->m_attributes.getRawData<Vec2>("FORCE");
	Vec2 velocity = m_owner->m_attributes.getRawData<Vec2>("VELOCITY");
	float mass = m_owner->m_attributes.getRawData<float>("MASS");

	float windCoefficiant = s_windForce;

	if (m_touchedGroundLastFrame && m_touchedGroundThisFrame)
	{

		force.x += (-windCoefficiant * (velocity.x - s_windDirection.x));
		force.y = (-windCoefficiant *  (velocity.y - s_windDirection.y));
	}
	else
	{
		force.x += (-windCoefficiant * (velocity.x - s_windDirection.x) - mass * m_gravity.x);
		force.y += (-windCoefficiant * (velocity.y - s_windDirection.y) - mass * m_gravity.y);
	}


	Vec2 pos = m_owner->m_coreComponent->m_position;// m_owner->m_attributes.getRawData<Vec2>("POSITION");
	nextPos.x = pos.x + velocity.x * delta * percentDistanceToTravel;
	nextPos.y = pos.y + velocity.y * delta * percentDistanceToTravel;

	Vec2 distanceToTravel = Vec2(pos.x - nextPos.x, pos.y - nextPos.y);
	float distance = distanceToTravel.getMagnetude();

	detectCollisions(out_result, pos, nextPos);

	velocity = m_owner->m_attributes.getRawData<Vec2>("VELOCITY");

	nextVelocity.x = velocity.x + (force.x / mass) * delta * percentDistanceToTravel;
	nextVelocity.y = velocity.y + (force.y / mass) * delta * percentDistanceToTravel;

	force = Vec2(0.f, 0.f);
	//SWAP

	if (nextVelocity.getMagnetude() < .2f)
	{
		nextVelocity = Vec2(0.0f, 0.0f);
	}

	velocity = nextVelocity;

	if (isGrounded())
	{
		velocity.x *= .9f;
	}

	m_owner->m_attributes.set<Vec2>("VELOCITY", velocity);
	m_owner->m_attributes.set<Vec2>("FORCE", force);
}

//----------------------------------------------------------------------------------
void Physics2D::sUpdateAllDynamicPhysics(float delta)
{
	for (int i = 0; i < s_dynamicPhysicsObjects.size(); ++i)
	{
		s_dynamicPhysicsObjects[i]->m_percentOfDistanceToTravel = 1.0f;
		s_dynamicPhysicsObjects[i]->m_thoseWhoCollidedThisTurn = std::vector<Physics2D*>();
	}

	for (int i = 0; i < s_dynamicPhysicsObjects.size(); ++i)
	{
		CollisionResult out_result;
		out_result.hasCollided = false;

		s_dynamicPhysicsObjects[i]->updatePhysics(delta, out_result, 1.0f);
	}
}

//----------------------------------------------------------------------------------------------
void Physics2D::rayTraceDistanceCanTravel(Vec2& distanceAndDirection, Vec2& startPosition, Vec2& sideCollidedWith, RayTraceResult& out_result, const std::vector<Physics2D*>& listOfCloseOjbects)
{
	float distanceToTravel = distanceAndDirection.getMagnetude();
	float distanceSquared = distanceToTravel * distanceToTravel;
	const float invalidNum = numeric_limits<float>::infinity();

	int xIteration = 0;
	int yIteration = 0;

	Vec2 size = m_owner->m_coreComponent->m_size;// m_owner->m_attributes.getRawData<Vec2>("SIZE");

	//Initialize
	Vec2 blockPosition = Vec2(0.0f, 0.0f);
	IntVec2 chunkPosition = IntVec2(0, 0);

	Vec2 normalizedDirection = distanceAndDirection;
	normalizedDirection.normalize();

	Vec2 position = startPosition;

	Vec2 basePosition = position;

	float stepSize = .01f;
	int iterationMin = 10;

	Vec2 step = Vec2(stepSize, stepSize);
	if (normalizedDirection.x < 0)
	{
		step.x = -stepSize;
	}
	if (normalizedDirection.y < 0)
	{
		step.y = -stepSize;
	}

	Vec2 tMax = Vec2(0.0f, 0.0f);

	tMax.x = abs(step.x / normalizedDirection.x);
	tMax.y = abs(step.y / normalizedDirection.y);

	Vec2 tDelta = Vec2(0.0f, 0.0f);

	tDelta.x = abs(step.x / normalizedDirection.x);
	tDelta.y = abs(step.y / normalizedDirection.y);

	if (normalizedDirection.x == 0)
	{
		tMax.x = invalidNum;
		tDelta.x = invalidNum;
	}
	if (normalizedDirection.y == 0)
	{
		tMax.y = invalidNum;
		tDelta.y = invalidNum;
	}

	Vec2 lengthVector = Vec2(0.0f, 0.0f);

	int iteration = 0;

	sideCollidedWith = Vec2(0.0f, 0.0f);
	out_result.length.y = abs(distanceAndDirection.y);
	out_result.length.x = abs(distanceAndDirection.x);

	bool stoppedOnXAxis = false;
	bool stoppedOnYAxis = false;
	int numberOfAxisStored = 0;
	while (lengthVector.x * lengthVector.x + lengthVector.y * lengthVector.y <= distanceSquared || distanceSquared <= .001f)
	{
		lengthVector = basePosition;

		Vec2 axis = Vec2(0, 0);

		if ((tMax.x < tMax.y) && !stoppedOnXAxis)
		{
			++xIteration;
			position.x = position.x + step.x;
			tMax.x = tMax.x + tDelta.x;
			axis.x = 1;
		}
		else if (!stoppedOnYAxis)
		{
			++yIteration;
			position.y = position.y + step.y;
			tMax.y = tMax.y + tDelta.y;
			axis.y = 1;
		}
		else
		{
			return;
		}

		lengthVector.x = abs(abs(lengthVector.x) - abs(position.x));
		lengthVector.y = abs(abs(lengthVector.y) - abs(position.y));

		int boundsReached = 0;
		//Check out of bounds
		if (((position.x + size.x >= g_numOfTileUnitsX && distanceAndDirection.x > 0.0f) || (position.x <= 0 && distanceAndDirection.x < 0.0f)) && !stoppedOnXAxis)
		{	
			sideCollidedWith.x = 1.0f;
			++numberOfAxisStored;
			out_result.length.x = abs(lengthVector.x + step.x);

			if (xIteration <= iterationMin)
			{
				out_result.length.x = 0.0f;
			}

			position.x = out_result.length.x;
			stoppedOnXAxis = true;
			out_result.xEntityCollidedWith = nullptr;
			++boundsReached;

			Vec2 newDistance = distanceAndDirection;
			newDistance.x = out_result.length.x;
			if (distanceAndDirection.x != 0.0f)
				newDistance.x *= (distanceAndDirection.x / abs(distanceAndDirection.x));
			distanceToTravel = newDistance.getMagnetude();
			distanceSquared = distanceToTravel * distanceToTravel;
		}

		if (((position.y + size.y >= g_numOfTileUnitsY && distanceAndDirection.y >= 0.0f) || (position.y <= 0 && distanceAndDirection.y < 0.0f)) && !stoppedOnYAxis)
		{	
			sideCollidedWith.y = 1.0f;
			++numberOfAxisStored;
			out_result.length.y = abs(lengthVector.y + step.y);

			if (yIteration <= iterationMin)
			{
				out_result.length.y = 0.0f;
			}

			position.y = out_result.length.y;
			out_result.yEntityCollidedWith = nullptr;
			stoppedOnYAxis = true;
			++boundsReached;

			Vec2 newDistance = distanceAndDirection;
			newDistance.y = out_result.length.y;

			if (distanceAndDirection.y != 0.0f)
				newDistance.y *= (distanceAndDirection.y / abs(distanceAndDirection.y));

			distanceToTravel = newDistance.getMagnetude();
			distanceSquared = distanceToTravel * distanceToTravel;
		}

		if (boundsReached == 2)
		{
			return;
		}

		if (isThisCollidingWithAnyObjectInList(listOfCloseOjbects, out_result, position, distanceAndDirection))
		{
			++numberOfAxisStored;

			if ((abs(axis.x - 1.0f) <= 0.01f) && !stoppedOnXAxis)
			{
				float distance = abs(lengthVector.x + step.x);

				if (xIteration <= iterationMin)
				{
					distance = 0.0f;
				}

				{
					out_result.length.x =  distance;
					out_result.xEntityCollidedWith = out_result.lastEntityCollidedWith;
					position.x = out_result.length.x;
				}

				sideCollidedWith.x = 1.0f;
				stoppedOnXAxis = true;

				Vec2 newDistance = distanceAndDirection;
				newDistance.x = distance;

				if (distanceAndDirection.x != 0.0f)
					newDistance.x *= (distanceAndDirection.x / abs(distanceAndDirection.x));

				distanceToTravel =  newDistance.getMagnetude();
				distanceSquared = distanceToTravel * distanceToTravel;
			}

			if ((abs(axis.y - 1.0f) <= 0.01f) && !stoppedOnYAxis)
			{
				float distance = abs(lengthVector.y + step.y);

				if (yIteration <= iterationMin)
				{
					distance = 0.0f;
				}

				{
					out_result.length.y = distance;
					out_result.yEntityCollidedWith = out_result.lastEntityCollidedWith;
					position.y = out_result.length.y;
				}
				sideCollidedWith.y = 1.0f;
				stoppedOnYAxis = true;

				Vec2 newDistance = distanceAndDirection;
				newDistance.y = distance;

				if (distanceAndDirection.x != 0.0f)
					newDistance.y *= (distanceAndDirection.y / abs(distanceAndDirection.y));

				distanceToTravel = newDistance.getMagnetude();
				distanceSquared = distanceToTravel * distanceToTravel;
			}
		}

		++iteration;
	}

	return;
}

//----------------------------------------------------------------------------------
bool Physics2D::isPointInAnyObjectAndGatherObjects(Vec2& pointToCheck, RayTraceResult& out_traceResult, const std::vector<Physics2D*>& listOfCloseOjbects, Vec2& distanceAndDirection)
{
	bool toReturn = false;
	for (int closeIndex = 0; closeIndex < listOfCloseOjbects.size(); ++closeIndex)
	{
		Noun* them = listOfCloseOjbects[closeIndex]->m_owner;
		Vec2 pos  = them->m_coreComponent->m_position;//them->m_attributes.getRawData<Vec2>("POSITION");
		Vec2 size = them->m_coreComponent->m_size;//them->m_attributes.getRawData<Vec2>("SIZE");

		if (them->m_ID == m_owner->m_ID)
		{
			continue;
		}

		if ((pointToCheck.x <= pos.x + size.x) && (pointToCheck.x >= pos.x) && (pointToCheck.y <= pos.y + size.y) && (pointToCheck.y >= pos.y))
		{
			out_traceResult.lastEntityCollidedWith = listOfCloseOjbects[closeIndex];
			toReturn = true;
			return toReturn;
		}
	}
	return toReturn;
}

//----------------------------------------------------------------------------------
void Physics2D::gatherCloseObjects(std::vector<Physics2D*>& out_closeObjects, std::vector<Vec2>& corners, float distance)
{
	float testDistance = max(distance, .5f);
	for (int dynIndex = 0; dynIndex < s_dynamicPhysicsObjects.size(); ++dynIndex)
	{
		Noun* them = s_dynamicPhysicsObjects[dynIndex]->m_owner;
		Vec2 pos = them->m_coreComponent->m_position;//them->m_attributes.getRawData<Vec2>("POSITION");
		Vec2 size = them->m_coreComponent->m_size;//them->m_attributes.getRawData<Vec2>("SIZE");

		if (them->m_ID == m_owner->m_ID)
		{
			continue;
		}

		Vec2 NW = Vec2(pos.x, pos.y + size.y);
		Vec2 NE = Vec2(pos.x + size.x, pos.y + size.y);
		Vec2 SW = Vec2(pos.x, pos.y);
		Vec2 SE = Vec2(pos.x + size.x, pos.y);

		std::vector<Vec2> cornerPos;
		cornerPos.push_back(NW);
		cornerPos.push_back(NE);
		cornerPos.push_back(SW);
		cornerPos.push_back(SE);

		for (int themIndex = 0; themIndex < cornerPos.size(); ++themIndex)
		{
			for (int thisIndex = 0; thisIndex < corners.size(); ++thisIndex)
			{
				Vec2 distanceCheck = Vec2(cornerPos[themIndex].x - corners[thisIndex].x, cornerPos[themIndex].y - corners[thisIndex].y);

				if (testDistance >= distanceCheck.getMagnetude())
				{
					out_closeObjects.push_back(s_dynamicPhysicsObjects[dynIndex]);
					break;
				}
			}
		}
		
	}

	for (int staticIndex = 0; staticIndex < s_staticPhysicsObjects.size(); ++staticIndex)
	{
		Noun* them = s_staticPhysicsObjects[staticIndex]->m_owner;
		Vec2 pos =  them->m_coreComponent->m_position;//them->m_attributes.getRawData<Vec2>("POSITION");
		Vec2 size = them->m_coreComponent->m_size;//them->m_attributes.getRawData<Vec2>("SIZE");

		if (them->m_ID == m_owner->m_ID)
		{
			continue;
		}

		Vec2 NW = Vec2(pos.x, pos.y + size.y);
		Vec2 NE = Vec2(pos.x + size.x, pos.y + size.y);
		Vec2 SW = Vec2(pos.x, pos.y);
		Vec2 SE = Vec2(pos.x + size.x, pos.y);

		std::vector<Vec2> cornerPos;
		cornerPos.push_back(NW);
		cornerPos.push_back(NE);
		cornerPos.push_back(SW);
		cornerPos.push_back(SE);

		for (int themIndex = 0; themIndex < cornerPos.size(); ++themIndex)
		{
			for (int thisIndex = 0; thisIndex < corners.size(); ++thisIndex)
			{
				Vec2 distanceCheck = Vec2(cornerPos[themIndex].x - corners[thisIndex].x, cornerPos[themIndex].y - corners[thisIndex].y);

				if (testDistance >= distanceCheck.getMagnetude())
				{
					out_closeObjects.push_back(s_staticPhysicsObjects[staticIndex]);
					break;
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------
bool Physics2D::isThisCollidingWithObject(Physics2D* them, const Vec2& positionToCheck, Vec2& distanceAndDirection)
{
	Noun* themNoun = them->m_owner;
	Vec2 pos =  themNoun->m_coreComponent->m_position;//themNoun->m_attributes.getRawData<Vec2>("POSITION");
	Vec2 size = themNoun->m_coreComponent->m_size;//themNoun->m_attributes.getRawData<Vec2>("SIZE");

	Vec2 NW = Vec2(pos.x, pos.y + size.y);
	Vec2 NE = Vec2(pos.x + size.x, pos.y + size.y);
	Vec2 SW = Vec2(pos.x, pos.y);
	Vec2 SE = Vec2(pos.x + size.x, pos.y);

	Vec2 thisPos = positionToCheck;
	Vec2 thisSize = m_owner->m_coreComponent->m_size;//m_owner->m_attributes.getRawData<Vec2>("SIZE");

	//Direction Check
	Vec2 directionOfThem = Vec2(pos.x - thisPos.x, pos.y - thisPos.y);
	Vec2 normalizedDD = distanceAndDirection;
	directionOfThem.normalize();
	normalizedDD.normalize();
	if (Vec2::dotProduct(directionOfThem, normalizedDD) < 0.25f)
	{
		return false;
	}

	//SW
	if ((thisPos.x >= pos.x) && (thisPos.x <= pos.x + size.x) && (thisPos.y >= pos.y) && (thisPos.y <= pos.y + size.y))
	{
		return true;
	}

	//SE
	if ((thisPos.x + thisSize.x >= pos.x) && (thisPos.x + thisSize.x <= pos.x + size.x) && (thisPos.y >= pos.y) && (thisPos.y <= pos.y + size.y))
	{
		return true;
	}

	//NE
	if ((thisPos.x + thisSize.x >= pos.x) && (thisPos.x + thisSize.x <= pos.x + size.x) && (thisPos.y + thisSize.y >= pos.y) && (thisPos.y + thisSize.y <= pos.y + size.y))
	{
		return true;
	}

	//NW
	if ((thisPos.x >= pos.x) && (thisPos.x <= pos.x + size.x) && (thisPos.y + thisSize.y >= pos.y) && (thisPos.y + thisSize.y <= pos.y + size.y))
	{
		return true;
	}

	return false;
}

//----------------------------------------------------------------------------------
bool Physics2D::isThisCollidingWithAnyObjectInList(const std::vector<Physics2D*>& closeObjects, RayTraceResult& out_traceResult, const Vec2& positionToCheck, Vec2& distanceAndDirection)
{
	for (int i = 0; i < closeObjects.size(); ++i)
	{
		if (closeObjects[i]->m_owner->m_ID == m_owner->m_ID)
		{
			continue;
		}

		if (isThisCollidingWithObject(closeObjects[i], positionToCheck, distanceAndDirection))
		{
			out_traceResult.lastEntityCollidedWith = closeObjects[i];
			return true;
		}
	}
	return false;
}

//----------------------------------------------------------------------------------
bool Physics2D::isGrounded()
{
	Vec2 size		= m_owner->m_coreComponent->m_size;//m_owner->m_attributes.getRawData<Vec2>("SIZE");
	Vec2 pos		= m_owner->m_coreComponent->m_position;//m_owner->m_attributes.getRawData<Vec2>("POSITION");
	float radius	= m_owner->m_coreComponent->m_radius;//m_owner->m_attributes.getRawData<float>("RADIUS");
	m_touchedGroundThisFrame = false;

	//Valid numbers should only be positive
	float distanceActuallyCanTravel = -1.f;

	//Check every corner 
	Vec2 NW = Vec2(pos.x, pos.y + size.y);
	Vec2 NE = Vec2(pos.x + size.x, pos.y + size.y);
	Vec2 SW = Vec2(pos.x, pos.y);
	Vec2 SE = Vec2(pos.x + size.x, pos.y);

	std::vector<Vec2> cornerPos;

	cornerPos.push_back(NW);
	cornerPos.push_back(NE);
	cornerPos.push_back(SW);
	cornerPos.push_back(SE);

	Vec2 start = pos;
	Vec2 distanceAndDirection = Vec2(0.0f, -.1f);

	std::vector<Physics2D*> closeObjects;
	gatherCloseObjects(closeObjects, cornerPos, distanceAndDirection.getMagnetude());

	RayTraceResult traceResult = RayTraceResult();
	rayTraceDistanceCanTravel(distanceAndDirection, pos, traceResult.sideCollidedWith, traceResult, closeObjects);

	if (abs(traceResult.length.y) <= .001)
	{
		return true;
	}
	return false;
}

//----------------------------------------------------------------------------------
void Physics2D::placeAttributesIntoGameEntity(Noun* owner)
{
	m_owner = owner;
	owner->m_attributes.set<Physics2DState>("PHYSICS_STATE", m_physicsVariables->m_state);
	owner->m_attributes.set<float>("MASS", m_physicsVariables->m_mass);
	owner->m_attributes.set<float>("BOUNCE", m_physicsVariables->m_bounciness);

	owner->m_attributes.set<Vec2>("FORCE", m_physicsVariables->m_force);
	owner->m_attributes.set<Vec2>("VELOCITY", m_physicsVariables->m_velocity);
}

//----------------------------------------------------------------------------------
Physics2D* Physics2D::getClone(AnyDataTypeMap& withTheseAttributeModifications)
{
	return new Physics2D(*this);
}

//----------------------------------------------------------------------------------
Physics2D* Physics2D::getClone()
{
	return new Physics2D(*this);
}