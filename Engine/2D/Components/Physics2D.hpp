#pragma once
#include <map>
#include "Engine/Math/Vec2.hpp"
#include <vector>
#include "Component.hpp"
#include <deque>
#include <unordered_set>

class Noun;
class Core2D;
class Physics2D;

//----------------------------------------------------------------------------------
enum Physics2DState
{
	PHYSICS_2D_STATE_DYNAMIC = 0,
	PHYSICS_2D_STATE_STATIC = 1
};

//----------------------------------------------------------------------------------
struct CollisionResult
{
	bool hasCollided;
	std::vector<Noun*> entitiesHit;
	std::vector<Vec2> collisionLocations;
};

//----------------------------------------------------------------------------------
struct RayTraceResult
{
	RayTraceResult(){
		length = Vec2(-1.f, -1.f);
		sideCollidedWith = Vec2(0.0f, 0.0f);
		lastEntityCollidedWith = nullptr;
		xEntityCollidedWith = nullptr;
		yEntityCollidedWith = nullptr;
	};
	Physics2D* lastEntityCollidedWith;
	Physics2D* xEntityCollidedWith;
	Physics2D* yEntityCollidedWith;
	Vec2 length;
	Vec2 sideCollidedWith;
};

//----------------------------------------------------------------------------------
struct Physics2DMemberVariables
{
	Physics2DMemberVariables(Physics2DState state, float mass, float bounciness, const Vec2& force, const Vec2& velocity)
		:m_state(state),
		m_mass(mass),
		m_bounciness(bounciness),
		m_force(force),
		m_velocity(velocity)
	{
	};

	Physics2DState m_state;

	float m_mass;
	float m_bounciness;

	Vec2 m_force;
	Vec2 m_velocity;
};

//----------------------------------------------------------------------------------
class Physics2D : public Component
{
public:
	Physics2D(TiXmlElement* componentData);
	Physics2D();
	Physics2D(Physics2DMemberVariables* initPhysicsVariables, bool isAStaticObject);
	~Physics2D();

	virtual void update(float delta);
	virtual void placeAttributesIntoGameEntity(Noun* owner);

	void updatePhysics(float delta, CollisionResult& out_result, float percentDistanceToTravel);

	//----------------------------------------------------------------------------------
	void detectCollisions(CollisionResult& out_result, const Vec2& startPos, const Vec2& endPos);
	void detectEntityBoundsCollisions(CollisionResult& out_result, const Vec2& startPos, const Vec2& endPos, bool& out_collidedBelow);

	//----------------------------------------------------------------------------------
	void rayTraceDistanceCanTravel(Vec2& distanceAndDirection, Vec2& startPosition, Vec2& sideCollidedWith, RayTraceResult& out_result, const std::vector<Physics2D*>& listOfCloseOjbects);
	bool isPointInAnyObjectAndGatherObjects(Vec2& pointToCheck, RayTraceResult& out_traceResult, const std::vector<Physics2D*>& listOfCloseOjbects, Vec2& distanceAndDirection);
	void movePointOutsideOfAllOtherColliders(Vec2& pointToCheck, Vec2& pointOfOrigin);
	void gatherCloseObjects(std::vector<Physics2D*>& out_closeObjects, std::vector<Vec2>& corners, float distance);

	static void sUpdateAllDynamicPhysics(float delta);

	bool isThisCollidingWithObject(Physics2D* them, const Vec2& positionToCheck, Vec2& distanceAndDirection);
	bool isThisCollidingWithAnyObjectInList(const std::vector<Physics2D*>& closeObjects, RayTraceResult& out_traceResult, const Vec2& positionToCheck, Vec2& distanceAndDirection);

	bool isGrounded();
	
	virtual void init();

	virtual Physics2D* getClone(AnyDataTypeMap& withTheseAttributeModifications);
	virtual Physics2D* getClone();

	static Component* createComponent(const std::string& name);
	virtual Component* createComponent(const std::string& name, TiXmlElement* componentData);

	Noun* m_owner;
	Noun* m_isGroundedTo;

	Physics2DMemberVariables* m_physicsVariables;

	float m_percentOfDistanceToTravel;

	bool m_touchedGroundLastFrame;
	bool m_touchedGroundThisFrame;

	std::vector<Physics2D*> m_thoseWhoCollidedThisTurn;

	Vec2 m_gravity;

	static float s_windForce;
	static Vec2 s_windDirection;

protected:
	static ComponentRegistration s_physics2DComponentRegistration;

private:
	static std::vector<Physics2D*> s_dynamicPhysicsObjects;
	static std::vector<Physics2D*> s_staticPhysicsObjects;
	static std::deque<Physics2D*> s_queueOfObjectsThatCollided;
};