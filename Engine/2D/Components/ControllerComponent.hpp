#pragma once
#include "Component.hpp"
#include "Engine/Math/IntVec2.hpp"
#include <string>
#include <vector>
#include "Engine/Language/Verb.hpp"
#include "Engine/Language/Noun.hpp"

//----------------------------------------------------------------------------------
struct InputTrigger
{
	InputTrigger(int inputToActivate, Verb* verb)
		: input(inputToActivate)
		, verbToCall(verb)
	{

	}

	int input;
	Verb* verbToCall;
};

//----------------------------------------------------------------------------------
class ControllerComponent : public Component
{
public:
	ControllerComponent(ControllerComponent& toCopy);
	ControllerComponent(TiXmlElement* verb);
	ControllerComponent();
	virtual ~ControllerComponent();

	virtual void update(float delta);
	virtual void placeAttributesIntoGameEntity(Noun* owner);

	void registerInput(int input, const std::string& nameOfVerb, AnyDataTypeMap& verbAttributes);

	virtual ControllerComponent* getClone(AnyDataTypeMap& withTheseAttributeModifications);
	virtual ControllerComponent* getClone();
	static Component* createComponent(const std::string& name);
	virtual Component* createComponent(const std::string& name, TiXmlElement* verb);

	virtual void init();

	bool  runLoop;
	Noun* m_referenceToOwner;
	std::vector<InputTrigger> m_inputsThatCanTrigger;
protected:
	static ComponentRegistration s_controllerComponentRegistration;
};