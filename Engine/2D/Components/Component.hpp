#pragma once
#include "Engine/Core/AnyDataTypeMap.hpp"
#include "Engine/Core/AnyDataType.hpp"
#include <string>
#include <map>
#include "Engine/Libraries/TinyXML/tinystr.h"
#include "Engine/Libraries/TinyXML/tinyxml.h"

class Noun;

//----------------------------------------------------------------------------------
class ComponentRegistration;
class Component;

typedef Component* (ComponentRegistrationFunc)(const std::string& name);
typedef std::map<std::string, ComponentRegistration*> ComponentRegistrationMap;
typedef std::map<std::string, Component*> ComponentMap;

//----------------------------------------------------------------------------------
class ComponentRegistration
{
public:
	ComponentRegistration(const std::string& name, ComponentRegistrationFunc* registrationFunc)
		:m_name(name)
		, m_registrationFunc(registrationFunc)
	{
		if (!s_componentRegistration)
		{
			s_componentRegistration = new ComponentRegistrationMap();
		}
		(*s_componentRegistration)[name] = this;
	};

	static ComponentRegistrationMap* getComponentRegistrations()
	{
		return s_componentRegistration;
	};

	std::string getName()
	{
		return m_name;
	};

	Component* createComponent();

protected:
	std::string m_name;
	ComponentRegistrationFunc* m_registrationFunc;
	static ComponentRegistrationMap* s_componentRegistration;

};

//----------------------------------------------------------------------------------
class Component
{
public:
	Component();
	virtual ~Component();

	virtual void update(float delta);
	virtual void placeAttributesIntoGameEntity(Noun* owner);

	virtual Component* getClone(AnyDataTypeMap& withTheseAttributeModifications){ return nullptr; };
	virtual Component* getClone(){ return nullptr; };

	virtual void init(){};

	virtual Component* createComponent(const std::string& name, TiXmlElement* verb);
	static Component* createComponent(const std::string& name);

	static int createComponents();

	short m_priority;
	std::string m_name;

	static ComponentMap s_generatedComponents;
protected:
	Component(AnyDataTypeMap& startingAttributes);

	Component* clone(AnyDataTypeMap& withTheseAttributeModifications){ return getClone(withTheseAttributeModifications); };
	Component* clone(){ return getClone(); };
};