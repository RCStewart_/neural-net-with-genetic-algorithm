#pragma once
#include "Component.hpp"
#include "Engine/Math/IntVec2.hpp"
#include <string>
#include <vector>
#include "Engine/Language/Verb.hpp"
#include "Engine/Language/Noun.hpp"
#include "Engine\Time\UtilitiesTime.hpp"

//----------------------------------------------------------------------------------
struct TimedTrigger
{
	TimedTrigger(int timesToTrigger, Verb* verb, double timeDelay)
		: m_timesToTrigger(timesToTrigger)
		, m_verbToCall(verb)
		, m_timeDelay(timeDelay)
	{
		m_timeOfLastTrigger = GetAbsoluteTimeSeconds();
		m_timeAddedSoFar = 0.0;
	}

	//----------------------------------------------------------------------------------
	void update(double delta, Noun* theActor)
	{
		if (m_timeAddedSoFar + delta < m_timeDelay)
		{
			m_timeAddedSoFar += delta;
		}
		else
		{
			
			m_timeAddedSoFar = delta + m_timeAddedSoFar - m_timeDelay;
			m_timeOfLastTrigger = GetAbsoluteTimeSeconds();
			if (m_timesToTrigger > 0)
			{
				--m_timesToTrigger;
				std::vector <GameEntity2D*> test;
				m_verbToCall->doThis(theActor, test);
			}
			else if (m_timesToTrigger == -1)
			{
				std::vector <GameEntity2D*> test;
				m_verbToCall->doThis(theActor, test);
			}
		}
	};

	int m_timesToTrigger;
	double m_timeDelay;
	double m_timeOfLastTrigger;
	double m_timeAddedSoFar;
	Verb* m_verbToCall;
};

//----------------------------------------------------------------------------------
class TimeComponent : public Component
{
public:
	TimeComponent(TimeComponent& toCopy);
	TimeComponent();
	TimeComponent(TiXmlElement* componentData);

	virtual ~TimeComponent();

	virtual void update(float delta);
	virtual void placeAttributesIntoGameEntity(Noun* owner);
	virtual void init();

	void registerTimer(int numOfRepeats, double timeDelay, const std::string& nameOfVerb, AnyDataTypeMap& verbAttributes);
	
	virtual TimeComponent* getClone(AnyDataTypeMap& withTheseAttributeModifications);
	virtual TimeComponent* getClone();
	static  Component* createComponent(const std::string& name);
	virtual Component* createComponent(const std::string& name, TiXmlElement* componentData);

	bool initialized;
	Noun* m_referenceToOwner;
	std::vector<TimedTrigger> m_timers;

protected:
	static ComponentRegistration s_timeComponentRegistration;
};