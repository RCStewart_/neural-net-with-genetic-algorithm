#pragma once
#include <string>
#include "Engine\Core\RGBAColors.hpp"
#include <vector>
#include <map>

class Vec2;
class FontGenerator;

using namespace std;
struct ConsoleCommandArgs
{
	string m_rawArgs;
	vector<string> m_args;
};

typedef void (CommandFunc)(const ConsoleCommandArgs& args);

struct RegisteredCommand
{
	CommandFunc* m_f;
	string m_description;
	string m_name;
};

struct ConsoleLine
{
	string m_text;
	RGBAColors m_textColor;
};

class ConsoleLog
{
public:
	ConsoleLog( void );
	ConsoleLog( FontGenerator* font, float cellHeight );
	~ConsoleLog( void );

	static void addStringToLog( const string& toAdd, RGBAColors& color);
	static void renderLog();
	static void registerCommand( const string& name, CommandFunc* f, const string& description);
	static void initializeConsole( FontGenerator* font, float cellHeight );
	static void executeCommand( const string& toExecute );
	static void getInputs();
	static void setMouseCursor( Vec2& newPos );

	static void test();

	static void help(const ConsoleCommandArgs& args);
	static void clear(const ConsoleCommandArgs& args);
	static void quit(const ConsoleCommandArgs& args);
	//static void toggleTextShadow(const ConsoleCommandArgs& args);

	static void drawBlinkingCursor();

	static FontGenerator* s_font;
	static vector<ConsoleLine> s_log;
	static map<string, RegisteredCommand*> s_registeredCommands;

	static string s_activeInput;
	static int s_indexOfActive;
	static float s_cellHeight;
	static bool s_isLogActive;
	//static bool s_isTextShadowOn;
};

