#include "FontGenerator.hpp"

#include "Engine\Core\Texture.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Math\Vec3.hpp"
//#include "GameCommon.hpp"


//----------------------------------------------------------------------------------
FontGenerator::FontGenerator(void)
{
}


//----------------------------------------------------------------------------------
FontGenerator::~FontGenerator(void)
{
}


//----------------------------------------------------------------------------------
int FontGenerator::loadFontInformationFromXML(string& xmlFileName, string& imageFileName)
{
	TiXmlDocument doc;
	
	if(!doc.LoadFile(xmlFileName.c_str()))
	{
		return 0;
	}

	TiXmlElement* root = doc.FirstChildElement();

	if(root == NULL)
	{
		doc.Clear();
		return 0;
	}

	Texture* fontTexture = Texture::CreateOrGetTexture(imageFileName);
	m_fontTextures.push_back( fontTexture );

	map<int, GlyphMetaData*> characterToData;

	TiXmlElement* common = root->FirstChildElement( "common" );
	string value = common->Attribute( "scaleW" );
	m_widthInPixels = atoi( value.c_str() );

	value = common->Attribute( "scaleH" );
	m_heightInPixels = atoi( value.c_str() );

	float widthConverter  = (float)( 1.f/m_widthInPixels );
	float heightConverter = (float)( 1.f/m_heightInPixels );

	common = root->FirstChildElement( "chars" );

	for( TiXmlElement* character = common->FirstChildElement(); character != NULL; character = character->NextSiblingElement() )
	{
		value = character->Attribute( "id" );
		int id = atoi( value.c_str() );
		GlyphMetaData* metaData = new GlyphMetaData();
		
		value = character->Attribute( "height" );
		float height = (float)atoi( value.c_str() );
		float heightRatioConverter = 1.f/height;

		value = character->Attribute( "width" );

		float ttfB = (float)atoi( value.c_str() );
		float pixelsttfB = ttfB;
		ttfB = (float)( ttfB * heightRatioConverter );
		
		value = character->Attribute( "xoffset" );
		float ttfA = (float)atoi( value.c_str() );
		ttfA = (float)( ttfA * heightRatioConverter );

		value = character->Attribute( "xadvance" );
		float ttfC = (float)atoi( value.c_str() );

		ttfC = (float)( ttfC * heightRatioConverter );
		ttfC = ttfC - ttfB;
		ttfC = ttfC - ttfA;

		metaData->m_ttfA = ttfA;
		metaData->m_ttfB = ttfB;
		metaData->m_ttfC = ttfC;

		value = character->Attribute( "x" );
		float x = (float) atoi( value.c_str() );
		value = character->Attribute( "y" );
		float y = (float) atoi( value.c_str() );

		Vec2 min = Vec2( x * widthConverter, y * heightConverter );
		
		value = character->Attribute( "height" );
		float yOffset = (float) atoi( value.c_str() );

		Vec2 max = Vec2( ( x + pixelsttfB ) * widthConverter, ( y + yOffset ) * heightConverter );

		metaData->m_minTexCoords = min;
		metaData->m_maxTexCoords = max;

		metaData->texture = fontTexture;

		characterToData[id] = metaData;
	}

	m_Fonts.push_back( characterToData );
	return 0;
}


//----------------------------------------------------------------------------------
void FontGenerator::drawText( string& toDraw, GENERATED_FONTS fontToUse, float cellHeight, Vec2& startingLocation, RGBAColors colorOfText)
{
	vector<Vertex3D_PCT> vertexes;
	float currentX = startingLocation.x;
	int textureID = 0;
	for( int i = 0; i < (int) toDraw.length(); ++i)
	{
		if(m_Fonts[fontToUse].find( toDraw.c_str()[i] ) == m_Fonts[fontToUse].end())
		{
			continue;
		}
		fontToUse = GENERATED_FONTS_BIT;
		GlyphMetaData* activeData = m_Fonts[fontToUse][toDraw.c_str()[i]];
		textureID = activeData->texture->m_openglTextureID;

		float a = activeData->m_ttfA * cellHeight;
		float b = activeData->m_ttfB * cellHeight;
		float c = activeData->m_ttfC * cellHeight;

		currentX += a;

		Vec2 min = activeData->m_minTexCoords;
		Vec2 max = activeData->m_maxTexCoords;

		vertexes.push_back( Vertex3D_PCT() );
		vertexes.push_back( Vertex3D_PCT() );
		vertexes.push_back( Vertex3D_PCT() );
		vertexes.push_back( Vertex3D_PCT() );

		int index = vertexes.size() - 4;
		vertexes[index].m_texCoords = min;
		vertexes[index].m_position = Vec3(currentX, startingLocation.y + cellHeight, 0.0f);
		++index;

		vertexes[index].m_texCoords = Vec2(min.x, max.y);
		vertexes[index].m_position = Vec3(currentX, startingLocation.y, 0.0f);
		++index;

		currentX += b;

		vertexes[index].m_texCoords = max;
		vertexes[index].m_position = Vec3(currentX, startingLocation.y, 0.0f);
		++index;

		vertexes[index].m_texCoords = Vec2(max.x, min.y);
		vertexes[index].m_position = Vec3(currentX, startingLocation.y + cellHeight, 0.0f);
		++index;

		currentX += c;
	}

	for( int i = 0; i < (int) vertexes.size(); ++i )
	{
		vertexes[i].m_color = colorOfText;
	}

	Vec3 translation(0.0f, 0.0f, 0.0f);

	GLWrapperEnableGLWrapperOrtho();
	GLWrapperVertexArray( vertexes, translation, GL_WRAPPER_QUADS, textureID );
	GLWrapperDisableGLWrapperOrtho();
}


//----------------------------------------------------------------------------------
void FontGenerator::initialize()
{
	string xmlFile = "Data/Font/bitFont.fnt";
	string imageFile = "Data/Font/bitFont_0.png"/*"Data/Images/stone.png"*/;
	loadFontInformationFromXML( xmlFile, imageFile );
}

//----------------------------------------------------------------------------------
int FontGenerator::findClosestTextIndex( string& toDraw, GENERATED_FONTS fontToUse, float cellHeight, Vec2& mousePos )
{
	int index = toDraw.length();

	float addedWidth = 0.0f;
	float lastAddedWidth = addedWidth;

	for(int i = 0; i < (int) index; ++i)
	{
		if(m_Fonts[fontToUse].find( toDraw.c_str()[i] ) == m_Fonts[fontToUse].end())
		{
			continue;
		}
		GlyphMetaData* activeData = m_Fonts[fontToUse][toDraw.c_str()[i]];

		float a = activeData->m_ttfA;
		float b = activeData->m_ttfB;
		float c = activeData->m_ttfC;
		float temp = a + b + c;
		temp *= cellHeight;
		addedWidth += temp;

		if(addedWidth > mousePos.x)
		{
			if(abs(addedWidth - mousePos.x) < abs(lastAddedWidth - mousePos.x))
			{
				return i;
			}
			else
			{
				return i - 1;
			}
		}

		lastAddedWidth = addedWidth;
	}

	return index - 1;
}

//----------------------------------------------------------------------------------
float FontGenerator::calcTextWidth( string& toDraw, GENERATED_FONTS fontToUse, float cellHeight, int index )
{
	if( index == -1 )
	{
		index = toDraw.length();
	}

	float addedWidth = 0.0f;

	for(int i = 0; i < (int) index; ++i)
	{
		if(m_Fonts[fontToUse].find( toDraw.c_str()[i] ) == m_Fonts[fontToUse].end())
		{
			continue;
		}
		GlyphMetaData* activeData = m_Fonts[fontToUse][toDraw.c_str()[i]];

		float a = activeData->m_ttfA;
		float b = activeData->m_ttfB;
		float c = activeData->m_ttfC;

		addedWidth += a;
		addedWidth += b;
		addedWidth += c;
	}

	addedWidth *= cellHeight;

	return addedWidth;
}