#pragma once
class EulerAngles
{
public:
	EulerAngles(void);
	EulerAngles(const float& newRollDegreesAboutX, const float& newPitchDegreesAboutY, const float& newYawDegreesAboutZ);
	~EulerAngles(void);

	float rollDegreesAboutX;
	float pitchDegreesAboutY;
	float yawDegreesAboutZ;
};

