#pragma once
class IntVec2
{
public:

	IntVec2(void);
	~IntVec2(void);
	IntVec2(int newX, int newY);

	//size_t operator()(const IntVec2& other) const;
	bool operator==(const IntVec2& other) const;
	bool operator>=(const IntVec2& other) const;
	bool operator<(const IntVec2& other) const;

	static int getManhattenDistance( const IntVec2& lhs, const IntVec2& rhs );
	static float getDistance( const IntVec2& lhs, const IntVec2& rhs);
	int x;
	int y;
};