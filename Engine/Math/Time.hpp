//-----------------------------------------------------------------------------------------------
// Time.hpp
// Developer: Squirrel Eiserloh
#pragma once
#ifndef included_Time
#define included_Time

//---------------------------------------------------------------------------
double GetCurrentTimeSeconds();


#endif // included_Time
