#include "Vec2.hpp"
#include <cmath>

const double PI = 3.14159265;


//---------------------------------------------------------------------------
Vec2::Vec2(void)
	: x( 0 )
	, y( 0 )
{

}


//---------------------------------------------------------------------------
Vec2::Vec2(const float& newX, const float& newY)
	: x( newX )
	, y( newY )
{
}


//---------------------------------------------------------------------------
float Vec2::getMagnetude()
{
	float magnetude;
	float pyth_x = x * x;
	float pyth_y = y * y;
	float lenSquared = pyth_x + pyth_y;
	if(lenSquared > 0.f)
	{
		magnetude = sqrt(pyth_x + pyth_y);
	}
	else
	{
		return 0.f;
	}
	
	return magnetude;
}


//---------------------------------------------------------------------------
Vec2 Vec2::rotateToDegrees(float angle)
{
	float baseAngle = getBaseOrientationDegrees();
	angle += baseAngle;
	Vec2 toReturn;
	float m = getMagnetude();
	toReturn.x = cos(degreesToRadians(angle)) * m;
	toReturn.y = sin(degreesToRadians(angle)) * m;
	return toReturn;
}


//---------------------------------------------------------------------------
float Vec2::degreesToRadians(const float& degrees)
{
	float convertRatio = (float) (PI/180.f);
	float toReturn = degrees * convertRatio;
	return toReturn;
}


//---------------------------------------------------------------------------
float Vec2::radiansToDegrees(const float& radians)
{
	float convertRatio = (float) (180.f/PI);
	float toReturn = radians * convertRatio;
	return toReturn;
}


//---------------------------------------------------------------------------
float Vec2::getBaseOrientationDegrees()
{
	float tempX = x;
	float tempY = y;

	normalize();

	float angle = atan2(y, x);

	x = tempX;
	y = tempY;
	return radiansToDegrees(angle);
}


//---------------------------------------------------------------------------
void Vec2::scalarMultiplication(const float& scalar)
{
	x *= scalar;
	y *= scalar;
}


//---------------------------------------------------------------------------
float Vec2::normalize()
{
	float magnetude = getMagnetude();
	if(magnetude > 0.f)
	{
		float invLen = 1.f / magnetude;
		x *= invLen;
		y *= invLen;
	}
	else
	{
		return 0.f;
	}
	return magnetude;
}


//---------------------------------------------------------------------------
Vec2 Vec2::addVectors(const Vec2& one, const Vec2& two)
{
	Vec2 sum;
	sum.x = one.x + two.x;
	sum.y = one.y + two.y;
	return sum;
}


float Vec2::dotProduct(const Vec2& one, const Vec2& two)
{
	float dotProduct = 0;
	dotProduct += one.x * two.x;
	dotProduct += one.y * two.y;
	return dotProduct;
}


//---------------------------------------------------------------------------
inline void Vec2::setLength(float newLength)
{
	normalize();
	x *= newLength;
	y *= newLength;
}


//---------------------------------------------------------------------------
Vec2::~Vec2(void)
{

}


//----------------------------------------------------------------------------------
bool Vec2::operator==(const Vec2& other) const
{
	return (x == other.x && y == other.y);
}

//----------------------------------------------------------------------------------
Vec2 Vec2::operator-(const Vec2& other) const
{
	Vec2 toReturn = Vec2(0.f, 0.f);
	toReturn.x = this->x - other.x;
	toReturn.y = this->y - other.y;
	return toReturn;
}