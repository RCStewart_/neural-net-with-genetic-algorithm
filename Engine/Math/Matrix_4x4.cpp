#include "Matrix_4x4.hpp"

const float DEG_TO_RAD = 0.0174532925f;

//----------------------------------------------------------------------------------
Matrix_4x4::Matrix_4x4(void)
{
	makeIdentity();
	m_scale = 1.0f;
}


//----------------------------------------------------------------------------------
Matrix_4x4::Matrix_4x4( vector<float>& matrix )
{
	for(int i = 0; i < 16; ++i)
	{
		m_matrix[i] = matrix[i];
	}
	m_scale = 1.0f;
}


//----------------------------------------------------------------------------------
Matrix_4x4::Matrix_4x4( float a, float b, float c, float d,
	float e, float f, float g, float h,
	float i, float j, float k, float l,
	float m, float n, float o, float p)
{
	m_matrix[0] = a;
	m_matrix[1] = b;
	m_matrix[2] = c;
	m_matrix[3] = d;
	m_matrix[4] = e;
	m_matrix[5] = f;
	m_matrix[6] = g;
	m_matrix[7] = h;
	m_matrix[8] = i;
	m_matrix[9] = j;
	m_matrix[10] = k;
	m_matrix[11] = l;
	m_matrix[12] = m;
	m_matrix[13] = n;
	m_matrix[14] = o;
	m_matrix[15] = p;

	m_scale = 1.0f;
}

//----------------------------------------------------------------------------------
Matrix_4x4::~Matrix_4x4(void)
{

}


//----------------------------------------------------------------------------------
void Matrix_4x4::makeIdentity()
{
	for(int i = 0; i < 16; ++i)
	{
		m_matrix[i] = 0.0f;
	}

	m_matrix[0] = 1.0f;
	m_matrix[5] = 1.0f;
	m_matrix[10] = 1.0f;
	m_matrix[15] = 1.0f;
}

//----------------------------------------------------------------------------------
void Matrix_4x4::setAsATranslationMatrix(float x, float y, float z)
{
	for(int i = 0; i < 16; ++i)
	{
		m_matrix[i] = 0.0f;
	}

	m_matrix[0] = 1.0f;
	m_matrix[5] = 1.0f;
	m_matrix[10] = 1.0f;
	m_matrix[12] = x;
	m_matrix[13] = y;
	m_matrix[14] = z;
	m_matrix[15] = 1.0f;
}

//----------------------------------------------------------------------------------------------
void Matrix_4x4::setTo3x3Matrix( const Vec3& xBasis, const Vec3& yBasis, const Vec3& zBasis )
{
	makeIdentity();
	m_matrix[0] = xBasis.x;
	m_matrix[1] = xBasis.y;
	m_matrix[2] = xBasis.z;
				  
	m_matrix[4] = yBasis.x;
	m_matrix[5] = yBasis.y;
	m_matrix[6] = yBasis.z;
				  
	m_matrix[8] = zBasis.x;
	m_matrix[9] = zBasis.y;
	m_matrix[10]= zBasis.z;
}

//----------------------------------------------------------------------------------------------
Vec3 Matrix_4x4::multiplyVec3By3x3Matrix( const Vec3& toModify )
{
	Vec3 toReturn = Vec3( 0.f, 0.f, 0.f );

	toReturn.x = toModify.x * m_matrix[0] + toModify.y * m_matrix[4] + toModify.z * m_matrix[8];
	toReturn.y = toModify.x * m_matrix[1] + toModify.y * m_matrix[5] + toModify.z * m_matrix[9];
	toReturn.z = toModify.x * m_matrix[2] + toModify.y * m_matrix[6] + toModify.z * m_matrix[10];

	toReturn.scalarMultiplication( m_scale );
	return toReturn;
}

//----------------------------------------------------------------------------------
void Matrix_4x4::setAsAScaleMatrix(float x, float y, float z)
{
	for(int i = 0; i < 16; ++i)
	{
		m_matrix[i] = 0.0f;
	}

	m_matrix[0] = x;
	m_matrix[5] = y;
	m_matrix[10] = z;
	m_matrix[15] = 1.0f;
}

//----------------------------------------------------------------------------------
Matrix_4x4 Matrix_4x4::transformByMatrix( const Matrix_4x4& rhs)
{
	vector<float> result;

	Matrix_4x4& lhs = *this;

	//column 1
	result.push_back(	
		lhs.m_matrix[0 ] * rhs.m_matrix[0] + 
		lhs.m_matrix[4 ] * rhs.m_matrix[1] + 
		lhs.m_matrix[8 ] * rhs.m_matrix[2] + 
		lhs.m_matrix[12] * rhs.m_matrix[3] );
	result.push_back(	
		lhs.m_matrix[1 ] * rhs.m_matrix[0] + 
		lhs.m_matrix[5 ] * rhs.m_matrix[1] + 
		lhs.m_matrix[9 ] * rhs.m_matrix[2] + 
		lhs.m_matrix[13] * rhs.m_matrix[3] );
	result.push_back(	
		lhs.m_matrix[2 ] * rhs.m_matrix[0] + 
		lhs.m_matrix[6 ] * rhs.m_matrix[1] + 
		lhs.m_matrix[10] * rhs.m_matrix[2] + 
		lhs.m_matrix[14] * rhs.m_matrix[3] );
	result.push_back(	
		lhs.m_matrix[3 ] * rhs.m_matrix[0] + 
		lhs.m_matrix[7 ] * rhs.m_matrix[1] + 
		lhs.m_matrix[11] * rhs.m_matrix[2] + 
		lhs.m_matrix[15] * rhs.m_matrix[3] );

	//column 2
	result.push_back(	
		lhs.m_matrix[0 ] * rhs.m_matrix[4] + 
		lhs.m_matrix[4 ] * rhs.m_matrix[5] + 
		lhs.m_matrix[8 ] * rhs.m_matrix[6] + 
		lhs.m_matrix[12] * rhs.m_matrix[7] );
	result.push_back(	
		lhs.m_matrix[1 ] * rhs.m_matrix[4] + 
		lhs.m_matrix[5 ] * rhs.m_matrix[5] + 
		lhs.m_matrix[9 ] * rhs.m_matrix[6] + 
		lhs.m_matrix[13] * rhs.m_matrix[7] );
	result.push_back(	
		lhs.m_matrix[2 ] * rhs.m_matrix[4] + 
		lhs.m_matrix[6 ] * rhs.m_matrix[5] + 
		lhs.m_matrix[10] * rhs.m_matrix[6] + 
		lhs.m_matrix[14] * rhs.m_matrix[7] );
	result.push_back(	
		lhs.m_matrix[3 ] * rhs.m_matrix[4] + 
		lhs.m_matrix[7 ] * rhs.m_matrix[5] + 
		lhs.m_matrix[11] * rhs.m_matrix[6] + 
		lhs.m_matrix[15] * rhs.m_matrix[7] );

	//column 3
	result.push_back(	
		lhs.m_matrix[0 ] * rhs.m_matrix[8] + 
		lhs.m_matrix[4 ] * rhs.m_matrix[9] + 
		lhs.m_matrix[8 ] * rhs.m_matrix[10] + 
		lhs.m_matrix[12] * rhs.m_matrix[11] );
	result.push_back(	
		lhs.m_matrix[1 ] * rhs.m_matrix[8] + 
		lhs.m_matrix[5 ] * rhs.m_matrix[9] + 
		lhs.m_matrix[9 ] * rhs.m_matrix[10] + 
		lhs.m_matrix[13] * rhs.m_matrix[11] );
	result.push_back(	
		lhs.m_matrix[2 ] * rhs.m_matrix[8] + 
		lhs.m_matrix[6 ] * rhs.m_matrix[9] + 
		lhs.m_matrix[10] * rhs.m_matrix[10] + 
		lhs.m_matrix[14] * rhs.m_matrix[11] );
	result.push_back(	
		lhs.m_matrix[3 ] * rhs.m_matrix[8] + 
		lhs.m_matrix[7 ] * rhs.m_matrix[9] + 
		lhs.m_matrix[11] * rhs.m_matrix[10] + 
		lhs.m_matrix[15] * rhs.m_matrix[11] );

	//column 4
	result.push_back(	
		lhs.m_matrix[0 ] * rhs.m_matrix[12] + 
		lhs.m_matrix[4 ] * rhs.m_matrix[13] + 
		lhs.m_matrix[8 ] * rhs.m_matrix[14] + 
		lhs.m_matrix[12] * rhs.m_matrix[15] );
	result.push_back(	
		lhs.m_matrix[1 ] * rhs.m_matrix[12] + 
		lhs.m_matrix[5 ] * rhs.m_matrix[13] + 
		lhs.m_matrix[9 ] * rhs.m_matrix[14] + 
		lhs.m_matrix[13] * rhs.m_matrix[15] );
	result.push_back(	
		lhs.m_matrix[2 ] * rhs.m_matrix[12] + 
		lhs.m_matrix[6 ] * rhs.m_matrix[13] + 
		lhs.m_matrix[10] * rhs.m_matrix[14] + 
		lhs.m_matrix[14] * rhs.m_matrix[15] );
	result.push_back(	
		lhs.m_matrix[3 ] * rhs.m_matrix[12] + 
		lhs.m_matrix[7 ] * rhs.m_matrix[13] + 
		lhs.m_matrix[11] * rhs.m_matrix[14] + 
		lhs.m_matrix[15] * rhs.m_matrix[15] );

	Matrix_4x4 toReturn(result);
	return toReturn;
}

//----------------------------------------------------------------------------------
void Matrix_4x4::setAsARotationMatrix(float angle, float x, float y, float z)
{
	makeIdentity();
	float c = cos(angle * DEG_TO_RAD);
	float s = sin(angle * DEG_TO_RAD);

	m_matrix[0] = x*x * (1.f - c) + c;
	m_matrix[1] = y*x * (1.f - c) + z*s;
	m_matrix[2] = z*x * (1.f - c) - y*s;
	m_matrix[3] = 0.f;
	m_matrix[4] = x*y * (1.f - c) - z*s;
	m_matrix[5] = y*y * (1.f - c) + c;
	m_matrix[6] = z*y * (1.f - c) + x*s;
	m_matrix[7] = 0.f;
	m_matrix[8] = x*z * (1.f -c) + y*s;
	m_matrix[9] = y*z * (1.f - c) - x*s;
	m_matrix[10] = z*z * (1.f - c) + c;
	m_matrix[11] = 0.f;
	m_matrix[12] = 0.f;
	m_matrix[13] = 0.f;
	m_matrix[14] = 0.f;
	m_matrix[15] = 1.f;
}

//----------------------------------------------------------------------------------------------
void Matrix_4x4::setScale( float scale )
{
	m_scale = scale;
}