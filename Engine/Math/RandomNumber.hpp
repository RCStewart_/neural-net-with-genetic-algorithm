#pragma once

class IntVec2;

class RandomNumber
{
public:
	static float getRandomFloatZeroToOne();
	static float getRandomFloatInRange(const float& min, const float& max);
	static bool getBool();
	static int getRandomIntInRange( const IntVec2& range );
};

