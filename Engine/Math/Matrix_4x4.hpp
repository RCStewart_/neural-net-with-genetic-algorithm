#pragma once
#include <vector>
#include "Engine/Math/Vec3.hpp"

using namespace std;
class Matrix_4x4
{
public:
	Matrix_4x4(void);
	Matrix_4x4( vector<float>& matrix );
	Matrix_4x4( float a, float b, float c, float d,
				float e, float f, float g, float h,
				float i, float j, float k, float l,
				float m, float n, float o, float p);
	~Matrix_4x4(void);

	void setTo3x3Matrix( const Vec3& xBasis, const Vec3& yBasis, const Vec3& zBasis );
	Vec3 multiplyVec3By3x3Matrix( const Vec3& toModify );
	void setAsATranslationMatrix(float x, float y, float z);
	void setAsAScaleMatrix(float x, float y, float z);
	void setAsARotationMatrix(float angle, float x, float y, float z);
	void makeIdentity();
	void setScale( float scale );
	Matrix_4x4 transformByMatrix( const Matrix_4x4& rhs);

	float m_matrix[16];

	float m_scale;
};

