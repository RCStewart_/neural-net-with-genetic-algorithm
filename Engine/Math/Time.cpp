//-----------------------------------------------------------------------------------------------
// Time.cpp
// Developer: Squirrel Eiserloh

#include "Engine\Math\Time.hpp"
#include "Engine\Core\WindowsWrapper.hpp"

//---------------------------------------------------------------------------
double GetCurrentTimeSeconds()
{
	return WindowWrapperGetCurrentTimeSeconds();
}


