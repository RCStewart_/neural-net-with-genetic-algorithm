#include "RandomNumber.hpp"
#include <stdlib.h>
#include "Engine/Math/IntVec2.hpp"

float RandomNumber::getRandomFloatZeroToOne()
{
	const float convertionRatio = 1 / (float) RAND_MAX;

	return (float) rand() * convertionRatio;
}

float RandomNumber::getRandomFloatInRange(const float& min, const float& max)
{
	float percentOfTotal = getRandomFloatZeroToOne();
	float randomGeneratedNumber = max - min;
	randomGeneratedNumber *= percentOfTotal;
	randomGeneratedNumber += min;
	return randomGeneratedNumber;
}

bool RandomNumber::getBool()
{
	bool boolToReturn = false;
	int temp = rand() % 2;
	if(temp == 1)
	{
		boolToReturn = true;
	}
	return boolToReturn;
}

int RandomNumber::getRandomIntInRange( const IntVec2& range )
{
	int displacement = range.y - range.x;
	if( displacement == 0 )
		displacement = 1;
	return rand() % (displacement) + range.x;
}