#pragma once
class Vec2
{
public:
	Vec2(void);
	Vec2(const float& newX, const float& newY);

	Vec2 rotateToDegrees(float angle);
	float getMagnetude();
	float getBaseOrientationDegrees();
	float degreesToRadians(const float& degrees);
	float radiansToDegrees(const float& radians);
	void scalarMultiplication(const float& scalar);
	float normalize();
	void setLength(float newLength);

	static float dotProduct(const Vec2& one, const Vec2& two);
	static Vec2 addVectors(const Vec2& one, const Vec2& two);
	Vec2 operator-(const Vec2& other) const;

	bool operator==(const Vec2& other) const;

	~Vec2(void);

	float x;
	float y;
};

