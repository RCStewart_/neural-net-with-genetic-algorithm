#include "ShaderProgram.hpp"


ShaderProgram::ShaderProgram(void)
{
	m_vertexID		= -1;
	m_colorID		= -1;
	m_textureID		= -1;
	m_programID		= -1;
	m_textCoordsID	= -1;
	m_lightPosID	= -1;
	m_normalID		= -1;
	m_biTangentID	= -1;
	m_tangentID		= -1;
	m_perlinMapID		= -1;
	m_perlinTexCoordsID	= -1;
	m_framebufferShaderColorTextureUniformLocation = -1;
	m_framebufferShaderDepthTextureUniformLocation = -1;
}


ShaderProgram::~ShaderProgram(void)
{
}
