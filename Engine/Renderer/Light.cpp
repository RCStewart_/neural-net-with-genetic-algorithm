#include "Engine\Renderer\Light.hpp"
#include "Engine\Core\GameCommon.hpp"
#include "Engine\Renderer\Renderer.hpp"
#include "Engine\Renderer\Material.hpp"
//#include "TheGame.hpp"

//----------------------------------------------------------------------------------
Light::Light(void)
{
}

//----------------------------------------------------------------------------------
Light::Light( const Vec3& pos )
{
	m_ambientness = 0.0f;

	m_innerRadius = g_zFar;
	m_outerRadius = g_zFar + 1;

	m_innerPenumbraDot = -1.5f;
	m_outerPenumbraDot = -2.f;

	m_intensityOutsidePenumbra = 0.0f;

	m_position = pos;
	m_direction = Vec3(1.0f, 1.0f, -.3f);
	m_direction.normalize();
	m_colorAndBrightness = RGBAColors(255, 255, 255, 255);
}

//----------------------------------------------------------------------------------
Light::~Light(void)
{
}

//----------------------------------------------------------------------------------
void Light::setAsGlobalPointLight( const Vec3& pos, const RGBAColors& colorAndBrightness )
{
	m_ambientness = 0.0f;

	m_innerRadius = g_zFar;
	m_outerRadius = g_zFar + 1;

	m_innerPenumbraDot = -1.5f;
	m_outerPenumbraDot = -2.f;

	m_intensityOutsidePenumbra = 0.0f;

	m_position = pos;
	m_direction = Vec3(1.0f, 1.0f, 1.0f);
	m_direction.normalize();
	m_colorAndBrightness = colorAndBrightness;
}

//----------------------------------------------------------------------------------
void Light::setAsLocalPointLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float innerRadiusDistance, float outerRadiusDistance )
{
	m_ambientness = 0.0f;

	m_innerRadius = innerRadiusDistance;
	m_outerRadius = outerRadiusDistance;

	m_innerPenumbraDot = -1.5f;
	m_outerPenumbraDot = -2.f;

	m_intensityOutsidePenumbra = 0.0f;

	m_position = pos;
	m_direction = Vec3(1.0f, 1.0f, 1.0f);
	m_direction.normalize();
	m_colorAndBrightness = colorAndBrightness;
}

//----------------------------------------------------------------------------------
void Light::setAsLocalSpotLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float innerRadiusDistance, float outerRadiusDistance, const Vec3& forwardDirection, float innerPenumbraDegrees, float outerPenumbraDegrees, float intensityOutsidePenumbra )
{
	m_ambientness = 0.0f;

	m_innerRadius = innerRadiusDistance;
	m_outerRadius = outerRadiusDistance;

	m_innerPenumbraDot = cos( (innerPenumbraDegrees * .5f) * (g_PI/180) );
	m_outerPenumbraDot = cos( (outerPenumbraDegrees * .5f) * (g_PI/180) );

	m_intensityOutsidePenumbra = intensityOutsidePenumbra;

	m_position = pos;
	m_direction = forwardDirection;
	m_direction.normalize();
	m_colorAndBrightness = colorAndBrightness;
}

//----------------------------------------------------------------------------------
void Light::setAsGlobalSpotLight( const Vec3& pos, const RGBAColors& colorAndBrightness, const Vec3& forwardDirection, float innerPenumbraDegrees, float outerPenumbraDegrees, float intensityOutsidePenumbra )
{
	UNUSED(pos);
	m_ambientness = 0.0f;

	m_innerRadius = 5;//g_zFar;
	m_outerRadius = 10;//g_zFar + 1;

	m_innerPenumbraDot = cos( (innerPenumbraDegrees * .5f) * (g_PI/180) );
	m_outerPenumbraDot = cos( (outerPenumbraDegrees * .5f) * (g_PI/180) );

	m_intensityOutsidePenumbra = intensityOutsidePenumbra;

	m_position = Vec3(30.f, 30.f, 30.f);//pos;
	m_direction = forwardDirection;
	m_direction.normalize();
	m_colorAndBrightness = colorAndBrightness;
}

//----------------------------------------------------------------------------------
void Light::setAsLocalAmbientLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float innerRadiusDistance, float outerRadiusDistance, float ambienceIntensity )
{
	m_ambientness = ambienceIntensity;

	m_innerRadius = innerRadiusDistance;
	m_outerRadius = outerRadiusDistance;

	m_innerPenumbraDot = 2.0f;
	m_outerPenumbraDot = 2.0f;

	m_intensityOutsidePenumbra = 0.0f;

	m_position = pos;
	m_direction = Vec3(1.0f, 1.0f, 1.0f);
	m_direction.normalize();
	m_colorAndBrightness = colorAndBrightness;
}

//----------------------------------------------------------------------------------
void Light::setAsGlobalAmbientLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float ambienceIntensity )
{
	m_ambientness = ambienceIntensity;

	m_innerRadius = g_zFar;
	m_outerRadius = g_zFar + 1;

	m_innerPenumbraDot = 2.0f;
	m_outerPenumbraDot = 2.0f;

	m_intensityOutsidePenumbra = 0.0f;

	m_position = pos;
	m_direction = Vec3(1.0f, 1.0f, 1.0f);
	m_direction.normalize();
	m_colorAndBrightness = colorAndBrightness;
}

//----------------------------------------------------------------------------------
void Light::setAsGlobalDirectLight( const Vec3& forwardDirection, const RGBAColors& colorAndBrightness )
{
	m_ambientness = 0.0f;

	m_innerRadius = g_zFar;
	m_outerRadius = g_zFar + 1;

	m_innerPenumbraDot = -1.5f;
	m_outerPenumbraDot = -2.f;

	m_intensityOutsidePenumbra = 0.0f;

	Vec3 pos = forwardDirection;
	pos.normalize();
	pos.scalarMultiplication( -1 ); //Reverse Direction
	pos.scalarMultiplication( g_zFar * .8f ); //Move far away
	m_position = pos;
	m_direction = Vec3(1.0f, 1.0f, 1.0f);
	m_direction.normalize();
	m_colorAndBrightness = colorAndBrightness;
}