#pragma once
#include <string>
#include <iostream>
#include <map>

#include <cstdio>

enum GL_TYPES;

class Shader
{
public:
	Shader( const std::string& shaderFilePath, GL_TYPES shaderType );
	static Shader* Shader::GetShaderByName( const std::string& shaderFilePath );
	static Shader* Shader::CreateOrGetShader( const std::string& shaderFilePath, GL_TYPES shaderType );

	static std::map< std::string, Shader* >	Shader::s_ShaderRegistry;

	unsigned int m_shaderID;
	GL_TYPES m_shaderType;
	std::string m_filePath;
};