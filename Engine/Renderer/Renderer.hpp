#pragma once

#include <string>
#include <vector>
#include "Engine\Renderer\SpriteSheet.hpp"
#include "Engine\Renderer\ShaderProgram.hpp"
#include "Engine\Math\Matrix_4x4.hpp"
#include "Engine\Core\RGBAColors.hpp"

enum GL_TYPES;
class Shader;
class Vec3;
class Material;
class FBO;
struct AttributeIDs;

using namespace std;
class Renderer
{
public:
	Renderer(void);
	~Renderer(void);
	void init();
	int createMaterial( const string& fragShaderPath, const string& vertexShaderPath );
	void addTexturesToMaterial(int materialIndex, Texture* diffuse, Texture* normal, Texture* specular, Texture* emissive);
	void setUniforms();
	void disableShaderProgram();

	void generateAndRenderSphere(float radius, unsigned int rings, unsigned int sectors, Vec3 position, int materialID);
	void generateAndRenderTexturedAABB( const Vec3& minPos, const Vec3& maxPos, Vec3 translation, int materialID );
	void renderSquares();
	void render2DSprite(int spriteSheetID, IntVec2& spriteCoords, const Vec2& translation, const Vec2& dimensionsOfEntity, const RGBAColors& color);

	int getSpriteSheetIDFromFileName(std::string& fileName, int spritesWide, int spritesHigh);

	//assumes file extension .XxY.ss.png.  XxY is the number of sprites in both the X and Y coords
	int getSpriteSheetIDFromFileName(std::string& fileName);

	//Matrix Functions
	//----------------------------------------------------------------------------------
	void initiateStack();
	void clearStack();
	int stackSize();
	void stackPush( Matrix_4x4& toPushBack );
	void stackPop();
	void loadIdentity();
	void translateMatrix( float x, float y, float z );
	void scaleMatrix( float x, float y, float z );
	void rotationMatrix( float angle, float x, float y, float z );
	void renderFBOOnScreen( int FBOIndex, const Vec3& minPos, const Vec3& maxPos, Vec3& translation );
	Matrix_4x4 getBackOfMatrix();

	Matrix_4x4 setFrustum(float left, float right, float bottom, float top, float near, float far);
	Matrix_4x4 setPerspectiveFrustum(float fovY, float aspect, float front, float back);
	Matrix_4x4 setOrthoFrustum(float left, float right, float bottom, float top, float near, float far);
	//----------------------------------------------------------------------------------

	vector<Material*> m_materials;
	vector<FBO*> m_FBOs;
	vector<SpriteSheet*> m_spriteSheets;

	int m_nextAttribIDNum;
	int m_currentMaterialID;

private:
	vector<Matrix_4x4> m_matrixStack;
};

extern Renderer* g_theRenderer;
