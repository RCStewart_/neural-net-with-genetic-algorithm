#pragma once
#include "Engine\Math\Vec3.hpp"
#include "Engine\Math\Vec2.hpp"

#include <vector>
#include <map>
//#include "GameCommon.hpp"
#include "Engine\Core\Camera3D.hpp"

#include "Engine\Core\RGBAColors.hpp"

using namespace std;

struct Vertex3D_PCT;

enum DEBUG_MODE
{
	DEBUG_MODE_DEPTHTEST,
	DEBUG_MODE_NO_DEPTHTEST,
	DEBUG_MODE_DUAL
};

struct DebugPoint
{
	DEBUG_MODE mode;
	float startTime;
	float duration;
	Vec3 position;
	RGBAColors color;
	float radius;
};

struct DebugLine
{
	DEBUG_MODE mode;
	float startTime;
	float duration;
	Vec3 startPos;
	Vec3 endPos;
	RGBAColors startCol;
	RGBAColors endCol;
};

struct DebugSphere
{
	DEBUG_MODE mode;
	float startTime;
	float duration;
	Vec3 position;
	RGBAColors color;
	float radius; 
};

struct DebugAABB
{
	DEBUG_MODE mode;
	float startTime;
	float duration;

	Vec3 min;
	Vec3 max;
	RGBAColors faceColor;
	RGBAColors lineColor; 
};

class RGBAColors;
class IntVec2;
class Chunk;
class SpriteSheet;

class DrawDebug
{
public:
	DrawDebug(void);

	~DrawDebug(void);

	void update();
	void renderWithDepth();
	void renderNoDepth();

	void drawAxis();
	void drawCrosshair();

	void drawPoint( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration, float startTime, int index );
	void drawLineSegment( const Vec3& startPosition, const RGBAColors& startColor, const Vec3& endPosition, const RGBAColors& endColor, DEBUG_MODE mode, float duration, float startTime, int index );
	void drawAABB( const Vec3& minPoint, const Vec3& maxPoint, const RGBAColors& faceColor, const RGBAColors& lineColor, DEBUG_MODE mode, float duration, float startTime, int index );
	void drawSphere( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration, float startTime, int index );

	void initiatePoint( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration = 0.0f );
	void initiateSphere( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration = 0.0f );
	void initiateLineSegment( const Vec3& startPosition, const RGBAColors& startColor, const Vec3& endPosition, const RGBAColors& endColor, DEBUG_MODE mode, float duration = 0.0f );
	void initiateArrow( const Vec3& startPosition, const Vec3& endPosition, const RGBAColors& color, DEBUG_MODE mode, float duration = 0.0f );
	void initiateAABB( const Vec3& minPos, const Vec3& maxPos, const RGBAColors& faceColor, const RGBAColors& lineColor, DEBUG_MODE mode, float duration = 0.0f );

	vector<DebugPoint> m_points;
	vector<DebugLine> m_lineSegs;
	vector<DebugSphere> m_spheres;
	vector<DebugAABB> m_AABBs;

	int m_materialIndex;
};