#include "Engine\Renderer\Material.hpp"
#include "Engine\Core\Texture.hpp"
#include "Engine\Renderer\ShaderProgram.hpp"
#include "Engine\Renderer\Shader.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Math\Time.hpp"
#include "Engine\Core\GameCommon.hpp"

//----------------------------------------------------------------------------------
Material::Material(void)
{
}

//----------------------------------------------------------------------------------
Material::~Material(void)
{
}

//----------------------------------------------------------------------------------
Material::Material(const string& fragShaderPath, const string& vertexShaderPath, int& nextAttribIDNumber )
{
	m_shader = new ShaderProgram();
	createProgramID();
	g_materialIDIndexes.push_back( m_shader->m_programID );

	bindShadersToProgramAndLink(fragShaderPath, vertexShaderPath, nextAttribIDNumber );
	useProgram();
	storeShaderUniformLocations();
	GLWrapperUseProgram(0); //Disable the shader	

	m_fogStartDistance	= 30.f;
	m_fogEndDistance	= 50.f;
	m_fogColorAndIntensity = RGBAColors(0, 0, 0, 255);
}

//----------------------------------------------------------------------------------
Shader* Material::createFragmentShader( const string& filePath )
{
	return Shader::CreateOrGetShader( filePath, GL_WRAPPER_FRAGMENT_SHADER );
}

//----------------------------------------------------------------------------------
Shader* Material::createVertexShader( const string& filePath )
{
	return Shader::CreateOrGetShader( filePath, GL_WRAPPER_VERTEX_SHADER );
}

//----------------------------------------------------------------------------------
void Material::bindShadersToProgramAndLink( const string& fragmentFilePath, const string& vertexFilePath, int& nextAttribIDNum )
{
	Shader* fragment = createFragmentShader( fragmentFilePath );
	Shader* vertex = createVertexShader( vertexFilePath );

	GLWrapperAttachShader( m_shader->m_programID, vertex->m_shaderID );
	GLWrapperAttachShader( m_shader->m_programID, fragment->m_shaderID );
	nextAttribIDNum = 1;
	GLWrapperBindAttribLocation(m_shader->m_programID, nextAttribIDNum, "a_vertexes" );
	m_shader->m_vertexID = nextAttribIDNum;
	++nextAttribIDNum;

	GLWrapperBindAttribLocation(m_shader->m_programID, nextAttribIDNum, "a_color" );
	m_shader->m_colorID = nextAttribIDNum;
	++nextAttribIDNum;

	GLWrapperBindAttribLocation(m_shader->m_programID, nextAttribIDNum, "a_textureInCoords" );
	m_shader->m_textCoordsID = nextAttribIDNum;
	++nextAttribIDNum;

	GLWrapperBindAttribLocation(m_shader->m_programID, nextAttribIDNum, "a_tangent" );
	m_shader->m_tangentID = nextAttribIDNum;
	++nextAttribIDNum;

	GLWrapperBindAttribLocation(m_shader->m_programID, nextAttribIDNum, "a_biTangent" );
	m_shader->m_biTangentID = nextAttribIDNum;
	++nextAttribIDNum;

	GLWrapperBindAttribLocation(m_shader->m_programID, nextAttribIDNum, "a_normal" );
	m_shader->m_normalID = nextAttribIDNum;
	++nextAttribIDNum;

	GLWrapperBindAttribLocation(m_shader->m_programID, nextAttribIDNum, "a_perlinTextInCoords" );
	m_shader->m_perlinTexCoordsID = nextAttribIDNum;
	++nextAttribIDNum;

	GLWrapperLinkProgram( m_shader->m_programID );

	GLWrapperCheckForShaderProgramLinkErrors( m_shader->m_programID, fragment, vertex);
}

//----------------------------------------------------------------------------------
void Material::createProgramID()
{
	ShaderProgram newProgramShader;

	newProgramShader.m_programID = GLWrapperGenerateShaderProgram();
	m_shader->m_programID = newProgramShader.m_programID;
}

//----------------------------------------------------------------------------------
void Material::useProgram( )
{
	GLWrapperUseProgram( m_shader->m_programID );
}

//----------------------------------------------------------------------------------
void Material::disableShaderProgram()
{
	GLWrapperUseProgram(0);
}


//----------------------------------------------------------------------------------
void Material::storeShaderUniformLocations()
{
	string time		= "u_time";
	string diffuse	= "u_diffuseMap";
	string normal	= "u_normalMap";
	string specular = "u_specularMap";
	string emissive = "u_emissiveMap";
	string projMat	= "u_projectionMatrix";
	string perlin	= "u_perlinMap";

	string fboColor	= "u_fboColor";
	string fboDepth = "u_fboDepth";

	string objToWorld = "u_objectToWorldMatrix";

	string fogStart = "u_fogStartDistance";
	string fogEnd	= "u_fogEndDistance";
	string fogColorAndInt =  "u_fogColorAndIntensity";

	string cameraLocation = "u_camPos";

	string lightPos						= "u_lightWorldPositions";
	string lightDir						= "u_forwardDirections";
	string lightColor					= "u_ColorOfLights";
	string lightInnerDot				= "u_innerDots";
	string lightOuterDot				= "u_outerDots";
	string lightAmbient					= "u_ambienceOfLights";
	string lightInnerRadius				= "u_innerLightRadius";
	string lightOuterRadius				= "u_outerLightRadius";
	string lightIntensityOutsidePenumbra = "u_intensityOutsidePenumbra";

	m_shader->m_uniforms.timeUniformLocation		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, time );
	m_shader->m_uniforms.projectionMatrixLocation	=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, projMat );	
	m_shader->m_uniforms.diffuseMapUniformLocation	=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, diffuse );
	m_shader->m_uniforms.normalMapUniformLocation	=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, normal );
	m_shader->m_uniforms.emissiveMapUniformLocation	=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, emissive );
	m_shader->m_uniforms.specularMapUniformLocation	=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, specular );
	m_shader->m_perlinMapID							=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, perlin );
	m_shader->m_uniforms.objectToWorldLocation		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, objToWorld );

	m_shader->m_uniforms.fogStartDistanceLocation		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, fogStart );
	m_shader->m_uniforms.fogEndDistanceLocation			=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, fogEnd );	
	m_shader->m_uniforms.fogColorAndIntensityLocation	=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, fogColorAndInt );

	m_shader->m_lightWorldPositions		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightPos						);
	m_shader->m_forwardDirections 		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightDir						);
	m_shader->m_ColorOfLights			=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightColor					);
	m_shader->m_innerDots				=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightInnerDot					);
	m_shader->m_outerDots				=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightOuterDot					);
	m_shader->m_ambienceOfLights		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightAmbient					);
	m_shader->m_innerLightRadius		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightInnerRadius				);
	m_shader->m_outerLightRadius		=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightOuterRadius				);
	m_shader->m_intensityOutsidePenumbra=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, lightIntensityOutsidePenumbra	);

	m_shader->m_uniforms.cameraPositionLocation	=	GLWrapperGetShaderUniformLocation( m_shader->m_programID, cameraLocation );

	m_shader->m_framebufferShaderColorTextureUniformLocation = GLWrapperGetShaderUniformLocation( m_shader->m_programID, fboColor );
	m_shader->m_framebufferShaderDepthTextureUniformLocation = GLWrapperGetShaderUniformLocation( m_shader->m_programID, fboDepth );

	GLWrapperEnableShaderTextures(	m_shader->m_uniforms.emissiveMapUniformLocation,
		m_shader->m_uniforms.specularMapUniformLocation,
		m_shader->m_uniforms.normalMapUniformLocation,
		m_shader->m_uniforms.diffuseMapUniformLocation );

	GLWrapperEnableTexture( m_shader->m_perlinMapID, 4 );
	GLWrapperEnableTexture( m_shader->m_framebufferShaderColorTextureUniformLocation, 5 );
	GLWrapperEnableTexture( m_shader->m_framebufferShaderDepthTextureUniformLocation, 6 );
}

//----------------------------------------------------------------------------------
void Material::setShaderUniforms()
{
	GLWrapperUniform1f( m_shader->m_uniforms.timeUniformLocation, (float) GetCurrentTimeSeconds() );
	GLWrapperUniform3f( m_shader->m_uniforms.cameraPositionLocation, g_cameraPos );
	GLWrapperUniform1i( m_shader->m_uniforms.diffuseMapUniformLocation, 0 );	// for GL_TEXTURE0, texture unit 0
	GLWrapperUniform1i( m_shader->m_uniforms.normalMapUniformLocation, 1 );		// for GL_TEXTURE1, texture unit 1
	GLWrapperUniform1i( m_shader->m_uniforms.specularMapUniformLocation, 2 );   // for GL_TEXTURE2, texture unit 2
	GLWrapperUniform1i( m_shader->m_uniforms.emissiveMapUniformLocation, 3 );   // for GL_TEXTURE3, texture unit 3
	GLWrapperUniform1i( m_shader->m_perlinMapID, 4 );
	GLWrapperUniform1i( m_shader->m_framebufferShaderColorTextureUniformLocation, 5 );
	GLWrapperUniform1i( m_shader->m_framebufferShaderDepthTextureUniformLocation, 6 );

	GLWrapperUniform1f( m_shader->m_uniforms.fogStartDistanceLocation, m_fogStartDistance );
	GLWrapperUniform1f( m_shader->m_uniforms.fogEndDistanceLocation, m_fogEndDistance );
	GLWrapperUniform4f( m_shader->m_uniforms.fogColorAndIntensityLocation, m_fogColorAndIntensity );

	vector<Vec3> lightPos;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightPos.push_back(g_lights[i].m_position);
	}
	vector<Vec3> lightDir;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightDir.push_back(g_lights[i].m_direction);
	}
	vector<RGBAColors> lightColor;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightColor.push_back(g_lights[i].m_colorAndBrightness);
	}
	vector<float> lightInnerDots;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightInnerDots.push_back(g_lights[i].m_innerPenumbraDot);
	}
	vector<float> lightOuterDots;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightOuterDots.push_back(g_lights[i].m_outerPenumbraDot);
	}
	vector<float> lightAmbient;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightAmbient.push_back(g_lights[i].m_ambientness);
	}
	vector<float> lightInnerRadius;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightInnerRadius.push_back(g_lights[i].m_innerRadius);
	}
	vector<float> lightOuterRadius;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightOuterRadius.push_back(g_lights[i].m_outerRadius);
	}
	vector<float> lightOutsidePenumbra;
	for(int i = 0; i < (int) g_lights.size(); ++i)
	{
		lightOutsidePenumbra.push_back(g_lights[i].m_intensityOutsidePenumbra);
	}

	GLWrapperUniform3fv( m_shader->m_lightWorldPositions	, lightPos   );
	GLWrapperUniform3fv( m_shader->m_forwardDirections 		, lightDir   );
	GLWrapperUniform4fv( m_shader->m_ColorOfLights			, lightColor );
	GLWrapperUniform1fv( m_shader->m_innerDots				, lightInnerDots );
	GLWrapperUniform1fv( m_shader->m_outerDots				, lightOuterDots );
	GLWrapperUniform1fv( m_shader->m_ambienceOfLights		, lightAmbient );
	GLWrapperUniform1fv( m_shader->m_innerLightRadius		, lightInnerRadius );
	GLWrapperUniform1fv( m_shader->m_outerLightRadius		, lightOuterRadius );
	GLWrapperUniform1fv( m_shader->m_intensityOutsidePenumbra, lightOutsidePenumbra );
}

//----------------------------------------------------------------------------------
void Material::addTextures( Texture* diffuse, Texture* normal, Texture* specular, Texture* emissive )
{
	m_textureMaps.push_back( diffuse );
	m_textureMaps.push_back( normal );
	m_textureMaps.push_back( specular );
	m_textureMaps.push_back( emissive );
}

//----------------------------------------------------------------------------------
void Material::setFog(float fogStart, float fogEnd, RGBAColors& fogColorAndIntensity)
{
	m_fogStartDistance = fogStart;
	m_fogEndDistance = fogEnd;
	m_fogColorAndIntensity = fogColorAndIntensity;
}
