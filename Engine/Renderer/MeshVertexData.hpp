#pragma once

#include "Engine\Math\Vec3.hpp"
#include "Engine\Core\RGBAColors.hpp"
#include "Engine\Math\Vec2.hpp"
#include <vector>
#include <unordered_map>
#include <functional>

struct ModelVertex
{
	ModelVertex()
	{
		m_pos = Vec3(0.0f, 0.0f, 0.0f);
		m_color = RGBAColors(255,255,255,255);
		m_texCoords = Vec2(0.0f, 0.0f);
		m_normal = Vec3(0.0f, 0.0f, 0.0f);
		m_tangent = Vec3(0.0f, 0.0f, 0.0f);
		m_bitangent = Vec3(0.0f, 0.0f, 0.0f);
	};

	void setModelVertex( 
		Vec3&			pos,
		RGBAColors&		color,
		Vec2&			texCoords,
		Vec3&			normal,
		Vec3&			tangent,
		Vec3&			bitangent
		);

	bool operator==(const ModelVertex& rhs)
	{ 
		return	m_pos == rhs.m_pos
			&&	m_color == rhs.m_color
			&&	m_texCoords == rhs.m_texCoords
			&&	m_normal ==	rhs.m_normal
			&&	m_tangent == rhs.m_tangent
			&&	m_bitangent	== rhs.m_bitangent;
	};

	bool operator==(const ModelVertex& rhs) const
	{ 
		return	m_pos == rhs.m_pos
			&&	m_color == rhs.m_color
			&&	m_texCoords == rhs.m_texCoords
			&&	m_normal ==	rhs.m_normal
			&&	m_tangent == rhs.m_tangent
			&&	m_bitangent	== rhs.m_bitangent;
	};

	Vec3 m_pos;
	RGBAColors m_color;
	Vec2 m_texCoords;
	Vec3 m_normal;
	Vec3 m_tangent;
	Vec3 m_bitangent;
};

class MeshVertexData
{
public:
	MeshVertexData(void);
	~MeshVertexData(void);

	void addSelfToBufferAsBytes( std::vector<unsigned char>& buffer, int subType );

	std::vector<ModelVertex> m_vertexes;
	std::vector<Vec3> m_normals;
	std::vector<Vec2> m_textureCoords;
	std::vector<int> m_indexes;
	std::vector<int> m_textureIndexes;
	std::vector<int> m_normalIndexes;

	void clear();
};

template <class T>
class MyHash;

template<>
class MyHash<ModelVertex>
{
public:
	std::size_t operator()(ModelVertex const& MV) const 
	{

		size_t position = ((std::hash<float>()(MV.m_pos.x)
			^ (std::hash<float>()(MV.m_pos.y) << 1)) >> 1)
			^ (std::hash<float>()(MV.m_pos.z) << 1);

		size_t tex = ((std::hash<float>()(MV.m_texCoords.x)
			^ (std::hash<float>()(MV.m_texCoords.y) << 1)) >> 1);

		size_t color = ((std::hash<unsigned char>()(MV.m_color.m_red)
			^ (std::hash<unsigned char>()(MV.m_color.m_green) << 1)) >> 1)
			^ ((std::hash<unsigned char>()(MV.m_color.m_blue) << 1)
			^ (std::hash<unsigned char>()(MV.m_color.m_alpha >> 1)) << 1);

		size_t normal = ((std::hash<float>()(MV.m_normal.x)
			^ (std::hash<float>()(MV.m_normal.y) << 1)) >> 1)
			^ (std::hash<float>()(MV.m_normal.z) << 1);

		size_t bitan = ((std::hash<float>()(MV.m_bitangent.x)
			^ (std::hash<float>()(MV.m_bitangent.y) << 1)) >> 1)
			^ (std::hash<float>()(MV.m_bitangent.z) << 1);

		size_t tangent = ((std::hash<float>()(MV.m_tangent.x)
			^ (std::hash<float>()(MV.m_tangent.y) << 1)) >> 1)
			^ (std::hash<float>()(MV.m_tangent.z) << 1);
		
		return position ^ (tex ^ ( color << 1 )) ^ (( normal >> 1 ) ^ ((bitan) ^ (tangent << 1)));
	}
};