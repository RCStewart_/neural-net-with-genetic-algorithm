#pragma once
#include <vector>
#include <string>
#include "Engine\Core\RGBAColors.hpp"

class Texture;
class ShaderProgram;
class Shader;

using namespace std;
class Material
{
public:
	Material(void);
	Material(const string& fragShaderPath, const string& vertexShaderPath, int& nextAttribIDNumber );
	~Material(void);

	void createProgramID();

	Shader* createFragmentShader( const string& filePath );
	Shader* createVertexShader( const string& filePath );

	void bindShadersToProgramAndLink(const string& fragmentFilePath, const string& vertexFilePath, int& nextAttribIDNum );
	void useProgram();
	void disableShaderProgram();

	void storeShaderUniformLocations();
	void setShaderUniforms();

	void addTextures( Texture* diffuse, Texture* normal, Texture* specular, Texture* emissive );
	void setFog( float fogStart, float fogEnd, RGBAColors& fogColorAndIntensity );

	vector<Texture*> m_textureMaps;
	ShaderProgram* m_shader;

	float		m_fogStartDistance;
	float		m_fogEndDistance;
	RGBAColors	m_fogColorAndIntensity;
};

