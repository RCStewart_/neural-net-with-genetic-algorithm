#include "MeshVertexData.hpp"
#include "Engine/Files/FileParser.hpp"

MeshVertexData::MeshVertexData(void)
{
}


MeshVertexData::~MeshVertexData(void)
{
}

void ModelVertex::setModelVertex( Vec3& pos, RGBAColors& color, Vec2& texCoords, Vec3& normal, Vec3& tangent, Vec3& bitangent )
{
	m_pos			= pos;
	m_color			= color;
	m_texCoords		= texCoords;
	m_normal		= normal;
	m_tangent		= tangent;
	m_bitangent		= bitangent;
}

void MeshVertexData::addSelfToBufferAsBytes( std::vector<unsigned char>& buffer, int subType )
{
	FileParser parser;

	if( subType > 1 )
	{
		parser.writeUIntToBuffer( buffer, ( unsigned int) m_indexes.size());

		for(int i = 0; i < (int) m_indexes.size(); ++i)
		{
			parser.writeUIntToBuffer(buffer, m_indexes[i]);
		}
	}

	parser.writeUIntToBuffer( buffer, ( unsigned int) m_vertexes.size());
	for(int i = 0; i < (int) m_vertexes.size(); ++i)
	{
		parser.writeVec3ToBuffer(buffer, m_vertexes[i].m_pos);
		parser.writeRGBAToBuffer(buffer, m_vertexes[i].m_color);
		parser.writeVec2ToBuffer(buffer, m_vertexes[i].m_texCoords);
		parser.writeVec3ToBuffer(buffer, m_vertexes[i].m_normal);
		parser.writeVec3ToBuffer(buffer, m_vertexes[i].m_tangent);
		parser.writeVec3ToBuffer(buffer, m_vertexes[i].m_bitangent);
	}
}

void MeshVertexData::clear()
{
	m_vertexes.clear();
	m_normals.clear();
	m_textureCoords.clear();
	m_indexes.clear();
	m_textureIndexes.clear();
	m_normalIndexes.clear();
}