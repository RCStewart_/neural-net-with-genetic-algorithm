#pragma once

#include <string>
#include <vector>
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/Vec3.hpp"

class Matrix_4x4;
class MeshVertexData;
class Texture;

class Mesh
{
public:
	Mesh(void);
	~Mesh(void);

	unsigned int m_vboID;
	unsigned int m_iboID;

	Texture* m_emissiveID;
	Texture* m_specularID;
	Texture* m_normalID;
	Texture* m_diffuseID;

	void loadTextures( std::string& filePath, std::string& fileName);

	bool loadMeshFromOBJFileAndCook( std::string& filePath, std::string& name, std::string& baseDataFolder, const std::string& directoryToWriteTo  );
	bool loadMeshFromC23File( std::string& filePath, std::string& name);

	void gatherVertexesFromOBJBufferAndCook( unsigned char* buffer, size_t sizeOfBuffer, std::string fileName, std::string filePath );
	void get3VertexNumbersForOBJ( unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void get3NormalNumbersForOBJ( unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void getTextureCoordsForOBJ( unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void parseOneLineForIndices( unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void seekToNonWhiteSpace( unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void switchOnOBJMetaData( unsigned char* buffer, int& index, MeshVertexData& storedVertexes, Matrix_4x4& changeOfBasis );
	void switchOnOBJGrimGrumMetaData( unsigned char* buffer, int& index, MeshVertexData& storedVertexes, Matrix_4x4& changeOfBasis );
	void seekToNextNonSpaceAndNonDigit( unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void seekToNextCharOfType( char type, unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void seekToWhiteSpace( unsigned char* buffer, int& index, MeshVertexData& storedVertexes );
	void seekUntilYouReachOneOfTheseChars( unsigned char* buffer, int& index, MeshVertexData& storedVertexes, std::vector<char>& chars, bool includeIsSpace );
	void addNumberToIndexes( int sizeOfVectorToBeIndexed, std::vector<int>& storedIndices, int indexToAdd );
	int convertStringToInt( std::string& toConvert );

	int m_numIndexes;
	int m_numOfVertexes;
};

