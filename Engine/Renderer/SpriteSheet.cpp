#include "SpriteSheet.hpp"
#include "Engine/Files/FileSystem.hpp"
#include "Engine/Renderer/Renderer.hpp"


//---------------------------------------------------------------------------
SpriteSheet::SpriteSheet(void)
{
	m_texture = nullptr;
}


//---------------------------------------------------------------------------
SpriteSheet::~SpriteSheet(void)
{
	if(m_texture != nullptr)
	{
		delete m_texture;
	}
}


//---------------------------------------------------------------------------
SpriteSheet::SpriteSheet(string& fileName, int spritesWide, int spritesHigh)
{
	m_imageFileName = fileName;
	m_texture = new Texture(m_imageFileName);
	m_spritesWide = spritesWide;
	m_spritesHigh = spritesHigh;
	m_spriteWidth = (float)(1.f / (float) spritesWide);
	m_spriteHeight = (float)(1.f / (float) spritesHigh);
}


//---------------------------------------------------------------------------
Vec2 SpriteSheet::getMinTextureCoordsForTile(IntVec2& XYCoords)
{
	float minX = ((float) XYCoords.x) * m_spriteWidth;
	float minY = ((float) XYCoords.y) * m_spriteHeight;
	return Vec2(minX, minY);
}


//---------------------------------------------------------------------------
Vec2 SpriteSheet::getMaxTextureCoordsForTile(IntVec2& XYCoords)
{
	float maxX = ((float) XYCoords.x) * m_spriteWidth + m_spriteWidth;
	float maxY = ((float) XYCoords.y) * m_spriteHeight + m_spriteHeight;
	return Vec2(maxX, maxY);
}


//---------------------------------------------------------------------------
IntVec2 SpriteSheet::getTileCoordsForIndex(int index)
{
	int xCoord = index % m_spritesWide;
	int yCoord = (int) (index / m_spritesWide);
	return IntVec2(xCoord, yCoord);
}

//----------------------------------------------------------------------------------
void SpriteSheet::sLoadAllSpriteSheets(const std::string& filePath)
{
	if (!g_theFileSystem)
	{
		return;
	}

	std::vector<std::string> ssFiles;
	g_theFileSystem->findAllFilesInDirectory(ssFiles, filePath + "*.ss.png");

	for (size_t fileIndex = 0; fileIndex < ssFiles.size(); ++fileIndex)
	{
		if (g_theRenderer)
		{
			g_theRenderer->getSpriteSheetIDFromFileName(filePath + ssFiles[fileIndex]);
		}
	}
}