//---------------------------------------------------------------------------
#include "Engine\Renderer\Shader.hpp"
#include "Engine\Core\UtilityFunctions.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine/Files/FileSystem.hpp"

#define STATIC // Do-nothing indicator that method/member is static in class definition


//---------------------------------------------------------------------------
STATIC std::map< std::string, Shader* >	Shader::s_ShaderRegistry;


//---------------------------------------------------------------------------
Shader::Shader( const std::string& shaderFilePath, GL_TYPES shaderType )
	: m_shaderID( 0 )
{
	m_filePath = shaderFilePath;
	m_shaderType = shaderType;

	FileBuffer buffer;
	if (!g_theFileSystem)
	{
		g_theFileSystem = new FileSystem(nullptr);
	}
	g_theFileSystem->loadFile( shaderFilePath, buffer, false);
	//buffer.buffer[buffer.size] = 0;
	unsigned int* numberOfBytes = new unsigned int;
	numberOfBytes[0] = buffer.size;
	GLWrapperGenerateShader(m_shaderID, shaderFilePath, m_shaderType, buffer.buffer, numberOfBytes);
	delete[] numberOfBytes;
}


//---------------------------------------------------------------------------
// Returns a pointer to the already-loaded Shader of a given image file,
//	or nullptr if no such Shader has been loaded.
STATIC Shader* Shader::GetShaderByName( const std::string& imageFilePath )
{
	Shader* loadedShader = nullptr;
	bool isShaderLoaded = false;

	isShaderLoaded = (s_ShaderRegistry.find(imageFilePath) != Shader::s_ShaderRegistry.end());

	if( isShaderLoaded )
	{
		loadedShader = s_ShaderRegistry[imageFilePath];
	}

	return loadedShader;
}


//---------------------------------------------------------------------------
// Finds the named Shader among the registry of those already loaded; if
//	found, returns that Shader*.  If not, attempts to load that Shader,
//	and returns a Shader* just created (or nullptr if unable to load file).
STATIC Shader* Shader::CreateOrGetShader( const std::string& shaderFilePath, GL_TYPES shaderType )
{
	Shader* loadedShader = nullptr;
	loadedShader = GetShaderByName(shaderFilePath);

	if( loadedShader == nullptr )
	{
		Shader::s_ShaderRegistry[shaderFilePath] = new Shader( shaderFilePath, shaderType );
		loadedShader = s_ShaderRegistry[shaderFilePath];
	}

	return loadedShader;
}