#pragma once

#include "Engine\Core\WindowsWrapper.hpp"

#include <gl/gl.h>
#include "GL/glu.h"
#include "glext.h"

#include <cstdio>

#include <vector>
#include <string>
#include <iostream>
#include <map>
#include "Engine\Math\Vec2.hpp"
#include "Engine\Math\Vec3.hpp"
#include "Engine\Core\RGBAColors.hpp"

#pragma comment( lib, "opengl32" ) 

class SpriteSheet;
class RGBAColors;
class Shader;
class Material;
class FBO;
struct ModelVertex;
class Mesh;

//// Put externs for these globals in some header that everyone (who cares about drawing) can see
extern PFNGLGENBUFFERSPROC				glGenBuffers;
extern PFNGLBINDBUFFERPROC				glBindBuffer;
extern PFNGLBUFFERDATAPROC				glBufferData;
extern PFNGLGENERATEMIPMAPPROC			glGenerateMipmap;
extern PFNGLDELETEBUFFERSPROC			glDeleteBuffers;

extern	PFNGLCREATESHADERPROC			glCreateShader;
extern	PFNGLSHADERSOURCEPROC			glShaderSource;
extern	PFNGLCOMPILESHADERPROC			glCompileShader;
extern	PFNGLGETSHADERIVPROC			glGetShaderiv;
extern	PFNGLCREATEPROGRAMPROC			glCreateProgram;
extern	PFNGLATTACHSHADERPROC			glAttachShader;
extern	PFNGLLINKPROGRAMPROC			glLinkProgram;
extern	PFNGLGETPROGRAMIVPROC			glGetProgramiv;
extern	PFNGLUSEPROGRAMPROC				glUseProgram;
extern	PFNGLACTIVETEXTUREPROC			glActiveTexture;
extern	PFNGLGETUNIFORMLOCATIONPROC		glGetUniformLocation;
extern	PFNGLUNIFORM1FPROC				glUniform1f;
extern	PFNGLUNIFORM3FPROC				glUniform3f;
extern	PFNGLUNIFORM4FPROC				glUniform4f;
extern	PFNGLUNIFORM1FVPROC				glUniform1fv;
extern	PFNGLUNIFORM3FVPROC				glUniform3fv;
extern	PFNGLUNIFORM4FVPROC				glUniform4fv;
extern	PFNGLUNIFORM1IPROC				glUniform1i;
extern	PFNGLGETSHADERINFOLOGPROC		glGetShaderInfoLog;
extern	PFNGLGETPROGRAMINFOLOGPROC		glGetProgramInfoLog;

extern	PFNGLVERTEXATTRIBPOINTERPROC		glVertexAttribPointer;
extern	PFNGLBINDATTRIBLOCATIONPROC			glBindAttribLocation;
extern	PFNGLGETATTRIBLOCATIONPROC			glGetAttribLocation;
extern	PFNGLENABLEVERTEXATTRIBARRAYPROC	glEnableVertexAttribArray;
extern	PFNGLDISABLEVERTEXATTRIBARRAYPROC	glDisableVertexAttribArray;
extern	PFNGLUNIFORMMATRIX4FVPROC			glUniformMatrix4fv;
extern	PFNGLUSEPROGRAMOBJECTARBPROC		glUseProgramObjectARB;
extern	PFNGLGENFRAMEBUFFERSPROC			glGenFramebuffers;
extern	PFNGLBINDFRAMEBUFFERPROC			glBindFramebuffer;
extern	PFNGLFRAMEBUFFERTEXTURE2DPROC		glFramebufferTexture2D;

extern void InitializeAdvancedOpenGLFunctions();

struct Vertex3D_PCT
{
	Vec3		m_position;
	RGBAColors	m_color;
	Vec2		m_texCoords;
	Vec2		m_perlinCoords;

	Vec3		m_tangent;
	Vec3		m_biTangent;
	Vec3		m_normal;
};

struct Vertex2D_PCT
{
	Vec3		m_position;
	RGBAColors	m_color;
	Vec2		m_texCoords;
};

using namespace std;
class GLWrapper
{
public:
	GLWrapper(void);
	~GLWrapper(void);
};

extern enum GL_TYPES
{	
	GL_WRAPPER_CULL_FACE = 0x0B44,
	GL_WRAPPER_BACK = 0x0405,
	GL_WRAPPER_TEXTURE_2D = 0x0DE1,
	GL_WRAPPER_QUADS = 0x0007,
	GL_WRAPPER_PROGRAM_POINT_SIZE = 0x8642,
	GL_WRAPPER_POINTS = 0x0000,
	GL_WRAPPER_DEPTH_TEST = 0x0B71,
	GL_WRAPPER_BLEND = 0x0BE2,
	GL_WRAPPER_ALPHA_TEST = 0x0BC0,
	GL_WRAPPER_ONE_MINUS_DST_COLOR = 0x0307,
	GL_WRAPPER_ZERO = 0,
	GL_WRAPPER_SRC_ALPHA = 0x0302, 
	GL_WRAPPER_ONE_MINUS_SRC_ALPHA = 0x0303,
	GL_WRAPPER_ONE = 1,
	GL_WRAPPER_DRAW_LINES = 0x0001,
	GL_WRAPPER_FRAGMENT_SHADER = 0x8B30,
	GL_WRAPPER_VERTEX_SHADER = 0x8B31,
	GL_WRAPPER_LINE_LOOP = 0x0002,
	GL_WRAPPER_LINE_STRIP = 0x0003,
	GL_WRAPPER_TRIANGLES = GL_TRIANGLES
};

std::string GLWrapperGetOpenGLAndGLSLVersion();
void GLWrapperEnableVertexAttribArrayTextCoords( int locationID, vector<Vec2>& textCoords );
void GLWrapperEnableTexture( int textureID, int whereToBindTexture );
void GLWrapperCheckForShaderProgramLinkErrors(int& programID,  Shader* fragment, Shader* vertex );
void GLWrapperUniform1f( int locationID, float value );
void GLWrapperUniform3f( int locationID, Vec3& value );
void GLWrapperUniform4f( int locationID, RGBAColors& value );

void GLWrapperUniform1fv( int locationID, vector<float>& input);
void GLWrapperUniform3fv( int locationID, vector<Vec3>& input);
void GLWrapperUniform4fv( int locationID, vector<RGBAColors>& input);

void GLWrapperUniform1i( int locationID, int value );

int GLWrapperGetShaderUniformLocation( int programShaderID, string& name );
void GLWrapperUseProgram( int programID );
void GLWrapperLinkProgram( int programID );
int GLWrapperGenerateShaderProgram();

void GLWrapperEnableShaderTextures( int emissiveTextureID, int specularTextureID, int normalTextureID, int diffuseTextureID );
void GLWrapperAttachShader( int programID, int shaderID );
void GLWrapperGenerateShader( unsigned int& generatedShaderID, const std::string& shaderFilePath, GL_TYPES shaderType, unsigned char* shaderBuffer, unsigned int* numOfBytes );

void GLWrapperDrawTexturedSphere(float radius, unsigned int rings, unsigned int sectors, Vec3 position, int textureID);

void GLWrapperGenerateTexture( unsigned int& generatedOutputedID, int& sizeX, int& sizeY, const std::string& imageFilePath );
void GLWrapperGenerateTexture( unsigned int& generatedOutputedID, int& sizeX, int& sizeY, unsigned char* data );
void GLWrapperGenerateVBO( unsigned int& vboID, vector<Vertex3D_PCT>& vertexes );
void GLWrapperGenerateVBO( unsigned int& vboID, vector<ModelVertex>& vertexes );

void GLWrapperRenderModernVBO(unsigned int& vboID, int numberOfVertexes, Vec3& translate, Material* material, GL_TYPES typeOfDrawing );

void GLWrapperRenderModernVBO( unsigned int& vboID, vector<Vertex3D_PCT>& vertexes, Vec3& translateXYZ );
void GLWrapperDeleteVBOBuffers( int number, unsigned int& vboID );

void GLWrapperDrawVectorOfPoints( const vector<Vec3>& points, const vector<RGBAColors>& color, const Vec3& translation, int size );
void GLWrapperColorBytes( const RGBAColors& color );
void GLWrapperColorFloat( const Vec3& color );
void GLWrapperVertex3f( const Vec3& vertex );
void GLWrapperTranslate( const Vec3& translation );
void GLWrapperRotate( const Vec3& rotation, float angle );
void GLWrapperScale( const Vec3& scale );

void GLWrapperEnableGLType( GL_TYPES toEnable );
void GLWrapperDisableGLType( GL_TYPES toDisable );
void GLWrapperEnableGLCullFace( GL_TYPES cullFace );
void GLWrapperBindGLTexture( GL_TYPES type, unsigned int textureID );
void GLWrapperSetUpPerspectiveProjection();
void GLWrapperSetUpInitialPerspectiveProjection();

void GLWrapperEnableGLWrapperOrtho();
void GLWrapperDisableGLWrapperOrtho();

void GLWrapperEnableModernOrtho();
void GLWrapperDisableModernOrtho();

void GLWrapperDrawLines( vector<Vec3>& pointPairs, vector<RGBAColors>& colorPerLine, Vec3& translation );
void GLWrapperDrawLines( vector<Vertex3D_PCT>& vertexes, Vec3& translation );

void GLWrapperDrawQuads( vector<Vec3>& points, vector<RGBAColors>& colorPerLine, Vec3& translation );
void GLWrapperDrawTexturedQuads( vector<Vec2>& textureCoords, vector<Vec3>& points, vector<RGBAColors>& colorPerLine, Vec3& translation, SpriteSheet* sprites );

void GLWrapperPushMatrix();
void GLWrapperPopMatrix();

void GLWrapperBegin( GL_TYPES toBegin );
void GLWrapperEnd();

void GLWrapperVertex2f(float x, float y);
void GLWrapperTexCoord2f(float x, float y);

void GLWrapperPointSize(int size);
void GLWrapperSwapBuffers(HDC displayDeviceContext);

void GLWrapperEnableBlending();

void GLWrapperSetDepthTestToFalse();
void GLWrapperSetDepthTestToTrue();

void GLWrapperBlendMode(GL_TYPES one, GL_TYPES two);

void GLWrapperVertexArray( const vector<Vertex3D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, int textureID = 0 );
void GLWrapperVertexArray( const vector<Vertex2D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, int textureID = 0 );
void GLWrapperVertexArray( const vector<ModelVertex>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, int textureID = 0 );

void GLWrapperLineWidth(float newWidth);
void GLWrapperDrawSphere(const Vec3& translation, const RGBAColors& color, float radius);
void GLWrappercircleTableSphere(double **sint,double **cost,const int n);

void GLWModernVertexArray( const vector<Vertex3D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, Material* material );
void GLWModernVertexArray( const vector<ModelVertex>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, Material* material );
void GLWrapperFBOVertexArray( const vector<Vertex3D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, FBO* fbo );
void GLWModernSphere( float radius, unsigned int rings, unsigned int sectors, Vec3 position, Material* material );
void GLWrapperBindAttribLocation( int program, int index, const char* name );
void GLWrapperEnableAttribArray( int ID );
int GLWrapperGetTexture0Value();
void GLWrapperDisableShader();

void GLWrapperInitFBOColorTexture( unsigned int& textureID );
void GLWrapperInitFBODepthTexture( unsigned int& textureID );
void GLWrapperGenerateFBO( unsigned int& fboID, int colorBufferID, int depthBufferID );
void GLWrapperBindFramebuffer( int fboID );

void GLWrapperGenerateIBO(unsigned int& iboID, vector<int>& vertexOrder);
void GLWrapperRenderModernVBOWithIBO(unsigned int& vboID, unsigned int& iboID, int numberOfIndices, Vec3& translation, Material* material, GL_TYPES typeOfDrawing );
void GLWrapperRenderModernVBOWithIBO(Mesh& mesh, Vec3& translation, Material* material, GL_TYPES typeOfDrawing );