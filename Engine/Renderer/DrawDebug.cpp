#include "Engine\Renderer\DrawDebug.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Math\IntVec2.hpp"
#include "Engine\Renderer\SpriteSheet.hpp"
#include "Engine\Core\UtilityFunctions.hpp"
#include <string>
#include "Engine\Math\Time.hpp"
#include "Engine\Core\GameCommon.hpp"
#include "Engine\Renderer\Renderer.hpp"

#include "Engine\Core\WindowsWrapper.hpp" //For WindowWrapperDebugPrintf

DrawDebug::DrawDebug(void)
{

}


DrawDebug::~DrawDebug(void)
{
} 

//----------------------------------------------------------------------------------
void DrawDebug::renderWithDepth()
{
	for(int i = m_points.size() - 1; i >= 0; --i)
	{
		DebugPoint& p = m_points[i];
		if(p.mode != DEBUG_MODE_NO_DEPTHTEST)
		drawPoint( p.position, p.color, p.radius, p.mode, p.duration, p.startTime, i );
	}

	for(int i = m_lineSegs.size() - 1; i >= 0; --i)
	{
		DebugLine& p = m_lineSegs[i];
		if(p.mode != DEBUG_MODE_NO_DEPTHTEST)
			drawLineSegment( p.startPos, p.startCol, p.endPos, p.endCol, p.mode, p.duration, p.startTime, i );
	}

	for(int i = m_spheres.size() - 1; i >= 0; --i)
	{
		DebugSphere& p = m_spheres[i];
		if(p.mode != DEBUG_MODE_NO_DEPTHTEST)
			drawSphere( p.position, p.color, p.radius, p.mode, p.duration, p.startTime, i );
	}

	for(int i = m_AABBs.size() - 1; i >= 0; --i)
	{
		DebugAABB& p = m_AABBs[i];
		if(p.mode != DEBUG_MODE_NO_DEPTHTEST)
			drawAABB( p.min, p.max, p.faceColor, p.lineColor, p.mode, p.duration, p.startTime, i );
	}
}

//----------------------------------------------------------------------------------
void DrawDebug::renderNoDepth()
{
	for(int i = m_AABBs.size() - 1; i >= 0; --i)
	{
		DebugAABB& p = m_AABBs[i];
		if(p.mode == DEBUG_MODE_NO_DEPTHTEST)
			drawAABB( p.min, p.max, p.faceColor, p.lineColor, p.mode, p.duration, p.startTime, i );
	}

	for(int i = m_points.size() - 1; i >= 0; --i)
	{
		DebugPoint& p = m_points[i];
		if(p.mode == DEBUG_MODE_NO_DEPTHTEST)
			drawPoint( p.position, p.color, p.radius, p.mode, p.duration, p.startTime, i );
	}

	for(int i = m_lineSegs.size() - 1; i >= 0; --i)
	{
		DebugLine& p = m_lineSegs[i];
		if(p.mode == DEBUG_MODE_NO_DEPTHTEST)
			drawLineSegment( p.startPos, p.startCol, p.endPos, p.endCol, p.mode, p.duration, p.startTime, i );
	}

	for(int i = m_spheres.size() - 1; i >= 0; --i)
	{
		DebugSphere& p = m_spheres[i];
		if(p.mode == DEBUG_MODE_NO_DEPTHTEST)
			drawSphere( p.position, p.color, p.radius, p.mode, p.duration, p.startTime, i );
	}
}

//----------------------------------------------------------------------------------
void DrawDebug::initiatePoint( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration /* = 0.f */ )
{
	if(mode == DEBUG_MODE_DUAL)
	{
		m_points.push_back(DebugPoint());
		m_points[m_points.size() - 1].radius = radius;
		m_points[m_points.size() - 1].position = position;
		m_points[m_points.size() - 1].color = color;
		m_points[m_points.size() - 1].duration = duration;
		m_points[m_points.size() - 1].mode = DEBUG_MODE_NO_DEPTHTEST;
		m_points[m_points.size() - 1].startTime = (float) GetCurrentTimeSeconds();
		mode = DEBUG_MODE_DEPTHTEST;
	}

	m_points.push_back(DebugPoint());
	m_points[m_points.size() - 1].radius = radius;
	m_points[m_points.size() - 1].position = position;
	m_points[m_points.size() - 1].color = color;
	m_points[m_points.size() - 1].duration = duration;
	m_points[m_points.size() - 1].mode = mode;
	m_points[m_points.size() - 1].startTime = (float) GetCurrentTimeSeconds();
}

//----------------------------------------------------------------------------------
void DrawDebug::initiateSphere( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration /* = 0.f */ )
{
	if(mode == DEBUG_MODE_DUAL)
	{
		m_spheres.push_back(DebugSphere());
		m_spheres[m_spheres.size() - 1].radius = radius;
		m_spheres[m_spheres.size() - 1].position = position;
		m_spheres[m_spheres.size() - 1].color = color;
		m_spheres[m_spheres.size() - 1].duration = duration;
		m_spheres[m_spheres.size() - 1].mode = DEBUG_MODE_NO_DEPTHTEST;
		m_spheres[m_spheres.size() - 1].startTime = (float) GetCurrentTimeSeconds();
		mode = DEBUG_MODE_DEPTHTEST;
	}

	m_spheres.push_back(DebugSphere());
	m_spheres[m_spheres.size() - 1].radius = radius;
	m_spheres[m_spheres.size() - 1].position = position;
	m_spheres[m_spheres.size() - 1].color = color;
	m_spheres[m_spheres.size() - 1].duration = duration;
	m_spheres[m_spheres.size() - 1].mode = mode;
	m_spheres[m_spheres.size() - 1].startTime = (float) GetCurrentTimeSeconds();
}

//----------------------------------------------------------------------------------
void DrawDebug::initiateAABB( const Vec3& minPoint, const Vec3& maxPoint, const RGBAColors& faceColor, const RGBAColors& lineColor, DEBUG_MODE mode, float duration )
{
	if(mode == DEBUG_MODE_DUAL)
	{
		m_AABBs.push_back(DebugAABB());
		m_AABBs[m_AABBs.size() - 1].min = minPoint;
		m_AABBs[m_AABBs.size() - 1].max = maxPoint;
		m_AABBs[m_AABBs.size() - 1].faceColor = faceColor;
		m_AABBs[m_AABBs.size() - 1].lineColor = lineColor;
		m_AABBs[m_AABBs.size() - 1].duration = duration;
		m_AABBs[m_AABBs.size() - 1].mode = DEBUG_MODE_NO_DEPTHTEST;
		m_AABBs[m_AABBs.size() - 1].startTime = (float) GetCurrentTimeSeconds();
		mode = DEBUG_MODE_DEPTHTEST;
	}

	m_AABBs.push_back(DebugAABB());
	m_AABBs[m_AABBs.size() - 1].min = minPoint;
	m_AABBs[m_AABBs.size() - 1].max = maxPoint;
	m_AABBs[m_AABBs.size() - 1].faceColor = faceColor;
	m_AABBs[m_AABBs.size() - 1].lineColor = lineColor;
	m_AABBs[m_AABBs.size() - 1].duration = duration;
	m_AABBs[m_AABBs.size() - 1].mode = mode;
	m_AABBs[m_AABBs.size() - 1].startTime = (float) GetCurrentTimeSeconds();
}

//----------------------------------------------------------------------------------
void DrawDebug::initiateLineSegment( const Vec3& startPosition, const RGBAColors& startColor, const Vec3& endPosition, const RGBAColors& endColor, DEBUG_MODE mode, float duration )
{
	if(mode == DEBUG_MODE_DUAL)
	{
		m_lineSegs.push_back(DebugLine());
		m_lineSegs[m_lineSegs.size() - 1].startPos = startPosition;
		m_lineSegs[m_lineSegs.size() - 1].endPos = endPosition;
		m_lineSegs[m_lineSegs.size() - 1].startCol = startColor;
		m_lineSegs[m_lineSegs.size() - 1].endCol = endColor;
		m_lineSegs[m_lineSegs.size() - 1].duration = duration;
		m_lineSegs[m_lineSegs.size() - 1].mode = DEBUG_MODE_NO_DEPTHTEST;
		m_lineSegs[m_lineSegs.size() - 1].startTime = (float) GetCurrentTimeSeconds();
		mode = DEBUG_MODE_DEPTHTEST;
	}

	m_lineSegs.push_back(DebugLine());
	m_lineSegs[m_lineSegs.size() - 1].startPos = startPosition;
	m_lineSegs[m_lineSegs.size() - 1].endPos = endPosition;
	m_lineSegs[m_lineSegs.size() - 1].startCol = startColor;
	m_lineSegs[m_lineSegs.size() - 1].endCol = endColor;
	m_lineSegs[m_lineSegs.size() - 1].duration = duration;
	m_lineSegs[m_lineSegs.size() - 1].mode = mode;
	m_lineSegs[m_lineSegs.size() - 1].startTime = (float) GetCurrentTimeSeconds();
}

//----------------------------------------------------------------------------------
void DrawDebug::initiateArrow( const Vec3& startPosition, const Vec3& endPosition, const RGBAColors& color, DEBUG_MODE mode, float duration )
{
	initiatePoint(endPosition, color, .5f, mode, duration );
	initiateLineSegment( startPosition, color, endPosition, color, mode, duration );
}

//----------------------------------------------------------------------------------
void DrawDebug::drawAxis()
{
	Vec3 translation(0.0f, 0.0f, 0.0f);
	GLWrapperLineWidth(1.f);
	vector<Vertex3D_PCT> vertexes;
	
	vertexes.push_back(Vertex3D_PCT());
	vertexes.push_back(Vertex3D_PCT());
	vertexes.push_back(Vertex3D_PCT());
	vertexes.push_back(Vertex3D_PCT());
	vertexes.push_back(Vertex3D_PCT());
	vertexes.push_back(Vertex3D_PCT());

	vertexes[0].m_color = RGBAColors(255, 0, 0, 255);
	vertexes[1].m_color = RGBAColors(255, 0, 0, 255);
	vertexes[2].m_color = RGBAColors(0, 255, 0, 255);
	vertexes[3].m_color = RGBAColors(0, 255, 0, 255);
	vertexes[4].m_color = RGBAColors(0, 0, 255, 255);
	vertexes[5].m_color = RGBAColors(0, 0, 255, 255);

	vertexes[0].m_position = Vec3(10, 0, 0);
	vertexes[1].m_position = Vec3(0, 0, 0);
	vertexes[2].m_position = Vec3(0, 10, 0);
	vertexes[3].m_position = Vec3(0, 0, 0);
	vertexes[4].m_position = Vec3(0, 0, 10);
	vertexes[5].m_position = Vec3(0, 0, 0);

	GLWrapperVertexArray(vertexes, translation, GL_WRAPPER_DRAW_LINES);
}


//----------------------------------------------------------------------------------
void DrawDebug::drawCrosshair()
{
	GLWrapperBlendMode( GL_WRAPPER_ONE_MINUS_DST_COLOR, GL_WRAPPER_ZERO );
	GLWrapperPushMatrix();
	GLWrapperTranslate(Vec3(800.f, 450.f, 0));

	vector<RGBAColors> colors;
	colors.push_back(RGBAColors(255, 255, 255, 255));
	colors.push_back(RGBAColors(255, 255, 255, 255));
	colors.push_back(RGBAColors(255, 255, 255, 255));
	colors.push_back(RGBAColors(255, 255, 255, 255));

	vector<Vec3> points;
	points.push_back(Vec3(10.f, 0.f, 0.f));
	points.push_back(Vec3(-10.f, 0.f, 0.f));
	points.push_back(Vec3(0.f, 10.f, 0.f));
	points.push_back(Vec3(0.f, -10.f, 0.f));

	Vec3 zeroVector(0.f, 0.f, 0.f);
	GLWrapperDrawLines(points, colors, zeroVector);
	GLWrapperPopMatrix();
	GLWrapperBlendMode( GL_WRAPPER_SRC_ALPHA, GL_WRAPPER_ONE_MINUS_SRC_ALPHA );
}


//----------------------------------------------------------------------------------
void DrawDebug::drawPoint( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration, float startTime, int index)
{
	RGBAColors tempColor = color;
	GLWrapperLineWidth(2.0f);
	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperLineWidth(.5f);
		GLWrapperDisableGLType(GL_WRAPPER_DEPTH_TEST);
		tempColor.m_blue	= (unsigned char)(tempColor.m_blue	* .25);
		tempColor.m_red 	= (unsigned char)(tempColor.m_red	* .25);
		tempColor.m_green 	= (unsigned char)(tempColor.m_green	* .25);
	}
	float rootOfRadius = sqrt(radius * radius * .5f);

	vector<Vertex3D_PCT> vertexes;

	for(int i = 0; i < 24; ++i)
	{
		vertexes.push_back(Vertex3D_PCT());
	}

	for(int i = 0; i < 24; ++i)
	{
		vertexes[i].m_position = position;
		vertexes[i].m_color = tempColor;
	}

	vertexes[0].m_position.x += rootOfRadius;
	vertexes[2].m_position.y += rootOfRadius;
	vertexes[4].m_position.x -= rootOfRadius;
	vertexes[6].m_position.y -= rootOfRadius;
	vertexes[8].m_position.x += rootOfRadius;
	vertexes[10].m_position.y += rootOfRadius;
	vertexes[12].m_position.x -= rootOfRadius;
	vertexes[14].m_position.y -= rootOfRadius;

	vertexes[0].m_position.z += rootOfRadius;
	vertexes[2].m_position.z += rootOfRadius;
	vertexes[4].m_position.z += rootOfRadius;
	vertexes[6].m_position.z += rootOfRadius;
	vertexes[8].m_position.z -= rootOfRadius;
	vertexes[10].m_position.z -= rootOfRadius;
	vertexes[12].m_position.z -= rootOfRadius;
	vertexes[14].m_position.z -= rootOfRadius;

	vertexes[16].m_position.x += rootOfRadius;
	vertexes[16].m_position.y += rootOfRadius;
	vertexes[18].m_position.x -= rootOfRadius;
	vertexes[18].m_position.y -= rootOfRadius;

	vertexes[20].m_position.x += rootOfRadius;
	vertexes[20].m_position.y -= rootOfRadius;
	vertexes[22].m_position.x -= rootOfRadius;
	vertexes[22].m_position.y += rootOfRadius;

	Vec3 translation = Vec3(0.f, 0.f, 0.f);
	GLWrapperVertexArray(vertexes, translation, GL_WRAPPER_DRAW_LINES);
	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperEnableGLType(GL_WRAPPER_DEPTH_TEST);
	}
	if(startTime + duration <= GetCurrentTimeSeconds())
	{
		m_points[index] = m_points[m_points.size() - 1];
		m_points.pop_back();
	}
	GLWrapperLineWidth(1.f);
}

//----------------------------------------------------------------------------------
void DrawDebug::drawLineSegment( const Vec3& startPosition, const RGBAColors& startColor, const Vec3& endPosition, const RGBAColors& endColor, DEBUG_MODE mode, float duration, float startTime, int index )
{

	RGBAColors tempStartColor = startColor;
	RGBAColors tempEndColor = endColor;
	GLWrapperLineWidth(2.0f);
	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperLineWidth(.5f);
		GLWrapperDisableGLType(GL_WRAPPER_DEPTH_TEST);
		tempStartColor.m_blue	= (unsigned char)(tempStartColor.m_blue	* .25);
		tempStartColor.m_red	= (unsigned char)(tempStartColor.m_red	* .25);
		tempStartColor.m_green	= (unsigned char)(tempStartColor.m_green	* .25);

		tempEndColor.m_blue		= (unsigned char)(tempEndColor.m_blue	* .25);
		tempEndColor.m_red		= (unsigned char)(tempEndColor.m_red	* .25);
		tempEndColor.m_green	= (unsigned char)(tempEndColor.m_green	* .25);
	}
	Vec3 translation(0.0f, 0.0f, 0.0f);

	vector<Vertex3D_PCT> vertexes;

	vertexes.push_back(Vertex3D_PCT());
	vertexes.push_back(Vertex3D_PCT());

	vertexes[0].m_color = tempStartColor;
	vertexes[1].m_color = tempEndColor;

	vertexes[0].m_position = startPosition;
	vertexes[1].m_position = endPosition;

	GLWrapperVertexArray(vertexes, translation, GL_WRAPPER_DRAW_LINES);

	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperEnableGLType(GL_WRAPPER_DEPTH_TEST);
	}
	if(startTime + duration <= GetCurrentTimeSeconds())
	{
		m_lineSegs[index] = m_lineSegs[m_lineSegs.size() - 1];
		m_lineSegs.pop_back();
	}
	GLWrapperLineWidth(1.0f);
}


//----------------------------------------------------------------------------------
void DrawDebug::drawAABB( const Vec3& minPos, const Vec3& maxPos, const RGBAColors& faceColor, const RGBAColors& lineColor, DEBUG_MODE mode, float duration, float startTime, int index )
{
	RGBAColors tempLineColor = lineColor;
	RGBAColors tempFaceColor = faceColor;
	GLWrapperLineWidth(2.0f);
	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperLineWidth(.5f);
		GLWrapperDisableGLType(GL_WRAPPER_DEPTH_TEST);
		tempLineColor.m_blue	= (unsigned char)(tempLineColor.m_blue	* .25);
		tempLineColor.m_red		= (unsigned char)(tempLineColor.m_red	* .25);
		tempLineColor.m_green	= (unsigned char)(tempLineColor.m_green	* .25);

		tempFaceColor.m_blue	= (unsigned char)(tempFaceColor.m_blue	* .25);
		tempFaceColor.m_red		= (unsigned char)(tempFaceColor.m_red	* .25);
		tempFaceColor.m_green	= (unsigned char)(tempFaceColor.m_green	* .25);
		//tempFaceColor.m_alpha *= .50;
	}

	vector<Vertex3D_PCT> vertexes;
	Vec3 translation(0.0f, 0.0f, 0.0f);

	for(int i = 0; i < 24; ++i)
	{
		vertexes.push_back(Vertex3D_PCT());
	}

	for(int i = 0; i < 24; ++i)
	{
		vertexes[i].m_color = tempFaceColor;
	}

	//Bot
	vertexes[0].m_position.x = minPos.x;
	vertexes[0].m_position.y = minPos.y;
	vertexes[0].m_position.z = minPos.z;
			 
	vertexes[1].m_position.x = maxPos.x;
	vertexes[1].m_position.y = minPos.y;
	vertexes[1].m_position.z = minPos.z;
			 
	vertexes[2].m_position.x = maxPos.x;
	vertexes[2].m_position.y = maxPos.y;
	vertexes[2].m_position.z = minPos.z;
			 
	vertexes[3].m_position.x = minPos.x;
	vertexes[3].m_position.y = maxPos.y;
	vertexes[3].m_position.z = minPos.z;

	//Top
	vertexes[4].m_position.x = minPos.x;
	vertexes[4].m_position.y = minPos.y;
	vertexes[4].m_position.z = maxPos.z;

	vertexes[5].m_position.x = maxPos.x;
	vertexes[5].m_position.y = minPos.y;
	vertexes[5].m_position.z = maxPos.z;

	vertexes[6].m_position.x = maxPos.x;
	vertexes[6].m_position.y = maxPos.y;
	vertexes[6].m_position.z = maxPos.z;

	vertexes[7].m_position.x = minPos.x;
	vertexes[7].m_position.y = maxPos.y;
	vertexes[7].m_position.z = maxPos.z;

	//South
	vertexes[8].m_position.x = minPos.x;
	vertexes[8].m_position.y = minPos.y;
	vertexes[8].m_position.z = maxPos.z;

	vertexes[9].m_position.x = minPos.x;
	vertexes[9].m_position.y = minPos.y;
	vertexes[9].m_position.z = minPos.z;

	vertexes[10].m_position.x = maxPos.x;
	vertexes[10].m_position.y = minPos.y;
	vertexes[10].m_position.z = minPos.z;

	vertexes[11].m_position.x = maxPos.x;
	vertexes[11].m_position.y = minPos.y;
	vertexes[11].m_position.z = maxPos.z;

	//East
	vertexes[12].m_position.x = maxPos.x;
	vertexes[12].m_position.y = minPos.y;
	vertexes[12].m_position.z = maxPos.z;

	vertexes[13].m_position.x = maxPos.x;
	vertexes[13].m_position.y = minPos.y;
	vertexes[13].m_position.z = minPos.z;

	vertexes[14].m_position.x = maxPos.x;
	vertexes[14].m_position.y = maxPos.y;
	vertexes[14].m_position.z = minPos.z;

	vertexes[15].m_position.x = maxPos.x;
	vertexes[15].m_position.y = maxPos.y;
	vertexes[15].m_position.z = maxPos.z;

	//North
	vertexes[16].m_position.x = maxPos.x;
	vertexes[16].m_position.y = maxPos.y;
	vertexes[16].m_position.z = maxPos.z;

	vertexes[17].m_position.x = maxPos.x;
	vertexes[17].m_position.y = maxPos.y;
	vertexes[17].m_position.z = minPos.z;

	vertexes[18].m_position.x = minPos.x;
	vertexes[18].m_position.y = maxPos.y;
	vertexes[18].m_position.z = minPos.z;

	vertexes[19].m_position.x = minPos.x;
	vertexes[19].m_position.y = maxPos.y;
	vertexes[19].m_position.z = maxPos.z;

	//West
	vertexes[20].m_position.x = minPos.x;
	vertexes[20].m_position.y = maxPos.y;
	vertexes[20].m_position.z = maxPos.z;

	vertexes[21].m_position.x = minPos.x;
	vertexes[21].m_position.y = maxPos.y;
	vertexes[21].m_position.z = minPos.z;

	vertexes[22].m_position.x = minPos.x;
	vertexes[22].m_position.y = minPos.y;
	vertexes[22].m_position.z = minPos.z;

	vertexes[23].m_position.x = minPos.x;
	vertexes[23].m_position.y = minPos.y;
	vertexes[23].m_position.z = maxPos.z;

	if(mode == DEBUG_MODE_DEPTHTEST)
	GLWrapperVertexArray(vertexes, translation, GL_WRAPPER_QUADS);

	for(int i = 0; i < 24; ++i)
	{
		vertexes[i].m_color = tempLineColor;
	}

	//----------------------------------------------------------------------------------
	vertexes[0].m_position.x = minPos.x;
	vertexes[0].m_position.y = minPos.y;
	vertexes[0].m_position.z = minPos.z;

	vertexes[1].m_position.x = maxPos.x;
	vertexes[1].m_position.y = minPos.y;
	vertexes[1].m_position.z = minPos.z;

	//----------------------------------------------------------------------------------
	vertexes[2].m_position.x = maxPos.x;
	vertexes[2].m_position.y = minPos.y;
	vertexes[2].m_position.z = minPos.z;

	vertexes[3].m_position.x = maxPos.x;
	vertexes[3].m_position.y = maxPos.y;
	vertexes[3].m_position.z = minPos.z;

	//----------------------------------------------------------------------------------
	vertexes[4].m_position.x = maxPos.x;
	vertexes[4].m_position.y = maxPos.y;
	vertexes[4].m_position.z = minPos.z;

	vertexes[5].m_position.x = minPos.x;
	vertexes[5].m_position.y = maxPos.y;
	vertexes[5].m_position.z = minPos.z;

	//----------------------------------------------------------------------------------
	vertexes[6].m_position.x = minPos.x;
	vertexes[6].m_position.y = maxPos.y;
	vertexes[6].m_position.z = minPos.z;

	vertexes[7].m_position.x = minPos.x;
	vertexes[7].m_position.y = minPos.y;
	vertexes[7].m_position.z = minPos.z;

	//----------------------------------------------------------------------------------
	vertexes[8].m_position.x = minPos.x;
	vertexes[8].m_position.y = minPos.y;
	vertexes[8].m_position.z = minPos.z;

	vertexes[9].m_position.x = minPos.x;
	vertexes[9].m_position.y = minPos.y;
	vertexes[9].m_position.z = maxPos.z;

	//----------------------------------------------------------------------------------
	vertexes[10].m_position.x = maxPos.x;
	vertexes[10].m_position.y = minPos.y;
	vertexes[10].m_position.z = minPos.z;

	vertexes[11].m_position.x = maxPos.x;
	vertexes[11].m_position.y = minPos.y;
	vertexes[11].m_position.z = maxPos.z;

	//----------------------------------------------------------------------------------
	vertexes[12].m_position.x = maxPos.x;
	vertexes[12].m_position.y = maxPos.y;
	vertexes[12].m_position.z = minPos.z;

	vertexes[13].m_position.x = maxPos.x;
	vertexes[13].m_position.y = maxPos.y;
	vertexes[13].m_position.z = maxPos.z;

	//----------------------------------------------------------------------------------
	vertexes[14].m_position.x = minPos.x;
	vertexes[14].m_position.y = maxPos.y;
	vertexes[14].m_position.z = minPos.z;

	vertexes[15].m_position.x = minPos.x;
	vertexes[15].m_position.y = maxPos.y;
	vertexes[15].m_position.z = maxPos.z;

	//----------------------------------------------------------------------------------
	vertexes[16].m_position.x = minPos.x;
	vertexes[16].m_position.y = minPos.y;
	vertexes[16].m_position.z = maxPos.z;

	vertexes[17].m_position.x = maxPos.x;
	vertexes[17].m_position.y = minPos.y;
	vertexes[17].m_position.z = maxPos.z;

	//----------------------------------------------------------------------------------
	vertexes[18].m_position.x = maxPos.x;
	vertexes[18].m_position.y = minPos.y;
	vertexes[18].m_position.z = maxPos.z;

	vertexes[19].m_position.x = maxPos.x;
	vertexes[19].m_position.y = maxPos.y;
	vertexes[19].m_position.z = maxPos.z;

	//----------------------------------------------------------------------------------
	vertexes[20].m_position.x = maxPos.x;
	vertexes[20].m_position.y = maxPos.y;
	vertexes[20].m_position.z = maxPos.z;

	vertexes[21].m_position.x = minPos.x;
	vertexes[21].m_position.y = maxPos.y;
	vertexes[21].m_position.z = maxPos.z;

	//----------------------------------------------------------------------------------
	vertexes[22].m_position.x = minPos.x;
	vertexes[22].m_position.y = maxPos.y;
	vertexes[22].m_position.z = maxPos.z;

	vertexes[23].m_position.x = minPos.x;
	vertexes[23].m_position.y = minPos.y;
	vertexes[23].m_position.z = maxPos.z;

	GLWrapperVertexArray(vertexes, translation, GL_WRAPPER_DRAW_LINES);

	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperEnableGLType(GL_WRAPPER_DEPTH_TEST);
	}
	if(startTime + duration <= GetCurrentTimeSeconds())
	{
		m_AABBs[index] = m_AABBs[m_AABBs.size() - 1];
		m_AABBs.pop_back();
	}
	GLWrapperLineWidth(1.0f);
}

//----------------------------------------------------------------------------------
void DrawDebug::drawSphere( const Vec3& position, const RGBAColors& color, float radius, DEBUG_MODE mode, float duration, float startTime, int index )
{
	RGBAColors tempColor = color;
	GLWrapperLineWidth(2.0f);
	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperLineWidth(.5f);
		GLWrapperDisableGLType(GL_WRAPPER_DEPTH_TEST);
		tempColor.m_blue	= (unsigned char)(tempColor.m_blue	* .25);
		tempColor.m_red		= (unsigned char)(tempColor.m_red	* .25);
		tempColor.m_green	= (unsigned char)(tempColor.m_green	* .25);
	}

	GLWrapperDrawSphere( position, tempColor, radius );

	if(mode == DEBUG_MODE_NO_DEPTHTEST)
	{
		GLWrapperEnableGLType(GL_WRAPPER_DEPTH_TEST);
	}
	if(startTime + duration <= GetCurrentTimeSeconds())
	{
		m_spheres[index] = m_spheres[m_spheres.size() - 1];
		m_spheres.pop_back();
	}
	GLWrapperLineWidth(1.0f);
}

//----------------------------------------------------------------------------------
void GLWrapperDisableShader()
{
	glUseProgramObjectARB(0);
}