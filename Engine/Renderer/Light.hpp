#pragma once
#include "Engine\Core\RGBAColors.hpp"
#include "Engine\Math\Vec3.hpp"

class Light
{
public:
	Light(void);
	Light( const Vec3& pos );
	~Light(void);

	void setAsGlobalPointLight( const Vec3& pos, const RGBAColors& colorAndBrightness );
	void setAsLocalPointLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float innerRadiusDistance, float outerRadiusDistance );
	void setAsLocalSpotLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float innerRadiusDistance, float outerRadiusDistance, const Vec3& forwardDirection, float innerPenumbraDegrees, float outerPenumbraDegrees, float intensityOutsidePenumbra );
	void setAsGlobalSpotLight( const Vec3& pos, const RGBAColors& colorAndBrightness, const Vec3& forwardDirection, float innerPenumbraDegrees, float outerPenumbraDegrees, float intensityOutsidePenumbra );
	void setAsLocalAmbientLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float innerRadiusDistance, float outerRadiusDistance, float ambienceIntensity );
	void setAsGlobalAmbientLight( const Vec3& pos, const RGBAColors& colorAndBrightness, float ambienceIntensity );
	void setAsGlobalDirectLight( const Vec3& forwardDirection, const RGBAColors& colorAndBrightness );

	float m_ambientness;
	float m_innerRadius;
	float m_outerRadius;
	float m_innerPenumbraDot;
	float m_outerPenumbraDot;

	float m_intensityOutsidePenumbra;

	Vec3 m_position;
	Vec3 m_direction;
	RGBAColors m_colorAndBrightness;
};