#pragma once

#include "Engine\Math\Vec3.hpp"

class AABB
{
public:
	AABB( Vec3& min, Vec3& max);
	~AABB(void);

	Vec3 m_min;
	Vec3 m_max;
};

