#include "Commandlet.hpp"
#include "Engine/Core/GameCommon.hpp"
#include <algorithm>

CommandletRegistrationMap* CommandletRegistration::s_commandletRegistration = nullptr;
CommandletsGeneratedMap Commandlet::s_generatedCommandlets;

//----------------------------------------------------------------------------------
Commandlet* CommandletRegistration::createCommandlet()
{
	return (*s_commandletRegistration)[m_name]->m_registrationFunc(m_name);
}

//----------------------------------------------------------------------------------
Commandlet::Commandlet(const std::string& name)
	:m_name(name)
{

}

//----------------------------------------------------------------------------------
Commandlet::~Commandlet(void)
{

}

//----------------------------------------------------------------------------------
int Commandlet::runCommandlet(CommandletArgs& args)
{
	UNUSED(args);
	return 0;
}

//----------------------------------------------------------------------------------
int Commandlet::createCommandlets()
{
	int numOfRegistrations = 0;
	CommandletRegistrationMap* registrations = CommandletRegistration::getCommandletRegistrations();
	if (registrations)
	{
		for (CommandletRegistrationMap::iterator registerIterator = registrations->begin(); registerIterator != registrations->end(); registerIterator++)
		{
			++numOfRegistrations;
			CommandletRegistration* registration = registerIterator->second;
			Commandlet* commandlet = registration->createCommandlet();
			std::string name = registration->getName();
			s_generatedCommandlets[name] = commandlet;
		}
	}
	return numOfRegistrations;
}
