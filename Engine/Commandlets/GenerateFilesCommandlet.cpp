#include "GenerateFilesCommandlet.hpp"

CommandletRegistration GenerateFilesCommandlet::s_generateFilesRegistration("generateFiles", &GenerateFilesCommandlet::createCommandlet);

//----------------------------------------------------------------------------------
GenerateFilesCommandlet::GenerateFilesCommandlet(const std::string& name)
	: Commandlet(name)
{

}

//----------------------------------------------------------------------------------
GenerateFilesCommandlet::~GenerateFilesCommandlet(void)
{

}

//----------------------------------------------------------------------------------
Commandlet* GenerateFilesCommandlet::createCommandlet(const std::string& name)
{
	return new GenerateFilesCommandlet(name);
}

//----------------------------------------------------------------------------------
void GenerateFilesCommandlet::runCommandlet(CommandletArgs& args)
{
	int i = 0;
}