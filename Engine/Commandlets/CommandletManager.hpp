#pragma once

#include <string>
#include <vector>
#include <map>
#include "Commandlet.hpp"

class CommandletManager
{
public:

	CommandletManager();
	~CommandletManager();

	//static std::map<std::string, functionPointer> s_nameToFunctionPointerMap;
	int runCommands( const std::string &commandLineArguments );

	std::vector<CommandletArgs> m_commands;

private:
	void splitString(const std::string &toSplit, char delimeter, std::vector<std::string> &outDestination);
	int processArgs(std::vector<CommandletArgs> &args);
	int generateArgs(const std::string &commandLineArguments);
};

extern CommandletManager* g_commandletManager;