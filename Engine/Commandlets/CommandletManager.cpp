#include "CommandletManager.hpp"
#include <sstream>
#include <algorithm>
#include "Engine/Core/WindowsWrapper.hpp"

CommandletManager* g_commandletManager = nullptr;

//----------------------------------------------------------------------------------
CommandletManager::CommandletManager()
{
	Commandlet::createCommandlets();
}

//----------------------------------------------------------------------------------
int CommandletManager::runCommands(const std::string &commandLineArguments)
{
	return generateArgs(commandLineArguments);
}

//----------------------------------------------------------------------------------
void CommandletManager::splitString(const std::string &toSplit, char delimeter, std::vector<std::string> &outDestination)
{
	std::stringstream ss(toSplit);
	std::string item;
	while (std::getline(ss, item, delimeter)) {
		outDestination.push_back(item);
	}
}

//----------------------------------------------------------------------------------
int CommandletManager::processArgs(std::vector<CommandletArgs> &args)
{
	int toReturn = 0;
	for (int argsIndex = 0; argsIndex < (int)args.size(); ++argsIndex)
	{
		if (Commandlet::s_generatedCommandlets.find(args[argsIndex].command) == Commandlet::s_generatedCommandlets.end()) {
			// not found
			WindowWrapperDebuggerPrintf("Command %s was not found", args[argsIndex].command.c_str());
		}
		else {
			// run command
			toReturn += Commandlet::s_generatedCommandlets[args[argsIndex].command]->runCommandlet(args[argsIndex]);
		}
	}
	return toReturn;
}

//----------------------------------------------------------------------------------
int CommandletManager::generateArgs(const std::string &commandLineArguments)
{
	std::vector<std::string> separatedByQuotes;
	splitString( commandLineArguments, '"', separatedByQuotes );

	std::vector<std::string> args;
	for (int argIndex = 0; argIndex < (int)separatedByQuotes.size(); ++argIndex)
	{
		if ((std::count(separatedByQuotes[argIndex].begin(), separatedByQuotes[argIndex].end(), ' ') > 0) 
			&& (separatedByQuotes[argIndex][0] != ' ') 
			&& (separatedByQuotes[argIndex][separatedByQuotes[argIndex].size() - 1] != ' ')
			&& (separatedByQuotes[argIndex][0] != '-'))
		{
			args.push_back(separatedByQuotes[argIndex]);
		}
		else
		{
			std::vector<std::string> tempArgs;
			splitString(separatedByQuotes[argIndex], ' ', tempArgs);

			for (int tempArgsIndex = 0; tempArgsIndex < (int) tempArgs.size(); ++tempArgsIndex)
			{
				if (tempArgs[tempArgsIndex].size() > 0)
				{
					args.push_back(tempArgs[tempArgsIndex]);
				}
			}
		}
	}

	std::vector<CommandletArgs> commandlineArgs;
	for (int argIndex = 0; argIndex < (int)args.size(); ++argIndex)
	{
		if ((std::count(args[argIndex].begin(), args[argIndex].end(), ' ') == 0) && (args[argIndex][0] == '-'))
		{
			CommandletArgs newArgument;
			args[argIndex].erase(args[argIndex].begin());
			newArgument.command = args[argIndex];
			commandlineArgs.push_back(newArgument);
		}
		else
		{
			if (commandlineArgs.size() > 0)
			{
				commandlineArgs[commandlineArgs.size() - 1].args.push_back(args[argIndex]);
			}
		}
	}

	return processArgs(commandlineArgs);
}