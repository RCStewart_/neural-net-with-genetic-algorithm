#pragma once
#include "Commandlet.hpp"
#include <string>

class GenerateFilesCommandlet : public Commandlet
{
public:
	GenerateFilesCommandlet( const std::string& name );
	~GenerateFilesCommandlet();

	virtual void runCommandlet(CommandletArgs& args);
	//virtual void generateCommandlet(Commandlet* commandlet);

	static Commandlet* createCommandlet( const std::string& name );

protected:
	static CommandletRegistration s_generateFilesRegistration;
};

/*
class BuildingsWithRiverGenerator: public CellularAutomataMapGenerator
{
public:
BuildingsWithRiverGenerator( const std::string& name );
~BuildingsWithRiverGenerator(void);

static BaseMapGenerator* createGenerator( const std::string& name );

virtual void generateMap( Map* map );
virtual void processOneStep( Map* map );
virtual void processSeveralSteps( Map* map );

void placeABuilding( Map* map, Room& buildingToAdd );
void addDoorWaysToBuilding( Map* map, Room& buildingToAdd );
void getRangesForDoorHorizantol( Map* map, int y, Vec2& xRange, std::vector<Vec2>& rangesToGather);
void getRangesForDoorVertical( Map* map, int x, Vec2& yRange, std::vector<Vec2>& rangesToGather);

void addRiver( Map* map );
bool isValidLocationForRiverTile( Map* map, IntVec2& locationToCheck, int index );

protected:
static MapGeneratorRegistration s_buildingsWithRiverRegistration;
};

*/