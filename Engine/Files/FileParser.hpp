#pragma once

#include <string>
#include "Engine\Math\Vec3.hpp"
#include <vector>
#include "Engine\Math\Vec2.hpp"
#include "Engine\Core\RGBAColors.hpp"

struct FileData
{
	FileData()
		:size(0)
		,buffer(nullptr)
		,startOfBuffer(nullptr)
		,currentIndex(0)
	{}
	~FileData()
	{
		delete[] startOfBuffer;
	}
	size_t size;
	unsigned int currentIndex;
	char* buffer;
	char* startOfBuffer;
};

class FileParser
{
public:
	FileParser();
	~FileParser();

	void writeFloatToBuffer	( std::vector<unsigned char>& outPutVector, const float& toWrite);
	void writeUIntToBuffer	( std::vector<unsigned char>& outPutVector, const unsigned int& toWrite);
	void writeIntToBuffer	( std::vector<unsigned char>& outPutVector, const int& toWrite);
	void writeUCharToBuffer	( std::vector<unsigned char>& outPutVector, const unsigned char& toWrite);
	void writeVec3ToBuffer	( std::vector<unsigned char>& outPutVector, const Vec3& toWrite);
	void writeVec2ToBuffer	( std::vector<unsigned char>& outPutVector, const Vec2& toWrite);
	void writeRGBAToBuffer	( std::vector<unsigned char>& outPutVector, const RGBAColors& toWrite);

	void readFloatFromBuffer( FileData* dataToRead, float& dataOut);
	void readIntFromBuffer	( FileData* dataToRead, int& dataOut);
	void readUIntFromBuffer	( FileData* dataToRead, unsigned int& dataOut);
	void readUCharFromBuffer( FileData* dataToRead, unsigned char& dataOut);
	void readVec3FromBuffer	( FileData* dataToRead, Vec3& dataOut);
	void readVec2FromBuffer	( FileData* dataToRead, Vec2& dataOut);
	void readRGBAFromBuffer	( FileData* dataToRead, RGBAColors& dataOut);
	void readStringFromBuffer( FileData* dataToRead, std::string& dataOut );

	FileData* generateFileDataFromFile( const std::string& filePath, const std::string& fileName );
};