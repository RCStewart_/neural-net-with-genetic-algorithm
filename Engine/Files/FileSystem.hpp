#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>

#include <vector>
#include <string>

#include <map>
//#include <mutex>

#include "Engine/Zip/zip.h"
#include "Engine/Zip/unzip.h"

//#include <algorithm>

struct FilesWithPaths
{
	std::vector<std::string> files;
	std::vector<std::string> directoriesFound;
};

struct FileBuffer
{
	FileBuffer()
		: buffer(nullptr)
		, size(0)
	{

	}

	void copyTo( FileBuffer& outBuffer)
	{
		delete[] outBuffer.buffer;
		outBuffer.buffer = new unsigned char[size];
		memcpy((void*)outBuffer.buffer, (void*)buffer, size);
		outBuffer.size = size;
	}

	~FileBuffer()
	{
		delete[] buffer;
	}

	unsigned char* buffer;
	int size;
};

class FileSystem
{
public:
	FileSystem( void* platformHandle );
	~FileSystem();
	void findAllFilesInDirectory( std::vector<std::string>& outFilesFound, const std::string& directory );
	void findAllFIlesInDirectoryAndStoreFilePaths( std::vector<std::string>& outFilesFound, const std::string& directory, std::vector<FilesWithPaths>& outFilesWithPaths, std::vector<std::string> currentPath);
	int loadFile(const std::string& fileToLoad, FileBuffer &outBuffer, bool storeInMemory);
	int loadFromData(const std::string& fileToLoad, FileBuffer &outBuffer, bool storeInMemory);
	int loadFromArchive(const std::string& fileToLoad, FileBuffer &outBuffer, bool storeInMemory);
	void compressAllFilesInData(const std::string& password);
	bool isFileInMemory(const std::string& file);
	bool doesFileExistOnDisc(const std::string& file);
	bool existsInData(const std::string& fileToLoad);
	bool existsInArchive(const std::string& fileToLoad);


	//std::mutex m_lockNameToBufferMap;
	std::map<std::string, FileBuffer> m_nameToBufferMap;
};

extern FileSystem* g_theFileSystem;

