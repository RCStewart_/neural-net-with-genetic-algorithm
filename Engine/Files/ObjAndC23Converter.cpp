#include "ObjAndC23Converter.hpp"
#include "Engine/Renderer/MeshVertexData.hpp"
#include "Engine\Math\Vec3.hpp"
#include "Engine\Math\Vec2.hpp"

#include <algorithm>
#include <iterator>

//----------------------------------------------------------------------------------
ObjAndC23Converter::ObjAndC23Converter()
	:m_versionNumber(1)
	 ,m_subtypeNumber(2)
{

}

//----------------------------------------------------------------------------------
ObjAndC23Converter::~ObjAndC23Converter()
{

}

//----------------------------------------------------------------------------------------------
void ObjAndC23Converter::writeToFile( const std::string& fileName, const std::string& filePath, MeshVertexData& dataToWrite,  unsigned char subType, unsigned char version )
{
	unsigned char toWrite= 'G';
	m_fileParser.writeUCharToBuffer( m_outputBuffer, toWrite);
	toWrite= 'C';
	m_fileParser.writeUCharToBuffer( m_outputBuffer, toWrite);
	toWrite= '2';
	m_fileParser.writeUCharToBuffer( m_outputBuffer, toWrite);
	toWrite= '3';
	m_fileParser.writeUCharToBuffer( m_outputBuffer, toWrite);

	//m_fileParser.writeUIntToBuffer( m_outputBuffer, (unsigned int) subType );
	m_fileParser.writeUCharToBuffer( m_outputBuffer, subType);
	m_fileParser.writeUCharToBuffer( m_outputBuffer, version);

	//comments should be printed here
	toWrite = '\0';
	m_fileParser.writeUCharToBuffer( m_outputBuffer, toWrite);

	//Gather vertex data
	dataToWrite.addSelfToBufferAsBytes( m_outputBuffer, subType );

	std::string directoryName = "Data/";
	std::string newFileName = fileName;
	newFileName += ".C23";

	FILE* f =  nullptr;
	fopen_s( &f, filePath.c_str(), "wb");

	unsigned char* buffer = new unsigned char[m_outputBuffer.size()];
	for(int i = 0; i < (int) m_outputBuffer.size(); ++i)
	{
		buffer[i] = m_outputBuffer[i];
	}

	fwrite(buffer , sizeof(unsigned char), (size_t) m_outputBuffer.size() - 1, f);
	fclose(f);

	delete[] buffer;

}

//----------------------------------------------------------------------------------
void ObjAndC23Converter::readFromC23File( const std::string& filePath, const std::string& fileName, MeshVertexData& outDataToFill )
{
	FileParser parser;
	FileData* dataBuffer = parser.generateFileDataFromFile(filePath, fileName);

	fillMeshVertexDataFromC23File( outDataToFill, dataBuffer );
	delete dataBuffer;
}

//----------------------------------------------------------------------------------
void ObjAndC23Converter::fillMeshVertexDataFromC23File( MeshVertexData& outDataToFill, FileData* dataToParse )
{
	outDataToFill.clear();
	//unsigned char currentChar;
	std::string GC23 = "????";
	unsigned char fourCC0;
	unsigned char fourCC1;
	unsigned char fourCC2;
	unsigned char fourCC3;
	FileParser parser;

	parser.readUCharFromBuffer( dataToParse, fourCC0);
	parser.readUCharFromBuffer( dataToParse, fourCC1);
	parser.readUCharFromBuffer( dataToParse, fourCC2);
	parser.readUCharFromBuffer( dataToParse, fourCC3);

	GC23[0] = fourCC0;
	GC23[1] = fourCC1;
	GC23[2] = fourCC2;
	GC23[3] = fourCC3;
	//TODO:  Alter how fourCC is read/wrote to handle endianness
	unsigned char subtype = 2;
	unsigned char version = 1;

	parser.readUCharFromBuffer( dataToParse, subtype);
	parser.readUCharFromBuffer( dataToParse, version);

	std::string comments;
	parser.readStringFromBuffer( dataToParse, comments);

	
	if(m_subtypeNumber > 1)
	{
		unsigned int indexArraySize;
		parser.readUIntFromBuffer( dataToParse, indexArraySize );
		for( int i = 0; i < (int) indexArraySize; ++i )
		{
			unsigned int toAdd;
			parser.readUIntFromBuffer( dataToParse, toAdd );
			outDataToFill.m_indexes.push_back( toAdd );
		}
	}


	unsigned int vertexArraySize = 0;
	parser.readUIntFromBuffer( dataToParse, vertexArraySize );
	for( int i = 0; i < (int) vertexArraySize; ++i )
	{
		ModelVertex toAdd;
		parser.readVec3FromBuffer( dataToParse, toAdd.m_pos			);
		parser.readRGBAFromBuffer( dataToParse, toAdd.m_color		);
		parser.readVec2FromBuffer( dataToParse, toAdd.m_texCoords	);
		parser.readVec3FromBuffer( dataToParse, toAdd.m_normal		);
		parser.readVec3FromBuffer( dataToParse, toAdd.m_tangent		);
		parser.readVec3FromBuffer( dataToParse, toAdd.m_bitangent	);
		outDataToFill.m_vertexes.push_back( toAdd );
	}

	if(m_subtypeNumber <= 1)
	{
		for( size_t currentIndexIndex = 0; currentIndexIndex < outDataToFill.m_vertexes.size(); ++currentIndexIndex )
		{
			outDataToFill.m_indexes.push_back(currentIndexIndex);
		}
	}
}