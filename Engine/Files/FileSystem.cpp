#include "Engine/Files/FileSystem.hpp"
#include "Engine/Core/GameCommon.hpp"
#include <io.h>

FileSystem* g_theFileSystem = nullptr;

bool FileSystem::isFileInMemory(const std::string& toFind)
{
	//m_lockNameToBufferMap.lock();
	if (m_nameToBufferMap.find(toFind) == m_nameToBufferMap.end()) {
		//m_lockNameToBufferMap.unlock();
		return false;
	}
	else {
		//m_lockNameToBufferMap.unlock();
		return true;
	}
}

//----------------------------------------------------------------------------------
FileSystem::FileSystem( void* platformHandle )
{
	UNUSED(platformHandle);
	//Do nothing
}

//----------------------------------------------------------------------------------
FileSystem::~FileSystem()
{

}

//----------------------------------------------------------------------------------
void FileSystem::findAllFilesInDirectory( std::vector<std::string>& outFilesFound, const std::string& directory )
{
	int error							= 0;
	struct _finddata_t	fileInfo;
	intptr_t			searchHandle	= _findfirst( directory.c_str(), &fileInfo);
	std::string lastFound = "ad;lkfjal;fjla;kjsdf"; //garbage value
	bool isFirstElement = true;
	while( searchHandle != -1 && searchHandle != error && lastFound != fileInfo.name )
	{
		lastFound = fileInfo.name;
		bool isADirectory = fileInfo.attrib & _A_SUBDIR ? true :false;

		if( isADirectory )
		{
			if(lastFound != "." && lastFound != "..")
			{
				std::string tempDir = directory;
				tempDir = tempDir.substr(0, tempDir.size()-1);
				std::string tempLastFound = lastFound;
				tempLastFound += "/*";
				tempLastFound = tempDir + tempLastFound;
				findAllFilesInDirectory( outFilesFound, tempLastFound );
			}	
		}

		isFirstElement = false;

		bool isHidden = fileInfo.attrib & _A_HIDDEN ? true : false;

		if(!isHidden && !isADirectory)
		{
			outFilesFound.push_back( fileInfo.name );
		}

		error = _findnext( searchHandle, &fileInfo );
	}
}

//----------------------------------------------------------------------------------
void FileSystem::findAllFIlesInDirectoryAndStoreFilePaths( std::vector<std::string>& outFilesFound, const std::string& directory, std::vector<FilesWithPaths>& outFilesWithPaths, std::vector<std::string> currentPath)
{
	int error							= 0;
	struct _finddata_t	fileInfo;
	intptr_t			searchHandle	= _findfirst( directory.c_str(), &fileInfo);
	std::string lastFound = "ad;lkfjal;fjla;kjsdf"; //garbage value
	bool isFirstElement = true;
	outFilesWithPaths.push_back( FilesWithPaths() );
	for(size_t indexOfCurrentPath = 0; indexOfCurrentPath < currentPath.size(); ++indexOfCurrentPath)
	{
		outFilesWithPaths[outFilesWithPaths.size() - 1].directoriesFound.push_back(currentPath[indexOfCurrentPath]);
	}
	while( searchHandle != -1 && searchHandle != error && lastFound != fileInfo.name )
	{
		lastFound = fileInfo.name;
		bool isADirectory = fileInfo.attrib & _A_SUBDIR ? true :false;

		if( isADirectory )
		{
			if(lastFound != "." && lastFound != "..")
			{
				std::string tempDir = directory;
				std::vector<std::string> path = currentPath;
				path.push_back(lastFound);
				tempDir = tempDir.substr(0, tempDir.size()-1);
				std::string tempLastFound = lastFound;
				tempLastFound += "/*";
				tempLastFound = tempDir + tempLastFound;
				findAllFIlesInDirectoryAndStoreFilePaths( outFilesFound, tempLastFound, outFilesWithPaths, path );
			}	
		}

		isFirstElement = false;

		bool isHidden = fileInfo.attrib & _A_HIDDEN ? true : false;

		if(!isHidden && !isADirectory)
		{
			outFilesFound.push_back( fileInfo.name );
			outFilesWithPaths[outFilesWithPaths.size() - 1].files.push_back(fileInfo.name);
		}

		error = _findnext( searchHandle, &fileInfo );
	}
}

//Requires outDestination to be proper size before using (toConvert.size() + 1)
//----------------------------------------------------------------------------------
void convertStringToTCharStar(std::string& toConvert, TCHAR* outDestination)
{
	outDestination[toConvert.size()] = 0;
	std::copy(toConvert.begin(), toConvert.end(), outDestination);
}

//----------------------------------------------------------------------------------
void FileSystem::compressAllFilesInData(const std::string& password)
{
	HZIP destination = CreateZip(L"Data.zip", password.c_str());
	std::vector<std::string> outFilesFound;
	std::string directory = "Data/*";
	std::vector<FilesWithPaths> outFilesWithPaths;
	std::vector<std::string> currentPath;
	findAllFIlesInDirectoryAndStoreFilePaths(outFilesFound, directory, outFilesWithPaths, currentPath);

	std::vector<std::string> getAllFilesWithFullPath;

	for (int filesWithPathIndex = 0; filesWithPathIndex < (int) outFilesWithPaths.size(); ++filesWithPathIndex)
	{
		//for each directory on the path
		std::string filePath = "";
		for (int directoryIndex = 0; directoryIndex < (int)outFilesWithPaths[filesWithPathIndex].directoriesFound.size(); ++directoryIndex)
		{
			filePath += outFilesWithPaths[filesWithPathIndex].directoriesFound[directoryIndex];
			filePath += "/";
		}
		
		if (outFilesWithPaths[filesWithPathIndex].files.size() == 0)
		{
			TCHAR* toCreate = new TCHAR[filePath.size() + 1];
			convertStringToTCharStar(filePath, toCreate);
			ZipAddFolder(destination, toCreate);
			delete[] toCreate;
		}
		else
		{
			//for each file in the directory
			for (int fileIndex = 0; fileIndex < (int)outFilesWithPaths[filesWithPathIndex].files.size(); ++fileIndex)
			{
				std::string tempDest = filePath;
				std::string tempSrc = "Data/";
				tempDest += outFilesWithPaths[filesWithPathIndex].files[fileIndex];
				tempSrc += tempDest;
				TCHAR* toCreate = new TCHAR[tempDest.size() + 1];
				TCHAR* toCreateFrom = new TCHAR[tempSrc.size() + 1];
				convertStringToTCharStar(tempDest, toCreate);
				convertStringToTCharStar(tempSrc, toCreateFrom);
				ZipAdd(destination, toCreate, toCreateFrom);
				delete[] toCreate;
			}
		}
	}

	CloseZip(destination);
}


//Returns -1 if the file was not loaded
//----------------------------------------------------------------------------------
int FileSystem::loadFile(const std::string& fileToLoad, FileBuffer &outBuffer, bool storeInMemory)
{
	int loadSuccess = -1;
	FileBuffer loadedBuffer;

	if (g_currentLoadPreference == LOAD_FROM_DATA_ONLY)
	{
		loadSuccess = loadFromData(fileToLoad, outBuffer, storeInMemory);
	}
	if (g_currentLoadPreference == LOAD_FROM_ARCHIVE_ONLY)
	{
		loadSuccess = loadFromArchive(fileToLoad, outBuffer, storeInMemory);
	}
	if (g_currentLoadPreference == PREFER_LOADING_FROM_DATA)
	{
		loadSuccess = loadFromData(fileToLoad, outBuffer, storeInMemory);
		if (loadSuccess == -1)
		{
			loadSuccess = loadFromArchive(fileToLoad, outBuffer, storeInMemory);
		}
	}
	if (g_currentLoadPreference == PREFER_LOADING_FROM_ARCHIVE)
	{
		loadSuccess = loadFromArchive(fileToLoad, outBuffer, storeInMemory);
		if (loadSuccess == -1)
		{
			loadSuccess = loadFromData(fileToLoad, outBuffer, storeInMemory);
		}
	}

	return loadSuccess;
}

//----------------------------------------------------------------------------------
int FileSystem::loadFromData(const std::string& fileToLoad, FileBuffer &outBuffer, bool storeInMemory)
{
	int loadSuccess = -1;
	FileBuffer loadedBuffer;

	if (isFileInMemory(fileToLoad))
	{
		loadSuccess = 1;
		//m_lockNameToBufferMap.lock();
		m_nameToBufferMap[fileToLoad].copyTo(outBuffer);
		//m_lockNameToBufferMap.unlock();
	}
	else
	{
		FILE* f = nullptr;
		fopen_s(&f, fileToLoad.c_str(), "rb");

		if (f != nullptr)
		{
			fseek(f, 0, SEEK_END);
			size_t size = ftell(f);
			rewind(f);

			loadedBuffer.size = (int)size;

			loadedBuffer.buffer = new unsigned char[size];
			fread(loadedBuffer.buffer, sizeof(char), (size_t)loadedBuffer.size, f);
			loadedBuffer.copyTo(outBuffer);
			loadSuccess = 1;
			fclose(f);
		}

		if (storeInMemory && loadSuccess != -1)
		{
			//m_lockNameToBufferMap.lock();
			m_nameToBufferMap[fileToLoad] = FileBuffer();
			loadedBuffer.copyTo(m_nameToBufferMap[fileToLoad]);
			//m_lockNameToBufferMap.unlock();
		}
	}

	return loadSuccess;
}

//----------------------------------------------------------------------------------
int FileSystem::loadFromArchive(const std::string& fileToLoad, FileBuffer &outBuffer, bool storeInMemory)
{
	int loadSuccess = -1;
	std::string fileToLoadSubString = fileToLoad.substr(fileToLoad.find_first_of("/") + 1);
	if (isFileInMemory(fileToLoad))
	{
		loadSuccess = 1;
		//m_lockNameToBufferMap.lock();
		m_nameToBufferMap[fileToLoad].copyTo(outBuffer);
		//m_lockNameToBufferMap.unlock();
	}
	else
	{
		HZIP openedZip = OpenZip(L"Data.zip", g_dataPassword.c_str());

		//HRSRC hrsrc = FindResource(nullptr, MAKEINTRESOURCE(1), RT_RCDATA);
		//HANDLE handle = LoadResource(nullptr, hrsrc);
		//void* zipbuf = LockResource(handle);
		//unsigned int ziplen = SizeofResource(nullptr, hrsrc);
		//hz = OpenZip(zipbuf, ziplen, 0);

		ZIPENTRY zipEntry;
		int i;

		TCHAR* fileNameToLoad = new TCHAR[fileToLoadSubString.size() + 1];
		convertStringToTCharStar(fileToLoadSubString, fileNameToLoad);
		FindZipItem(openedZip, fileNameToLoad, true, &i, &zipEntry);

		unsigned char *buffer = new unsigned char[zipEntry.unc_size];
		int size = zipEntry.unc_size;
		UnzipItem(openedZip, i, buffer, zipEntry.unc_size);

		//std::string tempBuffer((char*) buffer);
		if (i != -1)
		{
			loadSuccess = 1;
			outBuffer.buffer = new unsigned char[size];
			outBuffer.size = size;
			memcpy((void*)outBuffer.buffer, (void*)buffer, size);
		}

		delete[] buffer;
		CloseZip(openedZip);

		if (storeInMemory && loadSuccess != -1)
		{
			//m_lockNameToBufferMap.lock();
			m_nameToBufferMap[fileToLoad] = FileBuffer();
			outBuffer.copyTo(m_nameToBufferMap[fileToLoad]);
			//m_lockNameToBufferMap.unlock();
		}
	}

	return loadSuccess;
}

//----------------------------------------------------------------------------------
bool FileSystem::doesFileExistOnDisc(const std::string& fileToLoad)
{
	bool doesExist = false;
	if (g_currentLoadPreference == LOAD_FROM_DATA_ONLY)
	{
		doesExist = existsInData(fileToLoad);
	}
	if (g_currentLoadPreference == LOAD_FROM_ARCHIVE_ONLY)
	{
		doesExist = existsInArchive(fileToLoad);
	}
	if (g_currentLoadPreference == PREFER_LOADING_FROM_DATA)
	{
		doesExist = existsInData(fileToLoad);
		if (!doesExist)
		{
			doesExist = existsInArchive(fileToLoad);
		}
	}
	if (g_currentLoadPreference == PREFER_LOADING_FROM_ARCHIVE)
	{
		doesExist = existsInArchive(fileToLoad);
		if (!doesExist)
		{
			doesExist = existsInData(fileToLoad);
		}
	}
	return doesExist;
}

//----------------------------------------------------------------------------------
bool FileSystem::existsInData(const std::string& fileToLoad)
{
	bool doesExist = false;
	if (isFileInMemory(fileToLoad))
	{
		doesExist = true;
	}
	else
	{
		FILE* f = nullptr;
		fopen_s(&f, fileToLoad.c_str(), "rb");

		if (f != nullptr)
		{
			doesExist = true;
			fclose(f);
		}
	}
	return doesExist;
}

//----------------------------------------------------------------------------------
bool FileSystem::existsInArchive(const std::string& fileToLoad)
{
	bool doesExist = false;
	std::string fileToLoadSubString = fileToLoad.substr(fileToLoad.find_first_of("/") + 1);
	if (isFileInMemory(fileToLoad))
	{
		doesExist = true;
	}
	else
	{
		HZIP openedZip = OpenZip(L"Data.zip", g_dataPassword.c_str());

		//HRSRC hrsrc = FindResource(nullptr, MAKEINTRESOURCE(1), RT_RCDATA);
		//HANDLE handle = LoadResource(nullptr, hrsrc);
		//void* zipbuf = LockResource(handle);
		//unsigned int ziplen = SizeofResource(nullptr, hrsrc);

		//hz = OpenZip(zipbuf, ziplen, 0);

		ZIPENTRY zipEntry;
		int i;

		TCHAR* fileNameToLoad = new TCHAR[fileToLoadSubString.size() + 1];
		convertStringToTCharStar(fileToLoadSubString, fileNameToLoad);
		FindZipItem(openedZip, fileNameToLoad, true, &i, &zipEntry);

		unsigned char *buffer = new unsigned char[zipEntry.unc_size];
		//int size = zipEntry.unc_size;
		UnzipItem(openedZip, i, buffer, zipEntry.unc_size);

		//std::string tempBuffer((char*) buffer);
		if (i != -1)
		{
			doesExist = true;
		}

		delete[] buffer;
		CloseZip(openedZip);
	}
	return doesExist;
}