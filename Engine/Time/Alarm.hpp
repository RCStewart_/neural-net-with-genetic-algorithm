#pragma once
#include <string>

class Clock;

class Alarm
{
public:
	Alarm( Clock* controllingClock, double duration, const std::string &name );
	~Alarm();

	virtual void finishedEvent();

	void update();
	bool isFinished();

	double GetPercentElapsed();
	double GetPercentRemaining();
	double GetSecondsElapsed();
	double GetSecondsRemaining();

	double m_startTime;
	double m_duration;

	int m_name;

	Clock* m_controllingClock;
};