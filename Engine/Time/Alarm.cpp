#include "Alarm.hpp"
#include "Engine/Time/Clock.hpp"
#include "Engine/Core/StringTable.hpp"
#include "Engine/Core/WindowsWrapper.hpp"

Alarm::Alarm(Clock* controllingClock, double duration, const std::string &name)
	: m_controllingClock(controllingClock)
	, m_duration(duration)
{
	m_startTime = 0.0;
	if (m_controllingClock)
	{
		m_startTime = m_controllingClock->getTimeAsSeconds();
		m_controllingClock->m_alarms.push_back(this);
	}

	m_name = StringID(name);
}

//----------------------------------------------------------------------------------
Alarm::~Alarm()
{

}

//----------------------------------------------------------------------------------
void Alarm::finishedEvent()
{
	std::string name = StringValue(m_name);
	WindowWrapperDebuggerPrintf("%s has finished \n", name.c_str());
}

//----------------------------------------------------------------------------------
void Alarm::update()
{
	if (isFinished())
	{
		finishedEvent();
	}
}

//----------------------------------------------------------------------------------
bool Alarm::isFinished()
{
	return GetPercentRemaining() < 0.0;
}

//----------------------------------------------------------------------------------
double Alarm::GetPercentElapsed()
{
	if (m_controllingClock)
	{
		return GetSecondsElapsed()/m_duration;
	}
	return -1.0;
}

//----------------------------------------------------------------------------------
double Alarm::GetPercentRemaining()
{
	if (m_controllingClock)
	{
		return 1.0 - (GetSecondsElapsed() / m_duration);
	}
	return -1.0;
}

//----------------------------------------------------------------------------------
double Alarm::GetSecondsElapsed()
{
	if (m_controllingClock)
	{
		return m_controllingClock->getTimeAsSeconds() - m_startTime;
	}
	return -1.0;
}

//----------------------------------------------------------------------------------
double Alarm::GetSecondsRemaining()
{
	if (m_controllingClock)
	{
		return m_duration - GetSecondsElapsed();
	}
	return -1.0;
}