#pragma once
#include "Windows.h"
#define WIN32_LEAN_AND_MEAN

double GetAbsoluteTimeSeconds();
double InitializeTimeSystem( LARGE_INTEGER &timeToInitialize );
double GetSecondsPerClock();