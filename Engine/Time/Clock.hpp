#pragma once

#include <vector>
class Alarm;

class Clock
{
public:
	Clock(Clock* parentClock);
	Clock(Clock* parentClock, double timeScale);
	~Clock();

	void update();
	void addSelfToParent();
	void togglePause();
	void advanceTimeAndUpdateAlarms( double deltaTime );
	void removeClockFromChildren( int clockID );
	void removeAndDeleteEmptyAlarms();
	double getTimeAsSeconds();
	static void initializeClockSystem();

	int m_index;

	double m_calculatedTimeSoFar;
	double m_maxDelta;
	double m_timeScale;
	double m_lastRecordedTime;

	Clock* m_parent;

	std::vector<Clock*> m_children;
	std::vector<Alarm*> m_alarms;

private:
	Clock();
	bool m_isPaused;
};

extern Clock* g_rootClock;