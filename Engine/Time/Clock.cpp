#include "Clock.hpp"
#include "Engine/Time/UtilitiesTime.hpp"
#include "Windows.h"
#define WIN32_LEAN_AND_MEAN

#include "Engine/Core/UtilityFunctions.hpp"
#include "Engine/Time/Alarm.hpp"

Clock* g_rootClock = nullptr;
int g_nextClockID = 1;

//----------------------------------------------------------------------------------
Clock::Clock()
	: m_isPaused(false)
	, m_maxDelta(1.0)
	, m_timeScale(1.0)
	, m_parent(nullptr)
	, m_calculatedTimeSoFar(0.0)
{
	m_lastRecordedTime = GetAbsoluteTimeSeconds();
	m_index = g_nextClockID;
	++g_nextClockID;
}

//----------------------------------------------------------------------------------
Clock::Clock(Clock* parentClock)
	: m_isPaused(false)
	, m_maxDelta(1.0)
	, m_timeScale(1.0)
	, m_parent(parentClock)
	, m_calculatedTimeSoFar(0.0)
{
	//m_startTime = 
	m_lastRecordedTime = GetAbsoluteTimeSeconds();

	if (!m_parent)
	{
		m_parent = g_rootClock;
	}

	addSelfToParent();

	m_index = g_nextClockID;
	++g_nextClockID;
}

//----------------------------------------------------------------------------------
Clock::Clock(Clock* parentClock, double timeScale)
	: m_isPaused(false)
	, m_maxDelta(1.0)
	, m_timeScale(timeScale)
	, m_parent(parentClock)
	, m_calculatedTimeSoFar(0.0)
{
	//m_startTime = 
	m_lastRecordedTime = GetAbsoluteTimeSeconds();

	if (!m_parent)
	{
		m_parent = g_rootClock;
	}

	addSelfToParent();

	m_index = g_nextClockID;
	++g_nextClockID;
}

//----------------------------------------------------------------------------------
Clock::~Clock()
{
	for (int clockIndex = 0; clockIndex < (int) m_children.size(); ++clockIndex)
	{
		delete m_children[clockIndex];
	}
	m_parent->removeClockFromChildren(m_index);
}

//----------------------------------------------------------------------------------
void Clock::update()
{
	if (!m_isPaused)
	{
		double deltaTime = GetAbsoluteTimeSeconds() - m_lastRecordedTime;
		UtilityFunctions::clampDouble(deltaTime, 0.0, m_maxDelta);
		m_lastRecordedTime = GetAbsoluteTimeSeconds();

		advanceTimeAndUpdateAlarms(deltaTime);
	}
}

//----------------------------------------------------------------------------------
void Clock::addSelfToParent()
{
	if (m_parent)
	{
		m_parent->m_children.push_back( this );
	}
}

//----------------------------------------------------------------------------------
void Clock::advanceTimeAndUpdateAlarms(double deltaTime)
{
	if (!m_isPaused)
	{
		deltaTime *= m_timeScale;
		m_calculatedTimeSoFar += deltaTime;
		UtilityFunctions::clampDouble(deltaTime, 0.0, m_maxDelta);

		for (int childIndex = 0; childIndex < (int)m_children.size(); ++childIndex)
		{
			m_children[childIndex]->advanceTimeAndUpdateAlarms(deltaTime);
		}

		for (int alarmIndex = 0; alarmIndex < (int)m_alarms.size(); ++alarmIndex)
		{
			m_alarms[alarmIndex]->update();
		}

		removeAndDeleteEmptyAlarms();
	}
}

//----------------------------------------------------------------------------------
void Clock::removeAndDeleteEmptyAlarms()
{
	std::vector<Alarm*>::iterator it = m_alarms.begin();
	for (it = m_alarms.begin(); it != m_alarms.end();)
	{
		if ((*it)->isFinished())
		{
			delete *it;
			it = m_alarms.erase(it);
		}
		else
		{
			++it;
		}
	}
}

//----------------------------------------------------------------------------------
void Clock::removeClockFromChildren(int clockID)
{
	for (int childIndex = 0; childIndex < (int)m_children.size(); ++childIndex)
	{
		if ( m_children[childIndex]->m_index == clockID )
		{
			m_children.erase( m_children.begin() + childIndex );
		}
	}
}

//----------------------------------------------------------------------------------
void Clock::initializeClockSystem()
{
	g_rootClock = new Clock();
}

//----------------------------------------------------------------------------------
void Clock::togglePause()
{
	m_isPaused = !m_isPaused;
	if (!m_isPaused)
	{
		m_lastRecordedTime = GetAbsoluteTimeSeconds();
	}
}

//----------------------------------------------------------------------------------
double Clock::getTimeAsSeconds()
{
	return m_calculatedTimeSoFar;
}