#pragma once
#include "..\Math\Vec3.hpp"
#include "..\Math\EulerAngles.hpp"
#include "..\Math\IntVec2.hpp"
#include <vector>
#include "..\Math\Vec2.hpp"
#include "..\Renderer\Renderer.hpp"

class Camera3D
{
public:
	Camera3D(void);
	Camera3D(Vec3& startingPosition, EulerAngles& startingOrientation);
	~Camera3D(void);

	void clampPitch();

	void applyCameraTransform();
	void applyCameraTransform( Renderer* gameRenderer );
	void updateCameraFromMouseAndKeyboard( float deltaSeconds );
	Vec2 getMouseMovementSinceLastChecked();

	Vec3 m_position;
	Vec3 m_up;
	Vec3 m_left;

	EulerAngles m_orientation;

	//enum CAMERA_MODE
	//{
	//	CAMERA_MODE_NO_CLIP,
	//	CAMERA_MODE_FLYING,
	//	CAMERA_MODE_GROUNDED
	//};

	//CAMERA_MODE m_currentMode;
};

