#include "EngineCommon.hpp"
#include <stdio.h>
#include <stdarg.h>
#include <iostream>

Vec3 g_left		= Vec3( 0.f, 1.f, 0.f );
Vec3 g_right	= Vec3( 0.f, -1.f, 0.f );
Vec3 g_up		= Vec3( 0.f, 0.f, 1.f );
Vec3 g_down		= Vec3( 0.f, 0.f, -1.f );
Vec3 g_forward	= Vec3( 1.f, 0.f, 0.f );
Vec3 g_backward	= Vec3( -1.f, 0.f, 0.f );

Vec2 g_north	= Vec2( 0.f, 1.f );
Vec2 g_south	= Vec2( 0.f, -1.f );
Vec2 g_east	= Vec2( 1.f, 0.f );
Vec2 g_west	= Vec2( -1.f, 0.f );

Vec3 GetVec3FromStringDirection( std::string direction )
{
	if(direction == "left")
	{
		return g_left;
	}
	if(direction == "right")
	{
		return g_right;
	}
	if(direction == "up")
	{
		return g_up;
	}
	if(direction == "down")
	{
		return g_down;
	}
	if(direction == "forward")
	{
		return g_forward;
	}
	if(direction == "backward")
	{
		return g_backward;
	}

	return g_backward;
}

//----------------------------------------------------------------------------------
void ClearFile( const std::string& filePathToWriteTo )
{
	FILE* f = fopen( filePathToWriteTo.c_str(), "w");
	fclose( f );
	fflush( f );
}

//-----------------------------------------------------------------------------------------------
void LogAppendPrintf( const std::string& filePathToWriteTo, const char* messageFormat, ... )
{
	const int MESSAGE_MAX_LENGTH = 2048;
	char messageLiteral[ MESSAGE_MAX_LENGTH ];
	va_list variableArgumentList;
	va_start( variableArgumentList, messageFormat );
	vsnprintf_s( messageLiteral, MESSAGE_MAX_LENGTH, _TRUNCATE, messageFormat, variableArgumentList );
	va_end( variableArgumentList );
	messageLiteral[ MESSAGE_MAX_LENGTH - 1 ] = '\0'; // In case vsnprintf overran (doesn't auto-terminate)

#if defined( PLATFORM_WINDOWS )
	if( IsDebuggerAvailable() )
	{
		OutputDebugStringA( messageLiteral );
	}
#endif

	FILE* f = nullptr;
	f = fopen( filePathToWriteTo.c_str(), "a" );

	if(f == nullptr)
	{
		return;
	}
	//std::string messageLiteralAsString( "Testing..." );
	fputs( messageLiteral, f);

	//fwrite( /*messageLiteral*/messageLiteralAsString.c_str() , sizeof(unsigned char), (size_t) MESSAGE_MAX_LENGTH - 1, f);
	fclose( f );
	fflush( f );
}

//-----------------------------------------------------------------------------------------------
std::string GetMessageLiteralf( const char* messageFormat, ... )
{
	const int MESSAGE_MAX_LENGTH = 2048;
	char messageLiteral[ MESSAGE_MAX_LENGTH ];
	va_list variableArgumentList;
	va_start( variableArgumentList, messageFormat );
	vsnprintf_s( messageLiteral, MESSAGE_MAX_LENGTH, _TRUNCATE, messageFormat, variableArgumentList );
	va_end( variableArgumentList );
	messageLiteral[ MESSAGE_MAX_LENGTH - 1 ] = '\0'; // In case vsnprintf overran (doesn't auto-terminate)

#if defined( PLATFORM_WINDOWS )
	if( IsDebuggerAvailable() )
	{
		OutputDebugStringA( messageLiteral );
	}
#endif

	return std::string(messageLiteral);
}