#pragma once

//-----------------------------------------------------------------------------------------------
#define UNUSED(x) (void)(x);

#include <string>
#include <vector>
#include "Engine\Math\Vec3.hpp"
#include "Engine\Core\RGBAColors.hpp"
#include "Engine\Renderer\Light.hpp"
#include "Engine\Renderer\Renderer.hpp"

enum GAME_COMMON_LOAD_TYPE
{
	LOAD_FROM_DATA_ONLY,
	LOAD_FROM_ARCHIVE_ONLY,
	PREFER_LOADING_FROM_DATA,
	PREFER_LOADING_FROM_ARCHIVE
};

extern GAME_COMMON_LOAD_TYPE g_currentLoadPreference;

extern float g_numOfTileUnitsY;
extern float g_numOfTileUnitsX;

extern float g_maxNumOfTileUnitsXOnScreen;
extern float g_maxNumOfTileUnitsYOnScreen;

extern const float g_maxScreenY;
extern const float g_maxScreenX;
extern const float g_windowDisplacement;

extern const float g_PI;
extern const float g_charToFloat;

extern int g_numOfLetters;

extern std::vector<int> g_materialIDIndexes;

extern std::vector<Light> g_lights;

extern Vec3 g_cameraPos;
extern const float g_zFar;

extern std::string g_dataPassword;

extern float g_goalDeltaTime;
extern float g_actualDeltaTime;
