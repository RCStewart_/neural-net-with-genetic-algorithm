#pragma once

//-----------------------------------------------------------------------------------------------
#include <string>
#include "Engine/Math/Vec3.hpp"
#include "Engine/Math/Vec2.hpp"

extern Vec3 g_left;
extern Vec3 g_right;
extern Vec3 g_up;
extern Vec3 g_down;
extern Vec3 g_forward;
extern Vec3 g_backward;

extern Vec2 g_north;
extern Vec2 g_south;
extern Vec2 g_east;
extern Vec2 g_west;


Vec3 GetVec3FromStringDirection( std::string direction );

void ClearFile( const std::string& filePathToWriteTo );
void LogAppendPrintf( const std::string& filePathToWriteTo, const char* messageFormat, ... );
std::string GetMessageLiteralf( const char* messageFormat, ... );