#pragma once
#include <string>
#include <iostream>
#include <map>
#include "Engine\Math\Vec2.hpp"

#include "Engine\Renderer\GLWrapper.hpp"

#include <cstdio>

enum DefaultTexture
{
	DefaultTextureUnspecified,
	DefaultTextureSpecGlossEmissive,
	DefaultTextureDiffuse,
	DefaultTextureNormal
};

class Texture
{
public:
	Texture( const std::string& imageFilePath, DefaultTexture typeToDefaultTo = DefaultTextureUnspecified );
	Texture( unsigned char* data);
	static Texture* Texture::GetTextureByName( const std::string& imageFilePath, DefaultTexture typeToDefaultTo = DefaultTextureUnspecified  );
	static Texture* Texture::CreateOrGetTexture( const std::string& imageFilePath, DefaultTexture typeToDefaultTo = DefaultTextureUnspecified  );
	static void Texture::createDefaultTextures();
	static Texture* Texture::getDefaultTexture( DefaultTexture typeToDefaultTo );

	static std::map< std::string, Texture* >	Texture::s_textureRegistry;
	unsigned int m_openglTextureID;

	int m_sizeX;
	int m_sizeY;
};