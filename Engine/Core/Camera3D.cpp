#include "Engine\Core\Camera3D.hpp"
#include "Engine\Core\UtilityFunctions.hpp"
//#include "TheGame.hpp"
#include "Engine\Core\WindowsWrapper.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Core\GameCommon.hpp"
#include "Engine\Math\Time.hpp"
#include "Engine\Math\RandomNumber.hpp"
#include <algorithm>
#include "Engine\Input\ConsoleLog.hpp"
#include "Engine\Renderer\GLWrapper.hpp"

const float horizontalBoundaryDisplacement = .3f;
const float topVerticalBoundaryDisplacement = .23f;
const float midVerticalBoundaryDisplacement = -.62f;
const float bottomVerticalBoundaryDisplacement = -1.62f;

//-------------------------------------------------
Camera3D::Camera3D(void)
{
	m_up = Vec3(0.0f, 0.0f, 1.0f);
	m_left = Vec3(-1.0f, 0.0f, 0.0f);

	m_position = Vec3(-1.f, 0.f, 2.0f);
	m_orientation = EulerAngles(0.0f, 0.0f, 0.0f);
}


//----------------------------------------------------------------------------------
Camera3D::Camera3D(Vec3& startingPosition, EulerAngles& startingOrientation)
{
	m_up = Vec3(0.0f, 0.0f, 1.0f);
	m_left = Vec3(-1.0f, 0.0f, 0.0f);

	m_position = startingPosition;
	m_orientation = startingOrientation;
}


//-------------------------------------------------
void Camera3D::clampPitch()
{
	UtilityFunctions::clampFloat(m_orientation.pitchDegreesAboutY, -89.f, 89.f);
}


//-------------------------------------------------
Camera3D::~Camera3D(void)
{
}


//---------------------------------------------------------------------------
void Camera3D::updateCameraFromMouseAndKeyboard( float deltaSeconds )
{
	if(ConsoleLog::s_isLogActive)
	{
		return;
	}
	float speed = 7.f;

	// Update camera orientation (yaw and pitch only) from mouse x,y movement
	const float degreesPerMouseDelta = 0.04f;
	Vec2 mouseDeltas = getMouseMovementSinceLastChecked();
	m_orientation.yawDegreesAboutZ		-= (degreesPerMouseDelta * mouseDeltas.x);
	m_orientation.pitchDegreesAboutY	+= (degreesPerMouseDelta * mouseDeltas.y);

	// Update camera position based on which (if any) movement keys are down
	float cameraYawRadians = UtilityFunctions::degreesToRadians( m_orientation.yawDegreesAboutZ);
	Vec3 cameraForwardXYVector( cos( cameraYawRadians ), sin( cameraYawRadians ), 0.f);
	Vec3 cameraRightVector = Vec3( cos(cameraYawRadians + UtilityFunctions::degreesToRadians(90)), sin(cameraYawRadians + UtilityFunctions::degreesToRadians(90)), 0.0f);
	Vec3 movementVector = Vec3(0.0f, 0.0f, 0.0f);

	//...and so on, for A,S,D moving left, back, right - and for E,C moving up, down.

	if(WindowWrapperIsInputActive('A'))
	{
		movementVector = Vec3::addVectors(movementVector, cameraRightVector);
	}

	if(WindowWrapperIsInputActive('D'))
	{
		cameraRightVector.scalarMultiplication(-1);
		movementVector = Vec3::addVectors(movementVector, cameraRightVector);
	}

	if(WindowWrapperIsInputActive('W'))
	{
		movementVector = Vec3::addVectors(movementVector, cameraForwardXYVector);
	}

	if(WindowWrapperIsInputActive('S'))
	{
		Vec3 cameraBackXYVector(-cameraForwardXYVector.x, -cameraForwardXYVector.y, 0.f);
		movementVector = Vec3::addVectors(movementVector, cameraBackXYVector);
	}

	if(WindowWrapperIsInputActive(' '))
	{
		movementVector = Vec3::addVectors(movementVector, m_up);
	}

	if(WindowWrapperIsInputActive('X'))
	{
		Vec3 down = m_up;
		down.scalarMultiplication(-1);
		movementVector = Vec3::addVectors(movementVector, down);
	}

	if(WindowWrapperIsInputActive(WINDOW_WRAPPER_INPUT_SHIFT))
	{
		speed *= 8.0f;
	}

	movementVector.scalarMultiplication(speed);
	movementVector.scalarMultiplication(deltaSeconds);

	m_position = Vec3::addVectors(m_position, movementVector);

	clampPitch();

	g_cameraPos = m_position;
}


//---------------------------------------------------------------------------
Vec2 Camera3D::getMouseMovementSinceLastChecked()
{
	return WindowWrapperGetMouseMovementSinceLastChecked();
}

//---------------------------------------------------------------------------
void Camera3D::applyCameraTransform()
{
	// Put us in our preferred coordinate system: +X is east (forward), +Y is north, +Z is up
	GLWrapperRotate( Vec3(1.f, 0.f, 0.f), -90.f ); // lean "forward" 90 degrees, to put +Z up (was +Y)
	g_theRenderer->rotationMatrix( -90.f, 1.f, 0.f, 0.f );

	GLWrapperRotate( Vec3(0.f, 0.f, 1.f), 90.f ); // spin "left" 90 degrees, to put +X forward (was +Y)

	// Orient the view per the camera's orientation
	GLWrapperRotate( Vec3(1.f, 0.f, 0.f), -m_orientation.rollDegreesAboutX );
	GLWrapperRotate( Vec3(0.f, 1.f, 0.f), -m_orientation.pitchDegreesAboutY );
	GLWrapperRotate( Vec3(0.f, 0.f, 1.f), -m_orientation.yawDegreesAboutZ );


	
	g_theRenderer->rotationMatrix( 90.f, 0.f, 0.f, 1.f );

	g_theRenderer->rotationMatrix( -m_orientation.rollDegreesAboutX, 1.f, 0.f, 0.f );
	g_theRenderer->rotationMatrix( -m_orientation.pitchDegreesAboutY, 0.f, 1.f, 0.f );
	g_theRenderer->rotationMatrix( -m_orientation.yawDegreesAboutZ, 0.f, 0.f, 1.f );

	// Position the view per the camera's position
	Vec3 negCameraPos = m_position;
	negCameraPos.scalarMultiplication(-1.f);
	GLWrapperTranslate( negCameraPos );

	g_theRenderer->translateMatrix( negCameraPos.x, negCameraPos.y, negCameraPos.z );
}


//----------------------------------------------------------------------------------
void Camera3D::applyCameraTransform( Renderer* gameRenderer )
{
	// Put us in our preferred coordinate system: +X is east (forward), +Y is north, +Z is up
	GLWrapperRotate( Vec3(1.f, 0.f, 0.f), -90.f ); // lean "forward" 90 degrees, to put +Z up (was +Y)
	gameRenderer->rotationMatrix( -90.f, 1.f, 0.f, 0.f );

	GLWrapperRotate( Vec3(0.f, 0.f, 1.f), 90.f ); // spin "left" 90 degrees, to put +X forward (was +Y)

	// Orient the view per the camera's orientation
	GLWrapperRotate( Vec3(1.f, 0.f, 0.f), -m_orientation.rollDegreesAboutX );
	GLWrapperRotate( Vec3(0.f, 1.f, 0.f), -m_orientation.pitchDegreesAboutY );
	GLWrapperRotate( Vec3(0.f, 0.f, 1.f), -m_orientation.yawDegreesAboutZ );



	gameRenderer->rotationMatrix( 90.f, 0.f, 0.f, 1.f );

	gameRenderer->rotationMatrix( -m_orientation.rollDegreesAboutX, 1.f, 0.f, 0.f );
	gameRenderer->rotationMatrix( -m_orientation.pitchDegreesAboutY, 0.f, 1.f, 0.f );
	gameRenderer->rotationMatrix( -m_orientation.yawDegreesAboutZ, 0.f, 0.f, 1.f );

	// Position the view per the camera's position
	Vec3 negCameraPos = m_position;
	negCameraPos.scalarMultiplication(-1.f);
	GLWrapperTranslate( negCameraPos );

	gameRenderer->translateMatrix( negCameraPos.x, negCameraPos.y, negCameraPos.z );
}