#include "RGBAColors.hpp"

//---------------------------------------------------------------------------
RGBAColors::RGBAColors(void)
	: m_red( 255 )
	, m_green( 255 )
	, m_blue( 255 )
	, m_alpha( 255 )
{
}


//---------------------------------------------------------------------------
RGBAColors::RGBAColors(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
	: m_red( red )
	, m_green( green )
	, m_blue( blue )
	, m_alpha( alpha )
{
}


//---------------------------------------------------------------------------
RGBAColors::~RGBAColors(void)
{
}

//----------------------------------------------------------------------------------
RGBAColors RGBAColors::getColorMultipliedByValue( float multiple )
{
	return RGBAColors( (unsigned char) ((float) m_red * multiple ), (unsigned char) ((float) m_green * multiple), (unsigned char) ((float) m_blue * multiple), m_alpha );
}

//----------------------------------------------------------------------------------
bool RGBAColors::operator==(const RGBAColors& rhs)
{ 
	return	m_red == rhs.m_red
		&&	m_green == rhs.m_green
		&&	m_blue == rhs.m_blue
		&&	m_alpha == rhs.m_alpha;
}

//----------------------------------------------------------------------------------
bool RGBAColors::operator==(const RGBAColors& rhs) const
{ 
	return	m_red == rhs.m_red
		&&	m_green == rhs.m_green
		&&	m_blue == rhs.m_blue
		&&	m_alpha == rhs.m_alpha;
}