#include "AnyDataTypeMap.hpp"
#include "StringTable.hpp"

//----------------------------------------------------------------------------------
Base* AnyDataTypeMap::get(const std::string& key)
{
	return m_stringAsIntToDataMap[StringID(key)];
}

//----------------------------------------------------------------------------------
void  AnyDataTypeMap::deleteAtKey(const std::string& key)
{
	if (m_stringAsIntToDataMap.find(StringID(key)) == m_stringAsIntToDataMap.end()) {
		//not found

	}
	else {
		//found
		delete m_stringAsIntToDataMap[StringID(key)];
		m_stringAsIntToDataMap[StringID(key)] = nullptr;
	}

}

//----------------------------------------------------------------------------------
void  AnyDataTypeMap::fillTargetMapWithDataFromSelf(AnyDataTypeMap& toFill)
{
	typedef std::map<int, Base*> ::iterator it;
	for (it iterator = m_stringAsIntToDataMap.begin(); iterator != m_stringAsIntToDataMap.end(); iterator++) {
		if (StringValue(iterator->first))
		toFill.updateValueAtKey(StringValue(iterator->first), iterator->second);
	}
}

//----------------------------------------------------------------------------------
void  AnyDataTypeMap::setDeepCopy(const std::string& key, Base* data)
{
	deleteAtKey(key);
	m_stringAsIntToDataMap[StringID(key)] = data->getClone();
}

//----------------------------------------------------------------------------------
void  AnyDataTypeMap::updateValueAtKey(const std::string& key, Base* data)
{
	if (m_stringAsIntToDataMap.find(StringID(key)) == m_stringAsIntToDataMap.end()) {
		//not found
		setDeepCopy(key, data);
	}
	else {
		//found
		m_stringAsIntToDataMap[StringID(key)]->updateValues(data);
	}
}