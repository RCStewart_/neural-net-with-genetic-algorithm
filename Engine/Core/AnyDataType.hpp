#pragma once
#include "Engine/Core/StringTable.hpp"

//----------------------------------------------------------------------------------
class Base
{
public:
	Base(){};
	~Base(){};
	virtual Base* getClone() = 0;
	virtual void  updateValues(Base* toCopy) = 0;

protected:
	Base* clone(){ return getClone(); };
};

//----------------------------------------------------------------------------------
template<class T>
class AnyDataType : public Base
{
public:
	//----------------------------------------------------------------------------------
	AnyDataType(const T& data, const std::string& nameOfAttribute)
		: m_data(data)
	{
		m_stringID = StringID(nameOfAttribute);
	};

	//----------------------------------------------------------------------------------
	AnyDataType(AnyDataType<T>& toCopy)
		: m_data(toCopy.m_data)
		, m_stringID(toCopy.m_stringID)
	{
	}

	~AnyDataType(){};

	//----------------------------------------------------------------------------------
	virtual Base* getClone()
	{
		return new AnyDataType<T>(*this);
	};

	//----------------------------------------------------------------------------------
	virtual void updateValues(Base* toCopy)
	{
		AnyDataType<T>* temp = static_cast<AnyDataType<T>*>(toCopy);
		m_data = temp->m_data;
		m_stringID = temp->m_stringID;
	}

	T m_data;
	int m_stringID;

protected:
	AnyDataType<T>* clone(){ return getClone()};
};