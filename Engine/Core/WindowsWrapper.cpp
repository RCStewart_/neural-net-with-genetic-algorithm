#include "Engine\Core\WindowsWrapper.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Core\GameCommon.hpp"
#include "Engine\Math\Vec2.hpp"
#include "Engine\Math\IntVec2.hpp"
#include "Engine\Input\ConsoleLog.hpp"

//-----------------------------------------------------------------------------------------------
bool g_isQuitting = false;
HWND g_hWnd = nullptr;
HDC g_displayDeviceContext = nullptr;
HGLRC g_openGLRenderingContext = nullptr;
const char* APP_NAME = "Graphics Library";
std::vector<bool> WindowWrapperActiveInputs;
//std::vector<bool> /*WindowsWrapper::*/WindowWrapperActiveInputs;


//From http://stackoverflow.com/questions/27220/how-to-convert-stdstring-to-lpcwstr-in-c-unicode
//----------------------------------------------------------------------------------
std::wstring stringToWString(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

//----------------------------------------------------------------------------------
void WindowsWrapperMessageBox( std::string& message, std::string& caption )
{
	wstring wMessage = stringToWString(message);
	wstring wCaption = stringToWString(caption);
	MessageBox(0, (LPCWSTR) wMessage.c_str(), (LPCWSTR) wCaption.c_str(), MB_OK);
}

//-----------------------------------------------------------------------------------------------
void RunMessagePump()
{
	MSG queuedMessage;
	for( ;; )
	{
		const BOOL wasMessagePresent = PeekMessage( &queuedMessage, NULL, 0, 0, PM_REMOVE );
		if( !wasMessagePresent )
		{
			break;
		}
		TranslateMessage( &queuedMessage );
		DispatchMessage( &queuedMessage );
	}
}


//----------------------------------------------------------------------------------
void WindowsWrapperFindMemoryLeaks()
{
	_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)|_CRTDBG_LEAK_CHECK_DF);
	//_crtBreakAlloc = 8916;  //Put = to number in debug log
}


//----------------------------------------------------------------------------------
void WindowWrapperSetProcessDPIAware()
{
	SetProcessDPIAware();
}


//----------------------------------------------------------------------------------
void WindowWrapperCreateWindow( HINSTANCE applicationInstanceHandle )
{
	WindowWrapperCreateOpenGLWindow( applicationInstanceHandle );
}


//----------------------------------------------------------------------------------
void WindowWrapperCreateOpenGLWindow( HINSTANCE applicationInstanceHandle )
{
	// Define a window class
	WNDCLASSEX windowClassDescription;
	memset( &windowClassDescription, 0, sizeof( windowClassDescription ) );
	windowClassDescription.cbSize = sizeof( windowClassDescription );
	windowClassDescription.style = CS_OWNDC; // Redraw on move, request own Display Context
	windowClassDescription.lpfnWndProc = static_cast< WNDPROC >( WindowWrapperMessageHandlingProcedure ); // Assign a win32 message-handling function
	windowClassDescription.hInstance = GetModuleHandle( NULL );
	windowClassDescription.hIcon = NULL;
	windowClassDescription.hCursor = NULL;
	windowClassDescription.lpszClassName = TEXT( "Simple Window Class" );
	RegisterClassEx( &windowClassDescription );

	const DWORD windowStyleFlags = WS_CAPTION | WS_BORDER | WS_THICKFRAME | WS_SYSMENU | WS_OVERLAPPED;
	const DWORD windowStyleExFlags = WS_EX_APPWINDOW;

	RECT desktopRect;
	HWND desktopWindowHandle = GetDesktopWindow();
	GetClientRect( desktopWindowHandle, &desktopRect );

	RECT windowRect = { (LONG) g_windowDisplacement, (LONG) g_windowDisplacement, (LONG)(g_windowDisplacement + (LONG) g_maxScreenX), (LONG)(g_windowDisplacement + (LONG) g_maxScreenY) };
	AdjustWindowRectEx( &windowRect, windowStyleFlags, FALSE, windowStyleExFlags );

	WCHAR windowTitle[ 1024 ];
	MultiByteToWideChar( GetACP(), 0, APP_NAME, -1, windowTitle, sizeof(windowTitle)/sizeof(windowTitle[0]) );
	g_hWnd = CreateWindowEx(
		windowStyleExFlags,
		windowClassDescription.lpszClassName,
		windowTitle,
		windowStyleFlags,
		windowRect.left,
		windowRect.top,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		applicationInstanceHandle,
		NULL );

	ShowWindow( g_hWnd, SW_SHOW );
	SetForegroundWindow( g_hWnd );
	SetFocus( g_hWnd );

	g_displayDeviceContext = GetDC( g_hWnd );

	HCURSOR cursor = LoadCursor( NULL, IDC_ARROW );
	SetCursor( cursor );

	PIXELFORMATDESCRIPTOR pixelFormatDescriptor;
	memset( &pixelFormatDescriptor, 0, sizeof( pixelFormatDescriptor ) );
	pixelFormatDescriptor.nSize			= sizeof( pixelFormatDescriptor );
	pixelFormatDescriptor.nVersion		= 1;
	pixelFormatDescriptor.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelFormatDescriptor.iPixelType	= PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits	= 24;
	pixelFormatDescriptor.cDepthBits	= 24;
	pixelFormatDescriptor.cAccumBits	= 0;
	pixelFormatDescriptor.cStencilBits	= 8;

	int pixelFormatCode = ChoosePixelFormat( g_displayDeviceContext, &pixelFormatDescriptor );
	SetPixelFormat( g_displayDeviceContext, pixelFormatCode, &pixelFormatDescriptor );
	g_openGLRenderingContext = wglCreateContext( g_displayDeviceContext );
	wglMakeCurrent( g_displayDeviceContext, g_openGLRenderingContext );

	glLineWidth( 1.5f );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glEnable( GL_LINE_SMOOTH );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
}

//----------------------------------------------------------------------------------
void WindowWrapperCreateOpenGLOrthoWindow( HINSTANCE applicationInstanceHandle )
{
	// Define a window class
	WNDCLASSEX windowClassDescription;
	memset( &windowClassDescription, 0, sizeof( windowClassDescription ) );
	windowClassDescription.cbSize = sizeof( windowClassDescription );
	windowClassDescription.style = CS_OWNDC; // Redraw on move, request own Display Context
	windowClassDescription.lpfnWndProc = static_cast< WNDPROC >( WindowWrapperMessageHandlingProcedure ); // Assign a win32 message-handling
	windowClassDescription.hInstance = GetModuleHandle( NULL );
	windowClassDescription.hIcon = NULL;
	windowClassDescription.hCursor = NULL;
	windowClassDescription.lpszClassName = TEXT( "Simple Window Class" );
	RegisterClassEx( &windowClassDescription );

	const DWORD windowStyleFlags = WS_CAPTION | WS_BORDER | WS_THICKFRAME | WS_SYSMENU | WS_OVERLAPPED;
	const DWORD windowStyleExFlags = WS_EX_APPWINDOW;

	RECT desktopRect;
	HWND desktopWindowHandle = GetDesktopWindow();
	GetClientRect( desktopWindowHandle, &desktopRect );

	RECT windowRect = { 50 + 0, 50 + 0, (LONG)(50 + g_maxScreenX), (LONG)(50 + g_maxScreenY) };
	AdjustWindowRectEx( &windowRect, windowStyleFlags, FALSE, windowStyleExFlags );

	WCHAR windowTitle[ 1024 ];
	MultiByteToWideChar( GetACP(), 0, APP_NAME, -1, windowTitle, sizeof(windowTitle)/sizeof(windowTitle[0]) );
	g_hWnd = CreateWindowEx(
		windowStyleExFlags,
		windowClassDescription.lpszClassName,
		windowTitle,
		windowStyleFlags,
		windowRect.left,
		windowRect.top,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		applicationInstanceHandle,
		NULL );

	ShowWindow( g_hWnd, SW_SHOW );
	SetForegroundWindow( g_hWnd );
	SetFocus( g_hWnd );

	g_displayDeviceContext = GetDC( g_hWnd );

	HCURSOR cursor = LoadCursor( NULL, IDC_ARROW );
	SetCursor( cursor );

	PIXELFORMATDESCRIPTOR pixelFormatDescriptor;
	memset( &pixelFormatDescriptor, 0, sizeof( pixelFormatDescriptor ) );
	pixelFormatDescriptor.nSize			= sizeof( pixelFormatDescriptor );
	pixelFormatDescriptor.nVersion		= 1;
	pixelFormatDescriptor.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelFormatDescriptor.iPixelType	= PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits	= 24;
	pixelFormatDescriptor.cDepthBits	= 24;
	pixelFormatDescriptor.cAccumBits	= 0;
	pixelFormatDescriptor.cStencilBits	= 8;

	int pixelFormatCode = ChoosePixelFormat( g_displayDeviceContext, &pixelFormatDescriptor );
	SetPixelFormat( g_displayDeviceContext, pixelFormatCode, &pixelFormatDescriptor );
	g_openGLRenderingContext = wglCreateContext( g_displayDeviceContext );
	wglMakeCurrent( g_displayDeviceContext, g_openGLRenderingContext );
	glOrtho( 0.0, g_numOfTileUnitsX, 0.0, g_numOfTileUnitsY, 0.f, 1.f );
	glLineWidth( 1.5f );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glEnable( GL_LINE_SMOOTH );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
}

//----------------------------------------------------------------------------------
void WindowWrapperRunMessagePump()
{
	//WindowWrapperResetInputArray();
	MSG queuedMessage;
	for( ;; )
	{
		const BOOL wasMessagePresent = PeekMessage( &queuedMessage, NULL, 0, 0, PM_REMOVE );
		if( !wasMessagePresent )
		{
			break;
		}
		TranslateMessage( &queuedMessage );
		DispatchMessage( &queuedMessage );
	}
}

//----------------------------------------------------------------------------------
LRESULT CALLBACK WindowWrapperMessageHandlingProcedure( HWND windowHandle, UINT wmMessageCode, WPARAM wParam, LPARAM lParam )
{
	unsigned char asKey = (unsigned char) wParam;
	switch( wmMessageCode )
	{
	case WM_CLOSE:
	case WM_DESTROY:
	case WM_QUIT:
		g_isQuitting = true;
		return 0;
	case WM_CHAR:
		WindowWrapperActiveInputs[wParam] = true;
		break;
	case WM_KEYDOWN:
		if( asKey == VK_ESCAPE )
		{
			if(!ConsoleLog::s_isLogActive)
				g_isQuitting = true;
			WindowWrapperActiveInputs[VK_ESCAPE] = true;
			return 0;
		}
		if( asKey == VK_SHIFT )
		{
			WindowWrapperActiveInputs[VK_SHIFT] = true;
			return 0;
		}
		if( asKey == VK_RIGHT )
		{
			WindowWrapperActiveInputs[VK_RIGHT] = true;
			return 0;
		}
		if( asKey == VK_LEFT )
		{
			WindowWrapperActiveInputs[VK_LEFT] = true;
			return 0;
		}
		if( asKey == VK_RETURN)
		{
			WindowWrapperActiveInputs[VK_RETURN] = true;
			return 0;
		}
		if( asKey == VK_DELETE)
		{
			WindowWrapperActiveInputs[VK_DELETE] = true;
			return 0;
		}
		if( asKey == VK_HOME)
		{
			WindowWrapperActiveInputs[VK_HOME] = true;
			return 0;
		}
		if( asKey == VK_END)
		{
			WindowWrapperActiveInputs[VK_END] = true;
			return 0;
		}
		if(!ConsoleLog::s_isLogActive)
		{
			if(wParam < WindowWrapperActiveInputs.size())
				WindowWrapperActiveInputs[wParam] = true;
		}
		break;

	case WM_KEYUP:
			{
				if(wParam < WindowWrapperActiveInputs.size())
					WindowWrapperActiveInputs[wParam] = false;
			}
			if( wParam == VK_SHIFT )
			{
				WindowWrapperActiveInputs[VK_SHIFT] = false;
			}
			if( wParam == VK_SPACE )
			{
				WindowWrapperActiveInputs[VK_SPACE] = false;
			}
			if( asKey == VK_RIGHT )
			{
				WindowWrapperActiveInputs[VK_RIGHT] = false;
			}
			if( asKey == VK_LEFT )
			{
				WindowWrapperActiveInputs[VK_LEFT] = false;
			}
			if( asKey == VK_DELETE)
			{
				WindowWrapperActiveInputs[VK_DELETE] = false;
			}
			if( asKey == VK_HOME)
			{
				WindowWrapperActiveInputs[VK_HOME] = false;
			}
			if( asKey == VK_END)
			{
				WindowWrapperActiveInputs[VK_END] = false;
			}
			break;
	case WM_RBUTTONDOWN:
		WindowWrapperActiveInputs[WINDOW_WRAPPER_INPUT_RIGHT_MOUSE_CLICK] = true;
		break;

	case WM_LBUTTONDOWN:
		WindowWrapperActiveInputs[WINDOW_WRAPPER_INPUT_LEFT_MOUSE_CLICK] = true;
		break;

	case WM_MOUSEWHEEL:
		if(GET_WHEEL_DELTA_WPARAM(wParam) < 0)
		{
			WindowWrapperActiveInputs[WINDOW_WRAPPER_INPUT_MOUSE_WHEEL_DOWN] = true;
		}
		else
		{
			WindowWrapperActiveInputs[WINDOW_WRAPPER_INPUT_MOUSE_WHEEL_UP] = true;
		}
		break;
	}
	return DefWindowProc( windowHandle, wmMessageCode, wParam, lParam );
}


//---------------------------------------------------------------------------
double InitializeTime( LARGE_INTEGER& out_initialTime )
{
	LARGE_INTEGER countsPerSecond;
	QueryPerformanceFrequency( &countsPerSecond );
	QueryPerformanceCounter( &out_initialTime );
	return( 1.0 / static_cast< double >( countsPerSecond.QuadPart ) );
}


//----------------------------------------------------------------------------------
double WindowWrapperGetCurrentTimeSeconds()
{
	static LARGE_INTEGER initialTime;
	static double secondsPerCount = InitializeTime( initialTime );
	LARGE_INTEGER currentCount;
	QueryPerformanceCounter( &currentCount );
	LONGLONG elapsedCountsSinceInitialTime = currentCount.QuadPart - initialTime.QuadPart;

	double currentSeconds = static_cast< double >( elapsedCountsSinceInitialTime ) * secondsPerCount;
	return currentSeconds;
}


//-----------------------------------------------------------------------------------------------
// Print a string to the Visual Studio "Output" debug window, if we are running in the debugger.
// Uses printf-style formatting, for example:
// WindowWrapperDebuggerPrintf( "%s is a %i-year-old %s.", studentName, studentAge, studentGender );
// Provided by Squirrel Eiserloh
void WindowWrapperDebuggerPrintf( const char* messageFormat, ... )
{
	const int MESSAGE_MAX_LENGTH = 2048;
	char messageLiteral[ MESSAGE_MAX_LENGTH ];
	va_list variableArgumentList;
	va_start( variableArgumentList, messageFormat );
	vsnprintf_s( messageLiteral, MESSAGE_MAX_LENGTH, _TRUNCATE, messageFormat, variableArgumentList );
	va_end( variableArgumentList );
	messageLiteral[ MESSAGE_MAX_LENGTH - 1 ] = '\0'; // In case vsnprintf overran (doesn't auto-terminate)

	//#if defined( PLATFORM_WINDOWS )
	//	if( IsDebuggerAvailable() )
	//	{
	OutputDebugStringA( messageLiteral ); // Print the string to the "Output" debug window in Visual Studio
	//	}
	//#endif

	std::cout << messageLiteral; // Also write it out to standard out, if anyone is listening
}

//----------------------------------------------------------------------------------------------
Vec2 WindowWrapperGetMouseMovementSinceLastChecked()
{
	POINT centerCursorPos = { (LONG) (g_maxScreenX * .5f), (LONG) (g_maxScreenY * .5f) };
	POINT cursorPos;
	GetCursorPos( &cursorPos );
	SetCursorPos( centerCursorPos.x, centerCursorPos.y );
	IntVec2 mouseDeltaInts( cursorPos.x - centerCursorPos.x, cursorPos.y - centerCursorPos.y );
	Vec2 mouseDeltas( (float) mouseDeltaInts.x, (float) mouseDeltaInts.y );
	return mouseDeltas;
}

//----------------------------------------------------------------------------------------------
Vec2 WindowWrapperGetMousePosition()
{
	POINT cursorPos;
	GetCursorPos( &cursorPos );
	Vec2 mousePos( (float) cursorPos.x - g_windowDisplacement, (float) cursorPos.y - g_windowDisplacement );
	return mousePos;
}

//----------------------------------------------------------------------------------------------
void WindowWrapperSetCursorVisibility(bool isEnabled)
{
	ShowCursor(isEnabled);
}


//----------------------------------------------------------------------------------------------
void WindowWrapperInitialization()
{
	for(int i = 0; i < g_numOfLetters; ++i)
	{
		WindowWrapperActiveInputs.push_back(false);
	}
}


//----------------------------------------------------------------------------------------------
void WindowWrapperResetInputArray()
{
	for(int i = 0; i < (int) WindowWrapperActiveInputs.size(); ++i)
	{
		//if(WindowWrapperIsASinglePressKey((WINDOW_WRAPPER_INPUTS) i))
		{	
			WindowWrapperActiveInputs[i] = false;
		}
	}
}


//----------------------------------------------------------------------------------------------
bool WindowWrapperIsInputActive( char keyToCheck ) 
{
	if(keyToCheck < 0)
		return false;
	if(keyToCheck >= (int) WindowWrapperActiveInputs.size())
		return false;
	return WindowWrapperActiveInputs[keyToCheck];
}


//----------------------------------------------------------------------------------
bool WindowWrapperGetIsInputActiveAndConsume( char keyToCheck )
{
	bool isActive = WindowWrapperIsInputActive( keyToCheck );
	if(keyToCheck < 0)
		return false;
	if(keyToCheck >= (int) WindowWrapperActiveInputs.size())
		return false;
	WindowWrapperActiveInputs[keyToCheck] = false;
	return isActive;
}

//----------------------------------------------------------------------------------------------
int WindowWrapperGetScrollWheelInput()
{
	if(WindowWrapperActiveInputs[WINDOW_WRAPPER_INPUT_MOUSE_WHEEL_DOWN])
		return -1;
	if(WindowWrapperActiveInputs[WINDOW_WRAPPER_INPUT_MOUSE_WHEEL_UP])
		return 1;
	return 0;
}
