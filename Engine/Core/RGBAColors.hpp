#pragma once
class RGBAColors
{
public:
	RGBAColors(void);
	~RGBAColors(void);

	RGBAColors(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);

	RGBAColors getColorMultipliedByValue( float multiple );

	bool operator==( const RGBAColors& rhs);
	bool operator==( const RGBAColors& rhs) const;

	unsigned char m_red;
	unsigned char m_green;
	unsigned char m_blue;
	unsigned char m_alpha;
};

