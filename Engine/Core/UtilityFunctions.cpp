#include "Engine\Core\UtilityFunctions.hpp"
#include "Engine\Math\Vec2.hpp"
#include "Engine\Math\IntVec2.hpp"

const double PI = 3.14159265;

//---------------------------------------------------------------------------
UtilityFunctions::UtilityFunctions(void)
{
}


//---------------------------------------------------------------------------
UtilityFunctions::~UtilityFunctions(void)
{
}


//---------------------------------------------------------------------------
void UtilityFunctions::clampFloat(float& valueToClamp, const float& minValue, const float& maxValue, float radiusOfValueToClamp)
{
	if((valueToClamp + radiusOfValueToClamp) > maxValue)
	{
		valueToClamp = maxValue - radiusOfValueToClamp;
	}
	else if((valueToClamp - radiusOfValueToClamp) < minValue)
	{
		valueToClamp = minValue + radiusOfValueToClamp;
	}
}

//---------------------------------------------------------------------------
void UtilityFunctions::clampDouble(double& valueToClamp, const double& minValue, const double& maxValue)
{
	if (valueToClamp > maxValue)
	{
		valueToClamp = maxValue;
	}
	else if (valueToClamp < minValue)
	{
		valueToClamp = minValue;
	}
}

//---------------------------------------------------------------------------
bool UtilityFunctions::detectCircleCollision(const float& circleOneRadius, const Vec2& circleOnePosition, const float& circleTwoRadius, const Vec2& circleTwoPosition)
{
	bool hasCollided = false;
	float xDistance = circleOnePosition.x - circleTwoPosition.x;
	float yDistance = circleOnePosition.y - circleTwoPosition.y;

	float distanceBetweenCircles = xDistance * xDistance + yDistance * yDistance;

	float collisionDistance = circleOneRadius + circleTwoRadius;
	collisionDistance *= collisionDistance;

	if(collisionDistance >= distanceBetweenCircles)
	{
		hasCollided = true;
	}
	return hasCollided;
}


//---------------------------------------------------------------------------
float UtilityFunctions::computeShortestAngularDisplacement(float fromDegrees, float toDegrees)
{
	float degreesToGoal = toDegrees - fromDegrees;

	while( degreesToGoal > 180.f)
	{
		degreesToGoal -= 360.f;
	}
	while( degreesToGoal < -180.f)
	{
		degreesToGoal += 360.f;
	}
	return degreesToGoal;
}


//---------------------------------------------------------------------------
float UtilityFunctions::CalcDistSquaredBetweenPoints(const Vec2& a, const Vec2& b)
{
	float xDist = a.x - b.x;
	float yDist = a.y - b.y;
	return (xDist * xDist) + (yDist * yDist);
}


//---------------------------------------------------------------------------
int UtilityFunctions::CalcDistSquaredBetweenPoints(const IntVec2& a, const IntVec2& b)
{
	int xDist = a.x - b.x;
	int yDist = a.y - b.y;
	return (xDist * xDist) + (yDist * yDist);
}


//---------------------------------------------------------------------------
float UtilityFunctions::degreesToRadians(const float& degrees)
{
	float convertRatio = (float) (PI/180.f);
	float toReturn = degrees * convertRatio;
	return toReturn;
}


//---------------------------------------------------------------------------
float UtilityFunctions::radiansToDegrees(const float& radians)
{
	float convertRatio = (float) (180.f/PI);
	float toReturn = radians * convertRatio;
	return toReturn;
}

//----------------------------------------------------------------------------------
bool UtilityFunctions::detectCircleCollision(float circleOneRadius, Vec2 &circleOnePosition, float circleTwoRadius, Vec2 &circleTwoPosition)
{
	bool hasCollided = false;
	float xDistance = circleOnePosition.x - circleTwoPosition.x;
	float yDistance = circleOnePosition.y - circleTwoPosition.y;

	float distanceBetweenCircles = xDistance*xDistance + yDistance*yDistance;

	float collisionDistance = circleOneRadius + circleTwoRadius;
	collisionDistance *= collisionDistance;

	if (collisionDistance >= distanceBetweenCircles)
	{
		hasCollided = true;
	}
	return hasCollided;
}

//----------------------------------------------------------------------------------
unsigned int UtilityFunctions::getFileLength(const std::string filePath)
{
	int isLoaded = 0;

	std::string fileName = filePath;

	FILE* f = nullptr;
	fopen_s( &f, fileName.c_str(), "rb");

	if(f == nullptr)
	{
		return isLoaded;
	}
	isLoaded = 0;

	fseek(f, 0, SEEK_END);
	size_t fileSize = ftell(f);
	rewind(f);

	isLoaded = (unsigned int) fileSize;

	return isLoaded;
}

//----------------------------------------------------------------------------------
int UtilityFunctions::loadFileToNewBuffer( const std::string filePath, unsigned char* buffer )
{
	int isLoaded = -1;
	
	std::string fileName = filePath;
	
	FILE* f = nullptr;
	fopen_s( &f, fileName.c_str(), "rb");
	
	if(f == nullptr)
	{
		return isLoaded;
	}
	isLoaded = 0;
	
	fseek(f, 0, SEEK_END);
	size_t fileSize = ftell(f);
	rewind(f);
	
	isLoaded = (int) fileSize;
	
	fread(buffer , sizeof(unsigned char), (size_t) fileSize, f);
	fclose(f);

	return isLoaded;
}

//http://stackoverflow.com/questions/236129/split-a-string-in-c
//----------------------------------------------------------------------------------
std::vector<std::string>& UtilityFunctions::Split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> UtilityFunctions::Split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	UtilityFunctions::Split(s, delim, elems);
	return elems;
}
//----------------------------------------------------------------------------------