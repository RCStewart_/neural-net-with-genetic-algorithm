#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#include <string>
#include <map>
#include <vector>

class Vec2;

extern bool g_isQuitting;
extern HDC g_displayDeviceContext;
extern HWND g_hWnd;
extern HGLRC g_openGLRenderingContext;
extern const char* APP_NAME;

//std::vector<bool> WindowWrapperActiveInputs;

class WindowsWrapper
{
public:
	WindowsWrapper(void);
	~WindowsWrapper(void);

	//TODO switch to map
	//static std::vector<bool> WindowWrapperActiveInputs;
};

std::wstring stringToWString(const std::string& s);
void WindowsWrapperMessageBox( std::string& message, std::string& caption );
void WindowsWrapperFindMemoryLeaks();
void WindowWrapperSetProcessDPIAware();
void WindowWrapperCreateOpenGLWindow( HINSTANCE applicationInstanceHandle );
void WindowWrapperCreateOpenGLOrthoWindow( HINSTANCE applicationInstanceHandle );
void WindowWrapperCreateWindow( HINSTANCE applicationInstanceHandle );
void WindowWrapperRunMessagePump();
LRESULT CALLBACK WindowWrapperMessageHandlingProcedure( HWND windowHandle, UINT wmMessageCode, WPARAM wParam, LPARAM lParam );

double WindowWrapperGetCurrentTimeSeconds();

void WindowWrapperDebuggerPrintf( const char* messageFormat, ... );

Vec2 WindowWrapperGetMouseMovementSinceLastChecked();

void WindowWrapperSetCursorVisibility(bool isEnabled);

extern enum WINDOW_WRAPPER_INPUTS
{
	WINDOW_WRAPPER_INPUT_DELETE = VK_DELETE,
	WINDOW_WRAPPER_INPUT_HOME = VK_HOME,
	WINDOW_WRAPPER_INPUT_END = VK_END,
	WINDOW_WRAPPER_INPUT_LEFT = VK_LEFT,
	WINDOW_WRAPPER_INPUT_RIGHT = VK_RIGHT,
	WINDOW_WRAPPER_INPUT_UP = VK_UP,
	WINDOW_WRAPPER_INPUT_DOWN = VK_DOWN,
	WINDOW_WRAPPER_INPUT_SPACE = VK_SPACE,
	WINDOW_WRAPPER_INPUT_ENTER = VK_RETURN,
	WINDOW_WRAPPER_INPUT_ESCAPE = VK_ESCAPE,
	WINDOW_WRAPPER_INPUT_BACKSPACE = VK_BACK,
	WINDOW_WRAPPER_INPUT_LEFT_MOUSE_CLICK = VK_LBUTTON,
	WINDOW_WRAPPER_INPUT_RIGHT_MOUSE_CLICK = VK_RBUTTON,
	WINDOW_WRAPPER_INPUT_MOUSE_WHEEL_UP,
	WINDOW_WRAPPER_INPUT_MOUSE_WHEEL_DOWN,
	WINDOW_WRAPPER_INPUT_SHIFT = VK_SHIFT,
	WINDOW_WRAPPER_INPUT_MAX
};

int WindowWrapperGetScrollWheelInput();

void WindowWrapperInitialization();
void WindowWrapperResetInputArray();

bool WindowWrapperIsInputActive(/*WINDOW_WRAPPER_INPUTS*/char keyToCheck);
bool WindowWrapperGetIsInputActiveAndConsume( char keyToCheck );

//bool WindowWrapperIsASinglePressKey();
void WindowWrapperResetKeyInputs();

Vec2 WindowWrapperGetMousePosition();

