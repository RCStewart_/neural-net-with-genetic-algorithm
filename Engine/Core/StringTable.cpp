#include "StringTable.hpp"
#include <map>
#include <algorithm>

struct StringTableEntry
{
	int id;
	std::string originalString;
};

typedef std::map< std::string, StringTableEntry > StringTable;
static StringTable	s_theStringTable;
static int			s_nextStringID = 1;


//----------------------------------------------------------------------------------
int StringID( const std::string& stringValue )
{
	std::string stringToLower = stringValue;
	//std::transform(stringToLower.begin(), stringToLower.end(), stringToLower.begin(), ::tolower);
	StringTable::iterator it = s_theStringTable.find( stringValue );
	if( it == s_theStringTable.end() )
	{
		StringTableEntry newEntry;
		newEntry.originalString = stringValue;
		newEntry.id = s_nextStringID;
		++s_nextStringID;
		s_theStringTable[stringToLower] = newEntry;	
	}
	return s_theStringTable[stringToLower].id;
}


//----------------------------------------------------------------------------------
const char* StringValue(int stringID )
{
	for( StringTable::iterator it = s_theStringTable.begin(); it != s_theStringTable.end(); ++it)
	{
		if( it->second.id == stringID )
		{
			return it->second.originalString.c_str();
		}
	}
	return nullptr;
}