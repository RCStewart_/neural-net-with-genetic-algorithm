#include "Noun.hpp"
#include "Engine/Core/StringTable.hpp"
#include "Engine/Core/AnyDataType.hpp"
#include "Engine/Libraries/TinyXML/tinystr.h"
#include "Engine/Libraries/TinyXML/tinyxml.h"
#include "Verb.hpp"
#include "Engine/Core/WindowsWrapper.hpp"
#include "Engine/2D/Components/Component.hpp"

std::map<int, Noun*> Noun::s_mapOfNouns;
std::map<int, Noun*> Noun::s_mapOfNounFactories;

//----------------------------------------------------------------------------------
Noun::Noun(Noun* toCopy)
	:GameEntity2D()
{
	for (int i = 0; i < toCopy->m_components.size(); ++i)
	{
		m_components.push_back(toCopy->m_components[i]->getClone());
		if (toCopy->m_components[i]->m_name != "Core2D")
		{

		}
		else
		{
			m_coreComponent = static_cast<Core2D*>(toCopy->m_components[i]->getClone());
		}
	}
}

//----------------------------------------------------------------------------------
void Noun::finishInitalizationFromFactory()
{
	for (int i = 0; i < m_components.size(); ++i)
	{
		m_components[i]->placeAttributesIntoGameEntity(this);
	}
	for (int i = 0; i < m_components.size(); ++i)
	{
		m_components[i]->init();
	}

	m_conditionAsIntToTriggersMap[StringID("ON_UPDATE")] = new Trigger("ON_UPDATE");

	s_mapOfNouns[m_ID] = this;
}

//----------------------------------------------------------------------------------
Noun::Noun(std::vector<Component*>& components, AnyDataTypeMap& memberAttributes)
	:GameEntity2D(components)
{
	m_coreComponent = nullptr;
	for (int i = 0; i < components.size(); ++i)
	{
		if (components[i]->m_name == "Core2D")
		{
			if (m_coreComponent)
			{
				delete m_coreComponent;
				m_coreComponent = nullptr;
			}
			m_coreComponent = static_cast<Core2D*>(components[i]->getClone());
		}

		components[i]->placeAttributesIntoGameEntity(this);
	}
	memberAttributes.fillTargetMapWithDataFromSelf(m_attributes);
	m_conditionAsIntToTriggersMap[StringID("ON_UPDATE")] = new Trigger("ON_UPDATE");

	s_mapOfNouns[m_ID] = this;

	if (!m_coreComponent)
	{
		m_coreComponent = new Core2D();
		m_coreComponent->placeAttributesIntoGameEntity(this);
	}
}

//----------------------------------------------------------------------------------
Noun::~Noun()
{
	s_mapOfNouns.erase(m_ID);
}

//----------------------------------------------------------------------------------
void Noun::update(float delta)
{
	Trigger* updateVerbsRef = m_conditionAsIntToTriggersMap[StringID("ON_UPDATE")];
	std::vector<GameEntity2D*> testData;
	updateVerbsRef->activateTrigger(static_cast<GameEntity2D*>(this), testData);
	for (int i = 0; i < m_components.size(); ++i)
	{
		m_components[i]->update(delta);
	}
	
	onEvent();
}

//----------------------------------------------------------------------------------
void Noun::registerVerb(const std::string& conditionToTriggerVerb, const std::string& nameOfVerb, AnyDataTypeMap& verbAttributes)
{
	m_conditionAsIntToTriggersMap[StringID(conditionToTriggerVerb)]->addVerb(Verb::s_generatedVerbs[nameOfVerb]->getClone(verbAttributes), verbAttributes);
}

//----------------------------------------------------------------------------------
void Noun::onEvent()
{

}

//----------------------------------------------------------------------------------
int Noun::sGenerateNounsFromFiles(std::vector<std::string>& files)
{
	for (size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex)
	{
		TiXmlDocument doc;

		if (!doc.LoadFile(files[fileIndex].c_str()))
		{
			return 0;
		}

		TiXmlElement* root = doc.FirstChildElement();

		if (root == NULL)
		{
			doc.Clear();
			return 0;
		}

		TiXmlElement* noun = root;
		std::string name = noun->Attribute("name");

		AnyDataTypeMap toAdd;

		toAdd.set<std::string>("NAME", name);

		std::vector<Component*> componentsToAdd;
		TiXmlElement* components = noun->FirstChildElement("Components");
		if (components)
		{
			for (TiXmlElement* componentReference = components->FirstChildElement(); componentReference != NULL; componentReference = componentReference->NextSiblingElement())
			{
				std::string componentName = componentReference->Attribute("name");
				componentsToAdd.push_back(Component::s_generatedComponents[componentName]->createComponent(name, componentReference));
			}
		}

		Noun* theNoun = new Noun(componentsToAdd, toAdd);

		TiXmlElement* triggers = noun->FirstChildElement("Triggers");
		if (triggers)
		{
			for (TiXmlElement* triggerReference = triggers->FirstChildElement(); triggerReference != NULL; triggerReference = triggerReference->NextSiblingElement())
			{
				std::string triggerName = triggerReference->Attribute("name");
				for (TiXmlElement* verbToAdd = triggerReference->FirstChildElement(); verbToAdd != NULL; verbToAdd = verbToAdd->NextSiblingElement())
				{
					AnyDataTypeMap verbAttributes;
					Verb* baseVerb;
					std::string nameOfBehavior = verbToAdd->Attribute("name");

					baseVerb = Verb::s_generatedVerbs[nameOfBehavior]->getClone();
					theNoun->m_conditionAsIntToTriggersMap[StringID(triggerName)]->addVerb(baseVerb, verbAttributes);
				}
			}
		}

		s_mapOfNounFactories[StringID(name)] = theNoun;
	}
	return 0;
}

//----------------------------------------------------------------------------------
void Noun::sUpdateNouns(float delta)
{
	std::map<int, Noun*>::iterator it;
	for (it = s_mapOfNouns.begin(); it != s_mapOfNouns.end(); it++)
	{
		it->second->update(delta);
	}
}

//----------------------------------------------------------------------------------
void Noun::addComponent(Component* component)
{
	m_components.push_back(component);
	component->placeAttributesIntoGameEntity(this);

	if (component->m_name == "Core2D")
	{
		if (m_coreComponent)
		{
			delete m_coreComponent;
			m_coreComponent = nullptr;
		}

		m_coreComponent = static_cast<Core2D*>(component);
	}
}