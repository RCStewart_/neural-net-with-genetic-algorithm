#pragma once
#include <functional>
#include "Engine/Core/AnyDataType.hpp"
#include "Engine/Core/AnyDataTypeMap.hpp"
#include "Engine/2D/GameEntity2D.hpp"

#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"

#include <vector>
#include <string>

class VerbRegistration;
class Verb;

typedef Verb* (RegistrationFunc)(const std::string& name);
typedef std::map<std::string, VerbRegistration*> VerbRegistrationMap;
typedef std::map<std::string, Verb*> VerbMap;

//----------------------------------------------------------------------------------
class VerbRegistration
{
public:
	VerbRegistration(const std::string& name, RegistrationFunc* registrationFunc)
		:m_name(name)
		, m_registrationFunc(registrationFunc)
	{
		if (!s_verbRegistration)
		{
			s_verbRegistration = new VerbRegistrationMap();
		}
		(*s_verbRegistration)[name] = this;
	};

	static VerbRegistrationMap* getVerbRegistrations()
	{
		return s_verbRegistration;
	};

	std::string getName()
	{
		return m_name;
	};

	Verb* createVerb();

protected:
	std::string m_name;
	RegistrationFunc* m_registrationFunc;
	static VerbRegistrationMap* s_verbRegistration;
};

//----------------------------------------------------------------------------------
class Verb
{
public:
	Verb();
	virtual ~Verb(){};

	virtual void doThis(GameEntity2D* caster, std::vector<GameEntity2D*>& everyActor){};
	virtual Verb* getClone(AnyDataTypeMap& withTheseAttributeModifications){ return nullptr; };
	virtual Verb* getClone(){ return nullptr; };
	virtual void setBaseAttributes(){};
	virtual void fillAnyDataMapWithAttributesFromXMLElement(TiXmlElement* verb, AnyDataTypeMap& toFill);

	void setAttributes(AnyDataTypeMap& attributesToSet);

	static int createVerbs();

	AnyDataTypeMap m_attributes;

	static VerbMap s_generatedVerbs;
protected:
	Verb* clone(AnyDataTypeMap& withTheseAttributeModifications){ return getClone(withTheseAttributeModifications); };
	Verb* clone(){ return getClone(); };
	Verb(AnyDataTypeMap& startingAttributes);
};