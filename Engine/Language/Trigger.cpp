#include "Trigger.hpp"
#include "Engine/Core/StringTable.hpp"

//----------------------------------------------------------------------------------
Trigger::Trigger(const std::string& nameOfTrigger)
{
	m_name = nameOfTrigger;
	m_id = StringID(nameOfTrigger);
}

//----------------------------------------------------------------------------------
void Trigger::addVerb(Verb* verbToAdd, AnyDataTypeMap& verbAttributes)
{
	//verbToAdd->fillAnyDataMapWithAttributesFromXMLElement(verbToAdd, verbAttributes);
	verbToAdd->setAttributes(verbAttributes);
	m_verbsToCallOnTrigger.push_back(verbToAdd);
}

//----------------------------------------------------------------------------------
void Trigger::activateTrigger(GameEntity2D* caster, std::vector<GameEntity2D*>& everyActor)
{
	for (int verbIndex = 0; verbIndex < m_verbsToCallOnTrigger.size(); ++verbIndex)
	{
		m_verbsToCallOnTrigger[verbIndex]->doThis(caster, everyActor);
	}
}