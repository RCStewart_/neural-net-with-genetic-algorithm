#include "Job.hpp"

int g_nextJobID = 1;
std::mutex g_lockJobID;

//----------------------------------------------------------------------------------
Job::Job()
	: m_priority(50)
{
	{
		std::lock_guard<std::mutex> guard(g_lockJobID);
		m_jobID = g_nextJobID;
		++g_nextJobID;
	}
}

//----------------------------------------------------------------------------------
Job::Job(int priority, JobArgs* argsToProcess)
	: m_priority(priority)
	, m_argsToProcess(argsToProcess)
{
	{
		std::lock_guard<std::mutex> guard(g_lockJobID);
		m_jobID = g_nextJobID;
		++g_nextJobID;
	}
}

//----------------------------------------------------------------------------------
Job::~Job()
{

}

//----------------------------------------------------------------------------------
void Job::execute()
{
	
}

//----------------------------------------------------------------------------------
void Job::callBack()
{

}
