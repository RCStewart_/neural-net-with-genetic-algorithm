#pragma once

#include <mutex>
#include <thread>
#include <vector>
#include <list>
#include <condition_variable>
//#include <atomic>

class Job;

class JobManager
{
public:
	JobManager(int numberOfThreads);
	~JobManager();

	void addJob(Job* jobToAdd);
	void addJobToCompletedList( Job* jobToAdd );
	void update();

	void addThread();
	void removeThread();

private:
	void loop();
	
	Job* getNextJob();
	std::vector<std::thread> m_workers;
	std::vector<Job*> m_jobs;
	std::vector<Job*> m_completedJobs;
	std::mutex m_lockJobsList;
	std::mutex m_lockCompletedList;
	std::mutex m_lockWorkerList;
	std::condition_variable m_notifyJob;

	std::mutex m_lockRemoveThread;
	std::vector<std::thread::id> m_removeThreadByID;
	bool m_isRunning;
};

extern JobManager* g_theJobManager;