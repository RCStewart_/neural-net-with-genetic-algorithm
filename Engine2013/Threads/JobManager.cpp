#include "JobManager.hpp"
#include "Job.hpp"

JobManager* g_theJobManager = nullptr;

//----------------------------------------------------------------------------------
JobManager::JobManager(int numberOfThreads) 
	: m_isRunning(true)
{
	{
		std::lock_guard<std::mutex> guard(m_lockWorkerList);
		for (int workerIndex = 0; workerIndex < numberOfThreads; ++workerIndex)
		{
			m_workers.push_back(std::thread(&JobManager::loop, this));
		}
	}
}

//----------------------------------------------------------------------------------
JobManager::~JobManager()
{
	{
		std::unique_lock<std::mutex> lockList(m_lockJobsList);
		m_isRunning = false;
		m_notifyJob.notify_all();
	}

	for (int threadIndex = 0; threadIndex < (int) m_workers.size(); ++threadIndex)
	{
		m_workers[threadIndex].join();
	}

}

//----------------------------------------------------------------------------------
void JobManager::update()
{

	if (m_workers.size() == 0)
	{
		Job* job = getNextJob();
		if (job)
		{
			job->execute();
			addJobToCompletedList(job);
		}
	}

	{
		std::lock_guard<std::mutex> guard(m_lockCompletedList);
		for (int i = 0; i < (int)m_completedJobs.size(); ++i)
		{
			m_completedJobs[i]->callBack();
		}
		for (int i = 0; i < (int)m_completedJobs.size(); ++i)
		{
			delete m_completedJobs[i];
		}
		m_completedJobs.clear();
	}
}

//----------------------------------------------------------------------------------
void JobManager::addJob( Job* toAdd )
{
	{
		std::lock_guard<std::mutex> guard(m_lockJobsList);
		m_jobs.push_back(toAdd);
	}
}


//----------------------------------------------------------------------------------
void JobManager::addJobToCompletedList(Job* toAdd)
{
	{
		std::lock_guard<std::mutex> guard(m_lockCompletedList);
		m_completedJobs.push_back(toAdd);
	}
}

//----------------------------------------------------------------------------------
Job* JobManager::getNextJob()
{
	{
		{
			m_lockJobsList.lock();
			if (!m_jobs.empty())
			{
				int highestPriority = -1;
				int indexOfHighest = 0;
				for (int i = 0; i < (int) m_jobs.size(); ++i)
				{
					if (m_jobs[i]->m_priority > highestPriority)
					{
						highestPriority = m_jobs[i]->m_priority;
						indexOfHighest = i;
					}
				}
				Job* job = m_jobs[indexOfHighest];
				m_jobs.erase(m_jobs.begin() + indexOfHighest);
				m_lockJobsList.unlock();
				return job;
			}
			m_lockJobsList.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			return nullptr;
		}
	}

}

//----------------------------------------------------------------------------------
void JobManager::loop()
{
	while (m_isRunning)
	{
		Job* job = getNextJob();
		if (job)
		{
			job->execute();
			addJobToCompletedList(job);
		}

		m_lockRemoveThread.lock();
		for (int i = 0; i < (int) m_removeThreadByID.size(); ++i)
		{
			if (std::this_thread::get_id() == m_removeThreadByID[i])
			{
				m_lockRemoveThread.unlock();
				return;
			}
		}
		m_lockRemoveThread.unlock();
	}
}

//----------------------------------------------------------------------------------
void JobManager::addThread()
{
	m_lockWorkerList.lock();
	m_workers.push_back(std::thread(&JobManager::loop, this));
	printf("%d threads active \n", m_workers.size());
	m_lockWorkerList.unlock();
}

//----------------------------------------------------------------------------------
void JobManager::removeThread()
{
	m_lockWorkerList.lock();
	if (m_workers.size() > 0)
	{
		m_lockRemoveThread.lock();
		m_removeThreadByID.push_back(m_workers[m_workers.size() - 1].get_id());
		m_lockRemoveThread.unlock();

		m_workers[m_workers.size() - 1].join();
		m_workers.pop_back();
	}
	printf("%d threads active \n", m_workers.size());
	m_lockWorkerList.unlock();
}