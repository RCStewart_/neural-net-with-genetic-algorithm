#pragma once
#include <mutex>

class JobArgs
{
public:
	JobArgs()
	{};

	virtual ~JobArgs()
	{};
};

class Job
{
public:
	Job();
	Job(int priority, JobArgs* argsToProcess);
	virtual ~Job();
	virtual void execute();
	virtual void callBack();

	JobArgs* m_argsToProcess;
	int m_priority;
	int m_jobID;
};

extern int g_nextJobID;
extern std::mutex g_lockJobID;