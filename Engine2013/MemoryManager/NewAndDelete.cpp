#include "NewAndDelete.hpp"
#include <stdlib.h>
#include <exception>

#include "Engine/Core/WindowsWrapper.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine2013/MemoryManager/UndefineNew.hpp"


unsigned char* g_heap = nullptr;
MemoryManagerNode* g_rootNode = nullptr;
unsigned char* g_currentIndex = nullptr;

bool g_isProgramRunning = true;

int g_totalAllocations = 0;
int g_totalBytesAllocated = 0;
int g_largestBytesAllocated = 0;
int g_bytesAllocated = 0;
int g_totalDeletes = 0;

//----------------------------------------------------------------------------------
void AddToStatistics(int size, int numOfNodes)
{
	if (size > g_largestBytesAllocated)
	g_largestBytesAllocated = size;

	g_totalBytesAllocated += size;
	g_totalAllocations += numOfNodes;
}

//----------------------------------------------------------------------------------
void* OccupyNextAvailableValidSpace(const size_t sizeOfMemoryToAllocate, const char* file, int line)
{
	void* toReturn = nullptr;
	MemoryManagerNode* currentNode = g_rootNode;

	while (currentNode)
	{
		if (currentNode->m_isOccupied)
		{
			currentNode = currentNode->m_next;
		}
		else if ( currentNode->m_size < (int) sizeOfMemoryToAllocate)
		{
			currentNode = currentNode->m_next;
		}
		else if ( currentNode->m_size > (int) (sizeOfMemoryToAllocate + sizeof(MemoryManagerNode)))
		{
			//if there is no next node, start from tail buffer
			if (!currentNode->m_next)
			{
				toReturn = (void*)(currentNode->m_currentIndex);
				currentNode->m_isOccupied = true;
				g_currentIndex += sizeOfMemoryToAllocate;
				int sizeLeft = currentNode->m_size - sizeOfMemoryToAllocate;
				currentNode->m_size = sizeOfMemoryToAllocate;

				AddToStatistics(sizeOfMemoryToAllocate, 1);
				if (sizeLeft - sizeof(MemoryManagerNode) > 0)
				{
					//Unroll newing off the memory manager node
					//----------------------------------------------------------------------------------
					AddToStatistics(sizeof(MemoryManagerNode), 0);
					MemoryManagerNode* newNode = (MemoryManagerNode*)g_currentIndex;
					newNode->m_currentIndex = g_currentIndex + sizeof(MemoryManagerNode);
					g_currentIndex += sizeof(MemoryManagerNode);
					newNode->m_before = currentNode;
					newNode->m_next = nullptr;
					newNode->m_isOccupied = false;
					newNode->m_size = sizeLeft - sizeof(MemoryManagerNode);
					newNode->m_fileName = /*(char*)*/ file;
					newNode->m_lineNumber = line;

					if (currentNode->m_next)
					{
						newNode->m_next = currentNode->m_next;
						currentNode->m_next->m_before = newNode;
					}

					currentNode->m_next = newNode;
				}
			}
			//If there is a next node
			else
			{
				//make new node
				toReturn = (void*)(currentNode->m_currentIndex);
				currentNode->m_isOccupied = true;
				unsigned char* memoryIndex = (unsigned char*)toReturn;
				int sizeLeft = currentNode->m_size - sizeOfMemoryToAllocate;
				currentNode->m_size = sizeOfMemoryToAllocate;
				AddToStatistics(sizeOfMemoryToAllocate, 1);
				if (sizeLeft - sizeof(MemoryManagerNode) > 0)
				{
					AddToStatistics(sizeof(MemoryManagerNode), 0);
					//Unroll newing off the memory manager node
					//----------------------------------------------------------------------------------
					memoryIndex += sizeOfMemoryToAllocate;
					MemoryManagerNode* newNode = (MemoryManagerNode*)memoryIndex;

					newNode->m_currentIndex = memoryIndex + sizeof(MemoryManagerNode);
					memoryIndex += sizeof(MemoryManagerNode);
					newNode->m_before = currentNode;
					newNode->m_next = nullptr;
					newNode->m_isOccupied = false;
					newNode->m_size = sizeLeft - sizeof(MemoryManagerNode);
					newNode->m_fileName = /*(char*)*/file;
					newNode->m_lineNumber = line;

					if (currentNode->m_next)
					{
						newNode->m_next = currentNode->m_next;
						currentNode->m_next->m_before = newNode;
					}
					currentNode->m_next = newNode;
				}
			}
			return toReturn;
		}
		else
		{
			currentNode = currentNode->m_next;
		}

	}
	return toReturn;
}

//----------------------------------------------------------------------------------
void CheckForHeapAndMallocIfNull()
{
	int bytesToAllocate = 1024 * 1024 * 1024;

	while (!g_heap)
	{
		g_heap = (unsigned char*)malloc(bytesToAllocate);
		g_currentIndex = g_heap + 0;

		if (!g_heap)
		{
			bytesToAllocate -= 1024;
		}
	}
	if (!g_rootNode)
	{
		g_bytesAllocated = bytesToAllocate;

		AddToStatistics(sizeof(MemoryManagerNode), 1);

		//g_rootNode = new MemoryManagerNode(1073741824);
		g_rootNode = (MemoryManagerNode*)g_currentIndex + 0;
		g_rootNode->m_currentIndex = g_currentIndex + sizeof(MemoryManagerNode);
		g_currentIndex += sizeof(MemoryManagerNode);
		g_rootNode->m_before = nullptr;
		g_rootNode->m_next = nullptr;
		g_rootNode->m_isOccupied = false;
		g_rootNode->m_size = bytesToAllocate - sizeof(MemoryManagerNode);
		g_rootNode->m_fileName = __FILE__; 
		g_rootNode->m_lineNumber = __LINE__;
	}
}

//----------------------------------------------------------------------------------
void FreeMemoryPool()
{
	free(g_heap);
}

//----------------------------------------------------------------------------------
void Deallocate(void* ptr)
{
	++g_totalDeletes;
	if (g_isProgramRunning)
	{
		MemoryManagerNode* currentNode = g_rootNode;
		while (currentNode)
		{
			if (currentNode->m_currentIndex == ptr)
			{
				//----------------------------------------------------------------------------------
				if (currentNode->m_before)
				{
					if (!currentNode->m_before->m_isOccupied)
					{
						currentNode->m_before->m_size += currentNode->m_size + sizeof(MemoryManagerNode);

						currentNode->m_before->m_next = currentNode->m_next;

						if (currentNode->m_next)
						{
							currentNode->m_next->m_before = currentNode->m_before;
						}
					}
				}
				//----------------------------------------------------------------------------------
				if (currentNode->m_next)
				{
					if (!currentNode->m_next->m_isOccupied)
					{
						if (currentNode->m_before && !currentNode->m_before->m_isOccupied)
						{
							currentNode->m_before->m_size += currentNode->m_next->m_size + sizeof(MemoryManagerNode);
							currentNode->m_before->m_next = currentNode->m_next->m_next;

							if (currentNode->m_next->m_next)
							{
								currentNode->m_next->m_next->m_before = currentNode->m_before;
							}
						}
						else
						{
							currentNode->m_size += currentNode->m_next->m_size + sizeof(MemoryManagerNode);
							currentNode->m_next = currentNode->m_next->m_next;

							if (currentNode->m_next)
							{
								currentNode->m_next->m_before = currentNode;
							}
						}
					}
				}
				//else
				{
					currentNode->m_isOccupied = false;
				}
				break;
			}
			else
			{
				currentNode = currentNode->m_next;
			}
		}
	}
}


//----------------------------------------------------------------------------------
void* operator new(const size_t size, const char* file, int line) throw()
{
	CheckForHeapAndMallocIfNull();
	//----------------------------------------------------------------------------------
	void* toReturn = OccupyNextAvailableValidSpace(size, file, line);

	//----------------------------------------------------------------------------------
	try
	{
		if (!toReturn)
		{
			throw std::bad_alloc();
		}
	}
	catch (std::exception& e)
	{
		UNUSED(e);
	}


	return toReturn;
}


//----------------------------------------------------------------------------------
void* operator new[](const size_t size, const char* file, int line) throw()
{	
	CheckForHeapAndMallocIfNull();
	void* toReturn = OccupyNextAvailableValidSpace(size, file, line);

	try
	{
		if (!toReturn)
		{
			throw std::bad_alloc();
		}
	}
	catch (std::exception& e)
	{
		UNUSED(e);
	}


	return toReturn;
}


//----------------------------------------------------------------------------------
void operator delete(void* ptr) throw()
{
	Deallocate(ptr);
}

//----------------------------------------------------------------------------------
void operator delete[](void* ptr) throw()
{
	Deallocate(ptr);
}

//----------------------------------------------------------------------------------
void operator delete(void* ptr, const char* file, int line) throw()
{
	Deallocate(ptr);
	UNUSED(file);
	UNUSED(line);
}

//----------------------------------------------------------------------------------
void operator delete[](void* ptr, const char* file, int line) throw()
{
	Deallocate(ptr);
	UNUSED(file);
	UNUSED(line);
}


//----------------------------------------------------------------------------------
void* operator new(size_t size) throw()
{
	CheckForHeapAndMallocIfNull();
	void* toReturn = OccupyNextAvailableValidSpace(size, "Unknown File", -1);

	try
	{
		if (!toReturn)
		{
			throw std::bad_alloc();
		}
	}
	catch (std::exception& e)
	{
		UNUSED(e);
	}

	return toReturn;
}

//----------------------------------------------------------------------------------
void* operator new(size_t size, const std::nothrow_t&) throw()
{
	CheckForHeapAndMallocIfNull();
	void* toReturn = OccupyNextAvailableValidSpace(size, "Unknown File", -1);

	return toReturn;
};


//----------------------------------------------------------------------------------
void* operator new[](size_t size) throw()
{
	CheckForHeapAndMallocIfNull();
	void* toReturn = OccupyNextAvailableValidSpace(size, "Unknown File", -1);

	try
	{
		if (!toReturn)
		{
			throw std::bad_alloc();
		}
	}
	catch (std::exception& e)
	{
		UNUSED(e);
	}


	return toReturn;
}

//----------------------------------------------------------------------------------
void* operator new[](size_t size, const std::nothrow_t&) throw()
{
	CheckForHeapAndMallocIfNull();
	void* toReturn = OccupyNextAvailableValidSpace(size, "Unknown File", -1);

	return toReturn;
}

//----------------------------------------------------------------------------------
void operator delete(void* ptr, const std::nothrow_t&) throw()
{
	Deallocate(ptr);
}

//----------------------------------------------------------------------------------
void operator delete[](void* ptr, const std::nothrow_t&) throw()
{
	Deallocate(ptr);
}


//----------------------------------------------------------------------------------
void ReportMemoryLeaks()
{
	MemoryManagerNode* currentNode = g_rootNode;
	
	while (currentNode)
	{
		if (currentNode->m_isOccupied)
		{
			WindowWrapperDebuggerPrintf("%s(%d): %d bytes allocated \n", currentNode->m_fileName, currentNode->m_lineNumber, currentNode->m_size);
		}
		
		currentNode = currentNode->m_next;
	}

}

//----------------------------------------------------------------------------------
void CountOccupiedNodes(unsigned int& numOfOccupiedNodes, unsigned int& numOfBytes, unsigned int& currentLargest, unsigned int& freeSpace)
{
	freeSpace = 0;
	numOfOccupiedNodes = 0;
	numOfBytes = 0;
	currentLargest = 0;

	MemoryManagerNode* currentNode = g_rootNode;

	while (currentNode)
	{
		if (currentNode->m_isOccupied)
		{
			numOfBytes += currentNode->m_size;
			++numOfOccupiedNodes;

			if (currentNode->m_size > (int) currentLargest)
			{
				currentLargest = currentNode->m_size;
			}
		}
		else
		{
			freeSpace += currentNode->m_size;
		}

		currentNode = currentNode->m_next;
	}
}

//----------------------------------------------------------------------------------
void DebugPrintMemoryManagementStatistics()
{
	WindowWrapperDebuggerPrintf("Memory Management Statistics: \n");
	WindowWrapperDebuggerPrintf("	Total Bytes Allocated:			%d\n", g_totalBytesAllocated);
	WindowWrapperDebuggerPrintf("	Largest Allocation:				%d\n", g_largestBytesAllocated);
	WindowWrapperDebuggerPrintf("	Total Allocations:				%d\n", g_totalAllocations);
	WindowWrapperDebuggerPrintf("	Total Deletions:				%d\n", g_totalDeletes);
	WindowWrapperDebuggerPrintf("	Average Bytes Allocated:		%f\n", ((float)g_totalBytesAllocated) / ((float)g_totalAllocations));

	unsigned int numOfOccupied = 0;
	unsigned int numOfBytes = 0;
	unsigned int currentLargest = 0;
	unsigned int freeSpace = 0;
	CountOccupiedNodes(numOfOccupied, numOfBytes, currentLargest, freeSpace);

	WindowWrapperDebuggerPrintf("	Currently Occupied:				%u\n", numOfOccupied);
	WindowWrapperDebuggerPrintf("	Number Of Bytes in use:			%u\n", numOfBytes);
	WindowWrapperDebuggerPrintf("	Percent of Memory Free:			%f\n\n", (float)(1.f - ((float) g_bytesAllocated - freeSpace)/(float) g_bytesAllocated) * 100.f);
}

#include "Engine2013/MemoryManager/DefineNew.hpp"