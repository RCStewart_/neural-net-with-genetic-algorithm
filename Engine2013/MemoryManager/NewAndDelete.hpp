#pragma once
#include <new>

class ProgramEnded;

//----------------------------------------------------------------------------------
struct MemoryManagerNode
{

	MemoryManagerNode* m_before;
	MemoryManagerNode* m_next;
	bool m_isOccupied;
	int m_size;
	int m_lineNumber;
	const char* m_fileName;
	unsigned char* m_currentIndex;

};

extern MemoryManagerNode* g_rootNode;
extern int g_bytesAllocated;

void ReportMemoryLeaks();

void* operator new(const size_t size, const char* file, int line) throw();
void* operator new[](const size_t size, const char* file, int line) throw();

void operator delete(void* ptr) throw();
void operator delete[](void* ptr) throw();

void operator delete(void* ptr, const char* file, int line) throw();
void operator delete[](void* ptr, const char* file, int line) throw();

void* operator new(size_t size) throw();
void* operator new(size_t size, const std::nothrow_t&) throw();

void* operator new[](size_t size) throw();
void* operator new[](size_t size, const std::nothrow_t&) throw();

void operator delete(void* ptr, const std::nothrow_t&) throw();
void operator delete[](void* ptr, const std::nothrow_t&) throw();

void FreeMemoryPool();
void CheckForHeapAndMallocIfNull();
void NullPointers();
void DebugPrintMemoryManagementStatistics();

class ProgramHasEnded
{
public:
	~ProgramHasEnded()
	{
		ReportMemoryLeaks();
	};
};

#include "Engine2013/MemoryManager/DefineNew.hpp"
