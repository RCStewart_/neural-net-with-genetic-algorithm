#include "Sensor.hpp"
#include <windows.h>
#include <gl/gl.h>
#include <math.h>
#include "Engine/Core/GameCommon.hpp"
#include "Ship.hpp"
#include "Asteroid.hpp"
#include "Engine/Core/UtilityFunctions.hpp"
#include <set>

//----------------------------------------------------------------------------------
Sensor::Sensor(float orientation, float radius, float distanceFromParent)
	: m_orientation(orientation)
	, m_distanceFromParent(distanceFromParent)
	, m_radius(radius)
	, m_isColliding(0)
{

}

//----------------------------------------------------------------------------------
Sensor::~Sensor()
{

}

//----------------------------------------------------------------------------------
void Sensor::Render(const Ship* parentShip)
{
	//Move the entity to the proper position
	glPushMatrix();
	glTranslatef(parentShip->m_position.x, parentShip->m_position.y, 0.f);

	//Set the new orientation
	glPushMatrix();
	glRotatef(parentShip->m_orientationDegrees, 0.f, 0.f, 1.f);

	glPushMatrix();
	glRotatef(m_orientation, 0.f, 0.f, 1.f);

	glPushMatrix();
	glTranslatef(m_distanceFromParent, 0.f, 0.f);

	//Scale the entity to the proper size
	glPushMatrix();
	glScalef(parentShip->m_scale, parentShip->m_scale, parentShip->m_scale);

	if (m_isColliding > 0)
	{
		glColor3f(1.f, 0.f, 0.f);
	}
	if (m_isColliding < 0)
	{
		glColor3f(0.f, 1.f, 0.f);
	}
	if (m_isColliding == 0)
	{
		glColor3f(1.f, 1.f, 1.f);
	}

	glBegin(GL_LINE_LOOP);

	const float DEG2RAD = 3.14159 / 180;

	for (int i = 0; i < 360; i++)
	{
		float degInRad = i*DEG2RAD;
		glVertex2f(cos(degInRad)*m_radius, sin(degInRad)*m_radius);
	}

	glEnd();

	glColor3f(1.f, 1.f, 1.f);

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

//----------------------------------------------------------------------------------
void Sensor::updateCollisions(Ship* parentShip, const std::vector<Asteroid>& asteroids)
{
	m_isColliding = 0.f; 

	Vec2 position = Vec2(m_distanceFromParent, 0.f).rotateToDegrees(m_orientation + parentShip->m_orientationDegrees);
	position = Vec2::addVectors(position, parentShip->m_position);

	for (int i = 0; i < (int)asteroids.size(); ++i)
	{
		if (UtilityFunctions::detectCircleCollision(m_radius * parentShip->m_scale, position, asteroids[i].m_radius * asteroids[i].m_scale, asteroids[i].m_position))
		{
			m_isColliding = 1.f;
			parentShip->m_visitedAsteroidIDs.insert(asteroids[i].m_index);
		}
	}

	if (position.x < 0)
	{
		m_isColliding = 1.f;
	}
	if (position.x > g_numOfTileUnitsX)
	{
		m_isColliding = 1.f;
	}
	if (position.y < 0)
	{
		m_isColliding = 1.f;
	}
	if (position.y > g_numOfTileUnitsY)
	{
		m_isColliding = 1.f;
	}
}