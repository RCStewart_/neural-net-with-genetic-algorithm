#pragma once
#include <vector>

class Genome
{
public:
	Genome();
	Genome(int numOfRandomChromosomesToAdd, float min, float max, int generation);
	
	Genome(const std::vector<float> &startingDNA, int generation);
	~Genome();

	virtual void mutate() = 0;

	double m_fitness;
	int m_generation;

	std::vector<float> m_DNA;
};