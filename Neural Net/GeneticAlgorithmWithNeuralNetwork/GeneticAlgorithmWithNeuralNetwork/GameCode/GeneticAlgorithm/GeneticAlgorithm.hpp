#pragma once

#include "Engine/Math/RandomNumber.hpp"
#include "Genome.hpp"

class CrossedChildren
{
public:
	CrossedChildren()
	{
		child1 = nullptr;
		child2 = nullptr;
	}

	Genome* child1;
	Genome* child2;
};

class GeneticAlgorithm
{
public:
	GeneticAlgorithm(
		double crossOverRate,
		double mutationRate,
		int populationSize,
		int geneLength
		);

	virtual ~GeneticAlgorithm();

	virtual CrossedChildren* crossover(
		const Genome* mom,
		const Genome* dad
		);

	virtual void epoch() = 0;
	virtual Genome* selection() = 0;
	virtual void updateFitnessScores() = 0;
	virtual void createStartingPopulation() = 0;
	virtual void generateNewPopulation() = 0;

	std::vector<Genome*> m_genomes;
	std::vector<Genome*> m_bestGenomesPerGeneration;
	Genome* m_currentBestGenome;

	//double m_bestFitnessScore;
	double m_crossoverRate;
	double m_mutationRate;

	//size of a gene
	int m_geneLength;

	int m_fittestGenome;
	int m_populationSize;
	int m_generation;

	bool m_isBusy;
};
