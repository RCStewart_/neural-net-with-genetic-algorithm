#include "GeneticAlgorithmSorting.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include <algorithm>
#include "GeneticAlgorithmDefines.hpp"
#include "Engine/Math/IntVec2.hpp"
#include <algorithm>
#include "Engine/Renderer/DrawDebug.hpp"

//----------------------------------------------------------------------------------
GeneticAlgorithmSorting::GeneticAlgorithmSorting(double crossOverRate, double mutationRate, int populationSize, int geneLength)
	: GeneticAlgorithm(crossOverRate, mutationRate, populationSize, geneLength)
	, m_boltzmanTemperature(100.f)
{
	createStartingPopulation();
}

//----------------------------------------------------------------------------------
void GeneticAlgorithmSorting::createStartingPopulation()
{
	std::vector<float> numbersToSort;
	while (numbersToSort.size() < m_geneLength)
	{
		float valueToAdd = RandomNumber::getRandomIntInRange(IntVec2(0, 1000000));

		if (std::find(numbersToSort.begin(), numbersToSort.end(), valueToAdd) == numbersToSort.end())
		{
			numbersToSort.push_back(valueToAdd);
		}
	}

	//Change to random vector of the same numbers for actual test
	for (int currentGenomeToAddIndex = 0; (int) m_genomes.size() < m_populationSize; ++currentGenomeToAddIndex)
	{
		std::random_shuffle(numbersToSort.begin(), numbersToSort.end());
		m_genomes.push_back(new SortingGenome(numbersToSort, 0));
	}
}


//----------------------------------------------------------------------------------
Genome* GeneticAlgorithmSorting::selection()
{
	Genome* selected = nullptr;

	//roulette selection
	float whereToStop = RandomNumber::getRandomFloatZeroToOne();
	float accumulatedCount = 0;
	int selectedGenome = -1;
	for (int i = 0; i < m_populationSize; ++i)
	{
		accumulatedCount += m_genomes[i]->m_fitness;
		if (accumulatedCount > whereToStop)
		{
			selectedGenome = i;
			break;
		}
	}
	if (selectedGenome != -1)
	{
		selected = m_genomes[selectedGenome];
	}
	if (selected == nullptr)
	{
		selected = selection();
	}

	return selected;
}

//Total Population's score has to be == 1.0 for roulette selection
//----------------------------------------------------------------------------------
void GeneticAlgorithmSorting::updateFitnessScores()
{
	//----------------------------------------------------------------------------------
	//find highest and store
	//Reward next > current
	//Reward longer streaks
	//----------------------------------------------------------------------------------

	int highestFitness = 0;
	SortingGenome* fittestGenome = nullptr;

	for (int genomeIndex = 0; genomeIndex < (int) m_genomes.size(); ++genomeIndex)
	{
		int lengthOfLongestStreak = 0;
		int lengthOfCurrentStreak = 1;

		int count = 0;
		int negCount = 0;

		int numberOfStreaks = 0;

		for (int dnaIndex = 0; dnaIndex < (int)m_genomes[genomeIndex]->m_DNA.size() - 1; ++dnaIndex)
		{
			if (m_genomes[genomeIndex]->m_DNA[dnaIndex] <= m_genomes[genomeIndex]->m_DNA[dnaIndex + 1])
			{
				++lengthOfCurrentStreak;
				++count;
			}
			else
			{
				if (lengthOfCurrentStreak > lengthOfLongestStreak)
				{
					lengthOfLongestStreak = lengthOfCurrentStreak;
				}
				if (lengthOfCurrentStreak > 1)
				{
					numberOfStreaks += 1.f;
				}

				lengthOfCurrentStreak = 1;
				++negCount;
			}
		}
		if (lengthOfCurrentStreak > lengthOfLongestStreak)
		{
			lengthOfLongestStreak = lengthOfCurrentStreak;
		}
		int total = count + lengthOfLongestStreak;

		float fitness = lengthOfLongestStreak;
		m_genomes[genomeIndex]->m_fitness = fitness;
		dynamic_cast<SortingGenome*> (m_genomes[genomeIndex])->m_totalFitness = fitness;
		dynamic_cast<SortingGenome*> (m_genomes[genomeIndex])->m_longestStreak = lengthOfLongestStreak;

		if (fitness >= highestFitness)
		{
			highestFitness = fitness;
			fittestGenome = ( SortingGenome* ) m_genomes[genomeIndex];
		}
	}


	//Rescale fitness
	float totalFitness = 0;
	for (int genomeIndex = 0; genomeIndex < (int)m_genomes.size(); ++genomeIndex)
	{
		totalFitness += m_genomes[genomeIndex]->m_fitness;
	}

	float averageFitness = totalFitness / m_geneLength;

	m_boltzmanTemperature -= BOLTZMAN_DELTA;
	m_boltzmanTemperature = max(m_boltzmanTemperature, BOLTZMANN_MIN_TEMP);

	float divider = averageFitness/m_boltzmanTemperature;
	divider = 1 / divider;

	float boltDividend = 1 / m_boltzmanTemperature;

	for (int genomeIndex = 0; genomeIndex < (int)m_genomes.size(); ++genomeIndex)
	{
		m_genomes[genomeIndex]->m_fitness = (m_genomes[genomeIndex]->m_fitness * boltDividend) * divider;
	}

	float newTotal = 0;
	for (int i = 0; i < m_populationSize; ++i)
	{
		newTotal += m_genomes[i]->m_fitness;
	}
	for (int genomeIndex = 0; genomeIndex < (int)m_genomes.size(); ++genomeIndex)
	{
		m_genomes[genomeIndex]->m_fitness = m_genomes[genomeIndex]->m_fitness/newTotal;
	}



	if (fittestGenome)
	{
		fittestGenome = new SortingGenome(fittestGenome);
		m_bestGenomesPerGeneration.push_back(fittestGenome);
	}
	if (m_currentBestGenome)
	{
		if (dynamic_cast<SortingGenome*> (m_currentBestGenome)->m_totalFitness < dynamic_cast<SortingGenome*> (fittestGenome)->m_totalFitness)
		{
			m_currentBestGenome = fittestGenome;
		}
	}
	else
	{
		m_currentBestGenome = fittestGenome;
	}
}

//Generate 2 child genomes based on two parents
//Must create valid children for the problem
//----------------------------------------------------------------------------------
CrossedChildren* GeneticAlgorithmSorting::crossover(const Genome* mom, const Genome* dad/*, Genome* child1, Genome* child2*/)
{
	SortingGenome* child1 = new SortingGenome(mom->m_DNA, m_generation);
	SortingGenome* child2 = new SortingGenome(dad->m_DNA, m_generation);

	CrossedChildren* toReturn = new CrossedChildren();

	if (RandomNumber::getRandomFloatZeroToOne() > CROSSOVER_RATE)
	{
		toReturn->child1 = child1;
		toReturn->child2 = child2;
		return toReturn;
	}

	std::vector<int> indexes;
	std::vector<float> elements;

	int currentPosition = RandomNumber::getRandomIntInRange(IntVec2(0, mom->m_DNA.size() - 1));
	while (currentPosition < (int) mom->m_DNA.size())
	{
		indexes.push_back(currentPosition);
		elements.push_back(mom->m_DNA[currentPosition]);

		currentPosition += RandomNumber::getRandomIntInRange(IntVec2(1, mom->m_DNA.size() - currentPosition));
	}

	currentPosition = 0;

	//For every element in main parent
	for (int DNAIndex = 0; DNAIndex < (int)child2->m_DNA.size(); ++DNAIndex)
	{
		if (std::find(elements.begin(), elements.end(), child2->m_DNA[DNAIndex]) != elements.end())
		{
			child2->m_DNA[DNAIndex] = elements[currentPosition];
			++currentPosition;
		}
	}

	currentPosition = 0;

	elements.clear();
	for (int indexOfIndexes = 0; indexOfIndexes < (int) indexes.size(); ++indexOfIndexes)
	{
		elements.push_back(dad->m_DNA[indexes[indexOfIndexes]]);
	}

	for (int DNAIndex = 0; DNAIndex < (int)child1->m_DNA.size(); ++DNAIndex)
	{
		if (std::find(elements.begin(), elements.end(), child1->m_DNA[DNAIndex]) != elements.end())
		{
			child1->m_DNA[DNAIndex] = dad->m_DNA[indexes[currentPosition]];
			++currentPosition;
		}
	}

	toReturn->child1 = child1;
	toReturn->child2 = child2;

	return toReturn;
}


//Update to a new generation
//----------------------------------------------------------------------------------
void GeneticAlgorithmSorting::epoch()
{
	updateFitnessScores();
	++m_generation;
	generateNewPopulation();
}

//----------------------------------------------------------------------------------
void GeneticAlgorithmSorting::generateNewPopulation()
{
	//UPDATE FITNESS SCORES
	//Get Best and add to best

	std::vector<Genome*> m_childrenGenomes;

	if (m_currentBestGenome && HAS_ELITISM)
	{
		m_childrenGenomes.push_back(new SortingGenome(m_currentBestGenome->m_DNA, m_generation));
	}
	while ((int)m_childrenGenomes.size() < m_populationSize)
	{
		SortingGenome* parent1 = (SortingGenome*) selection();
		SortingGenome* parent2 = (SortingGenome*) selection();
		
		//SortingGenome* child1 = new SortingGenome(parent1->m_DNA, m_generation);//nullptr;
		//SortingGenome* child2 = new SortingGenome(parent2->m_DNA, m_generation);//nullptr;

		//crossover(parent1, parent2, child1, child2);

		CrossedChildren* children = crossover(parent1, parent2);

		SortingGenome* child1 = (SortingGenome*)children->child1;
		SortingGenome* child2 = (SortingGenome*)children->child2;

		if (child1)
		{
			child1->mutate();
		}

		if (child2)
		{
			child2->mutate();
		}

		m_childrenGenomes.push_back(child1);
		m_childrenGenomes.push_back(child2);
	}

	for (int i = 0; i < (int) m_genomes.size(); ++i)
	{
		delete m_genomes[i];
	}
	m_genomes.clear();
	m_genomes = m_childrenGenomes;
}