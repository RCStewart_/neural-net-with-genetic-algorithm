#include "NeuralNetGeneticAlgorithm.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include <algorithm>
#include "GeneticAlgorithmDefines.hpp"
#include "Engine/Math/IntVec2.hpp"
#include <algorithm>
#include "Engine/Renderer/DrawDebug.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Core/EngineCommon.hpp"

std::vector<Ship*> g_learningShips;

//----------------------------------------------------------------------------------
NeuralNetGeneticAlgorithm::NeuralNetGeneticAlgorithm(double crossOverRate, double mutationRate, int populationSize, int geneLength)
	: GeneticAlgorithm(crossOverRate, mutationRate, populationSize, geneLength)
	, m_currentBestShip(nullptr)
	, m_saveFile("Data.csv")
	, m_boltzmanTemperature(100.f)
{
	createStartingPopulation();
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::generatePopulationBasedOnShip(const Ship* toCopy)
{
	for (int i = 0; i < (int)g_learningShips.size(); ++i)
	{
		delete g_learningShips[i];
	}

	g_learningShips.clear();


	m_generation = 0;
	for (int i = 0; i < m_populationSize; ++i)
	{
		g_learningShips.push_back(new Ship(toCopy, m_generation));
	}
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::createStartingPopulation()
{
	Vec2 position = Vec2(12.5f, 12.5f);
	Vec2 tileToPixelRatio = Vec2((g_maxScreenX / g_numOfTileUnitsX), (g_maxScreenY / g_numOfTileUnitsX));

	g_learningShips;// = new std::vector<Ship*>();

	for (int i = 0; i < m_populationSize; ++i)
	{
		position.x += 15.f;
		if (position.x > g_numOfTileUnitsX)
		{
			position.x = 12.5f;
			position.y += 15.f;
		}

		//g_learningShips.push_back(new Ship(position.x, position.y, 0));
		g_learningShips.push_back(new Ship(g_numOfTileUnitsX * .5, g_numOfTileUnitsY * .5, 0));
	}
}

//----------------------------------------------------------------------------------
Ship* NeuralNetGeneticAlgorithm::selectionForShip()
{
	//Genome* selected = nullptr;
	Ship* selected = nullptr;

	//Tournament: Select best out of n random genomes
	int bestIndex = RandomNumber::getRandomIntInRange(IntVec2(0, g_learningShips.size()));
	double battleStrength = g_learningShips[bestIndex]->m_fitnessInRelationToGeneration;
	//double bestFitness = g_learningShips[bestIndex]->m_fitnessInRelationToGeneration;

	for (int combatantIndex = 0; combatantIndex < g_numberOfCombatants - 1; ++combatantIndex)
	{
		int challenger = RandomNumber::getRandomIntInRange(IntVec2(0, g_learningShips.size()));
		battleStrength -= g_learningShips[challenger]->m_fitnessInRelationToGeneration;


		if (battleStrength <= 0.0)
		{
			bestIndex = challenger;
			battleStrength = g_learningShips[bestIndex]->m_fitnessInRelationToGeneration;
		}
	}

	selected = g_learningShips[bestIndex];
	return selected;
}

//Total Population's score has to be == 1.0 for roulette selection
//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::updateFitnessScores()
{
	double highestFitness = 0.0;
	double totalFitness = 0.0;
	Ship* bestShip = nullptr;
	double averageTurnsAlive = 0.0;
	for (int shipIndex = 0; shipIndex < (int)g_learningShips.size(); ++shipIndex)
	{
		Vec2 distanceVector = Vec2(g_learningShips[shipIndex]->m_position.x - g_learningShips[shipIndex]->m_startPosition.x, g_learningShips[shipIndex]->m_position.y - g_learningShips[shipIndex]->m_startPosition.y);
		g_learningShips[shipIndex]->m_fitnessInRelationToGeneration =
			(g_learningShips[shipIndex]->m_distanceTraveled / g_maxDistanceCanTravel)
			* (g_learningShips[shipIndex]->m_numberOfTurnsAlive)
			* ((double)g_learningShips[shipIndex]->m_visitedAsteroidIDs.size())
			* (1.0 - ((double)max(g_learningShips[shipIndex]->m_numberOfTimesDied, 1.0)) / (double)g_maxIterationsPerGeneration);

		float fitness = g_learningShips[shipIndex]->m_fitnessInRelationToGeneration;

		averageTurnsAlive += g_learningShips[shipIndex]->m_numberOfTurnsAlive;
		totalFitness += fitness;

		if (fitness > highestFitness)
		{
			highestFitness = fitness;
			bestShip = g_learningShips[shipIndex];
		}
	}
	//----------------------------------------------------------------------------------

	float divisionOfFitness = 1.0 / totalFitness;
	averageTurnsAlive = averageTurnsAlive / (double)g_learningShips.size();
	for (int shipIndex = 0; shipIndex < (int)g_learningShips.size(); ++shipIndex)
	{
		g_learningShips[shipIndex]->m_overallFitness = g_learningShips[shipIndex]->m_fitnessInRelationToGeneration;
		g_learningShips[shipIndex]->m_fitnessInRelationToGeneration *= divisionOfFitness;
	}

	//----------------------------------------------------------------------------------

	m_averageFitness = totalFitness / (double) g_learningShips.size();

	m_boltzmanTemperature -= g_boltzmanDelta;
	m_boltzmanTemperature = max(m_boltzmanTemperature, g_boltzmannMinTemp);
	float divider = m_averageFitness / m_boltzmanTemperature;
	divider = 1 / divider;
	float boltDividend = 1 / m_boltzmanTemperature;

	for (int genomeIndex = 0; genomeIndex < (int)g_learningShips.size(); ++genomeIndex)
	{
		g_learningShips[genomeIndex]->m_overallFitness = (g_learningShips[genomeIndex]->m_overallFitness * boltDividend) * divider;
	}

	float newTotal = 0;
	for (int i = 0; i < m_populationSize; ++i)
	{
		newTotal += g_learningShips[i]->m_overallFitness;
	}
	for (int genomeIndex = 0; genomeIndex < (int)g_learningShips.size(); ++genomeIndex)
	{
		g_learningShips[genomeIndex]->m_overallFitness = g_learningShips[genomeIndex]->m_overallFitness / newTotal;
	}

	//----------------------------------------------------------------------------------

	//Store best
	if (!m_currentBestShip)
	{
		m_currentBestShip = new Ship(bestShip, m_generation);
	}
	else
	{
		delete m_currentBestShip;
		m_currentBestShip = new Ship(bestShip, m_generation);
	}

	m_bestShipsPerGeneration.push_back(bestShip);
	gatherData(m_averageFitness, averageTurnsAlive, highestFitness);

}


//----------------------------------------------------------------------------------------------
CrossedShipChildren* NeuralNetGeneticAlgorithm::shipCrossover(const Ship* mom, const Ship* dad)
{
	CrossedShipChildren* toReturn = new CrossedShipChildren();

	toReturn->child1 = new Ship(mom, m_generation);
	toReturn->child2 = new Ship(dad, m_generation);

	if ((RandomNumber::getRandomFloatZeroToOne() > g_crossoverRate) || (mom == dad))
	{
		return toReturn;
	}

	std::vector<vector<float>> momSplitGenome;
	std::vector<vector<float>> dadSplitGenome;

	mom->m_net->splitNetOnNeuronBoundaries(momSplitGenome);
	dad->m_net->splitNetOnNeuronBoundaries(dadSplitGenome);

	//determine two crossover points
	int index1 = RandomNumber::getRandomIntInRange(IntVec2(0, momSplitGenome.size() - 2));

	int numOfSplits = 4;
	std::vector<int> splitIndexes;
	splitIndexes.push_back(index1);
	for (int i = 1; i < numOfSplits; ++i)
	{
		splitIndexes.push_back(RandomNumber::getRandomIntInRange(IntVec2(splitIndexes[i - 1], momSplitGenome.size() - numOfSplits + i)));
	}

	std::vector<float> child1Genome;
	std::vector<float> child2Genome;

	auto testSplitIndex = [&](const std::vector<int>& indexes, int indexToCheck)->bool
	{ 
		bool toReturn = false;
		for (int i = 0; i < indexes.size() - 1; ++i)
		{
			if (i % 2 == 0)
			{
				if (indexToCheck >= indexes[i] && indexToCheck < indexes[i + 1])
				{
					toReturn = true;
				}
			}
		}

		return toReturn; 
	};

	for (int i = 0; i < (int) momSplitGenome.size(); ++i)
	{
		if (/*(i < index1) || (i >= index2)*/ testSplitIndex(splitIndexes, i))
		{
			//keep the same genes if outside of crossover points
			for (int weightIndex = 0; weightIndex < momSplitGenome[i].size(); ++weightIndex)
			{
				child1Genome.push_back(momSplitGenome[i][weightIndex]);
				child2Genome.push_back(dadSplitGenome[i][weightIndex]);
			}
		}
		else
		{
			for (int weightIndex = 0; weightIndex < momSplitGenome[i].size(); ++weightIndex)
			{
				child1Genome.push_back(dadSplitGenome[i][weightIndex]);
				child2Genome.push_back(momSplitGenome[i][weightIndex]);
			}
		}
	}

	toReturn->child1->m_net->replaceWeights(child1Genome);
	toReturn->child2->m_net->replaceWeights(child2Genome);

	return toReturn;
}

//Update to a new generation
//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::epoch()
{
	updateFitnessScores();
	++m_generation;
	generateNewPopulation();
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::generateNewPopulation()
{
	std::vector<Ship*> m_childrenGenomes;

	if (m_currentBestShip && g_hasElitism)
	{
		int numberOfIterations = g_learningShips.size() * .01;
		for (int i = 0; i < numberOfIterations; ++i)
		{ 
			m_childrenGenomes.push_back(new Ship(m_currentBestShip->m_net->getWeights(), m_generation));
		}
	}
	while ((int)m_childrenGenomes.size() < m_populationSize)
	{
		int i = 0;
		Ship* parent1 = selectionForShip();
		Ship* parent2 = selectionForShip();

		CrossedShipChildren* children = shipCrossover(parent1, parent2);

		Ship* child1 = children->child1;
		Ship* child2 = children->child2;

		if (child1)
		{
			child1->mutate();
		}

		if (child2)
		{
			child2->mutate();
		}

		m_childrenGenomes.push_back(child1);
		m_childrenGenomes.push_back(child2);
	}

	for (int i = 0; i < (int) g_learningShips.size(); ++i)
	{
		delete g_learningShips[i];
	}

	g_learningShips.clear();
	g_learningShips = m_childrenGenomes;

	int i = 0;
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::render()
{
	if (g_learningShips.size() > 0)
	{
		for (int i = 0; i < (int) g_learningShips.size(); ++i)
		{
			g_learningShips[i]->renderEntity();
		}
	}
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::update()
{
	if (g_learningShips.size() > 0)
	{
		for (int i = 0; i < (int)g_learningShips.size(); ++i)
		{
			//if (!g_learningShips[i]->m_isDead)
			{
				g_learningShips[i]->updateEntity();
			}
		}
	}
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::updateCollisions(const std::vector<Asteroid>& asteroids)
{
	if (g_learningShips.size() > 0)
	{
		for (int i = 0; i < (int)g_learningShips.size(); ++i)
		{
			g_learningShips[i]->updateCollisions(asteroids);
		}
	}
}


//**THIS FUNCTION IS NOT BEING USED**
//----------------------------------------------------------------------------------
Genome* NeuralNetGeneticAlgorithm::selection()
{
	return nullptr;
}

//Not Used
//----------------------------------------------------------------------------------------------
CrossedChildren* NeuralNetGeneticAlgorithm::crossover(
	const Genome* mom,
	const Genome* dad
	)
{
	return nullptr;
}


//----------------------------------------------------------------------------------
int NeuralNetGeneticAlgorithm::howManyShipsAreAlive()
{
	int count = 0;
	for (int i = 0; i < (int)g_learningShips.size(); ++i)
	{
		if (!g_learningShips[i]->m_isDead)
		{
			++count;
		}
	}
	return count;
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::gatherData(double averageFitness, double averageLifeSpan, double maxFitness)
{
	Data toAdd;
	toAdd.averageFitness = averageFitness;
	toAdd.averageLifeSpan = averageLifeSpan;
	toAdd.numberAlive = howManyShipsAreAlive();
	toAdd.generation = m_generation;
	toAdd.maxFitness = maxFitness;
	m_data.push_back(toAdd);
}

//----------------------------------------------------------------------------------
void NeuralNetGeneticAlgorithm::saveData()
{
	ClearFile(m_saveFile);

	LogAppendPrintf(m_saveFile, "Generation,Average Fitness,Average Life Span,Number Survived,Highest Fitness\n");
	for (int i = 0; i < (int) m_data.size(); ++i)
	{
		LogAppendPrintf(m_saveFile, "%d,%f,%f,%d,%f\n", m_data[i].generation, m_data[i].averageFitness, m_data[i].averageLifeSpan, m_data[i].numberAlive, m_data[i].maxFitness);
	}
	LogAppendPrintf(m_saveFile, "\0");
}