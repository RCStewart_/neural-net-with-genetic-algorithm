#include "GeneticAlgorithm.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include "Engine/Core/GameCommon.hpp"

//----------------------------------------------------------------------------------
GeneticAlgorithm::GeneticAlgorithm(double crossOverRate, double mutationRate, int populationSize, int geneLength)
	: m_crossoverRate(crossOverRate)
	, m_mutationRate(mutationRate)
	, m_populationSize(populationSize)
	, m_generation(0)
	, m_isBusy(false)
	, m_geneLength(geneLength)
	, m_currentBestGenome(nullptr)
{
}

//----------------------------------------------------------------------------------
void GeneticAlgorithm::createStartingPopulation()
{
}


//----------------------------------------------------------------------------------
Genome* GeneticAlgorithm::selection()
{
	return nullptr;
}

//Total Population's score has to be == 1.0
//----------------------------------------------------------------------------------
void GeneticAlgorithm::updateFitnessScores()
{

}

//Generate 2 child genomes based on two parents
//Must create valid children for the problem
//----------------------------------------------------------------------------------
CrossedChildren* GeneticAlgorithm::crossover(const Genome* mom, const Genome* dad/*, Genome* child1, Genome* child2*/)
{
	UNUSED(mom);
	UNUSED(dad);

	return nullptr;
}


//Update to a new generation
//----------------------------------------------------------------------------------
void GeneticAlgorithm::epoch()
{
	updateFitnessScores();
	++m_generation;
	generateNewPopulation();
}

//----------------------------------------------------------------------------------
void GeneticAlgorithm::generateNewPopulation()
{
	//While (Create new children until children == population size)
	
	/*
	//Select Parents
	//Crossover
	//Mutate
	*/

	//old parents = children
}

//----------------------------------------------------------------------------------
GeneticAlgorithm::~GeneticAlgorithm()
{
	for (int i = 0; i < m_genomes.size(); ++i)
	{
		delete m_genomes[i];
	}
	vector<Genome*> emptyGenomes;
	m_genomes = emptyGenomes;

	for (int i = 0; i < m_bestGenomesPerGeneration.size(); ++i)
	{
		delete m_bestGenomesPerGeneration[i];
	}
	vector<Genome*> emptyGenomesPerGeneration;
	m_bestGenomesPerGeneration = emptyGenomesPerGeneration;

}