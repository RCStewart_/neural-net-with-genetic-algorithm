#include "GeneticAlgorithmDefines.hpp"

double g_crossoverRate = 0.90;
double g_mutationRate = 0.001;
int g_populationSize = 100;
int g_chromozoneLength = 50;
double g_boltzmanDelta = .005;
double g_boltzmannMinTemp = 1;
bool g_hasElitism = true;
double g_maxIterationsPerGeneration = 1200.0;
double g_maxDistanceCanTravel = 1200.0;

double g_maxMutationAmount = 0.10;

int g_numberOfCombatants = 5;

bool g_hideSensers = true;

bool g_freeMode = false;
