#include "NeuralNetGenome.hpp"

#include "Engine/Core/UtilityFunctions.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "GeneticAlgorithmDefines.hpp"
#include "Engine/Math/RandomNumber.hpp"


//----------------------------------------------------------------------------------
NeuralNetGenome::NeuralNetGenome(const std::vector<float> &startingDNA, int generation)
	: Genome(startingDNA, generation)
	, m_longestStreak(0)
	, m_totalFitness(0)
{

}

//----------------------------------------------------------------------------------
NeuralNetGenome::~NeuralNetGenome()
{

}

//----------------------------------------------------------------------------------
NeuralNetGenome::NeuralNetGenome(const NeuralNetGenome* toCopy)
{
	m_fitness = toCopy->m_fitness;
	m_generation = toCopy->m_generation;
	m_DNA = toCopy->m_DNA;
	m_longestStreak = toCopy->m_longestStreak;
	m_totalFitness = toCopy->m_totalFitness;
}

//----------------------------------------------------------------------------------
void NeuralNetGenome::mutate()
{
	for (int i = 0; i < m_DNA.size(); ++i)
	{
		if (RandomNumber::getRandomFloatZeroToOne() < MUTATION_RATE)
		{
			m_DNA[i] += RandomNumber::getRandomFloatZeroToOne() * g_maxMutationAmount;
		}
	}
}