#pragma once
#include <vector>
#include "Genome.hpp"

class SortingGenome : public Genome
{
public:
	SortingGenome(const std::vector<float> &chromosomesToSort, int generation);
	SortingGenome(const SortingGenome* toCopy);
	~SortingGenome();

	virtual void mutate();

	int m_totalFitness;
	int m_longestStreak;
};