#include "Genome.hpp"
#include "Engine/Math/RandomNumber.hpp"

//----------------------------------------------------------------------------------
Genome::Genome()
{

}

//----------------------------------------------------------------------------------
Genome::Genome(int numOfRandomChromosomesToAdd, float min, float max, int generation)
	: m_fitness(0.0)
	, m_generation(generation)
{
}

//----------------------------------------------------------------------------------
Genome::Genome(const std::vector<float> &startingDNA, int generation)
	: m_DNA(startingDNA)
	, m_fitness(0.0)
	, m_generation(generation)
{

}

//----------------------------------------------------------------------------------
Genome::~Genome()
{

}

//----------------------------------------------------------------------------------
void Genome::mutate()
{

}