#pragma once

#include "GeneticAlgorithm.hpp"
#include "../NeuralNet/NeuralNet.hpp"
#include "../Ship.hpp"
#include <string>

struct Data
{
	int generation;
	double averageFitness;
	double averageLifeSpan;
	int numberAlive;
	double maxFitness;
};

class CrossedShipChildren
{
public:
	CrossedShipChildren()
	{
		child1 = nullptr;
		child2 = nullptr;
	}

	~CrossedShipChildren()
	{
		delete child1;
		delete child2;
	}

	Ship* child1;
	Ship* child2;
};

class Asteroid;

class NeuralNetGeneticAlgorithm : public GeneticAlgorithm
{
public:
	NeuralNetGeneticAlgorithm(
		double crossOverRate,
		double mutationRate,
		int populationSize,
		int geneLength
		);

	CrossedChildren* crossover(
		const Genome* mom,
		const Genome* dad
		);

	CrossedShipChildren* shipCrossover(const Ship* mom, const Ship* dad);
	
	void generatePopulationBasedOnShip(const Ship* toCopy);

	virtual void epoch();
	virtual Genome* selection();
	Ship* selectionForShip();

	virtual void updateFitnessScores();
	virtual void createStartingPopulation();
	virtual void generateNewPopulation();

	void render();
	void update();
	void updateCollisions( const std::vector<Asteroid>& asteroids );
	void gatherData(double averageFitness, double averageLifeSpan, double maxFitness);
	void saveData();
	int howManyShipsAreAlive();

	double m_averageFitness;

	std::vector<Ship*> m_bestShipsPerGeneration;
	std::vector<Data> m_data;
	Ship* m_currentBestShip;

	std::string m_saveFile;

	float m_boltzmanTemperature;
};

extern std::vector<Ship*> g_learningShips;
