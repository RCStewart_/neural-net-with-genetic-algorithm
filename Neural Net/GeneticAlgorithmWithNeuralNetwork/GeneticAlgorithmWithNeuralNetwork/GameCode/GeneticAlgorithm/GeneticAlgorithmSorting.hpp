#pragma once

#include "GeneticAlgorithm.hpp"
#include "SortingGenome.hpp"

class GeneticAlgorithmSorting : public GeneticAlgorithm
{
public:
	GeneticAlgorithmSorting(
		double crossOverRate,
		double mutationRate,
		int populationSize,
		int geneLength
		);

	CrossedChildren* crossover(
		const Genome* mom,
		const Genome* dad
		);

	virtual void epoch();
	virtual Genome* selection();
	virtual void updateFitnessScores();
	virtual void createStartingPopulation();
	virtual void generateNewPopulation();

	float m_boltzmanTemperature;
};
