#pragma once
#include <vector>
#include "Genome.hpp"

class NeuralNetGenome : public Genome
{
public:
	NeuralNetGenome(const std::vector<float> &chromosomesToSort, int generation);
	NeuralNetGenome(const NeuralNetGenome* toCopy);
	~NeuralNetGenome();

	virtual void mutate();

	int m_totalFitness;
	int m_longestStreak;
};