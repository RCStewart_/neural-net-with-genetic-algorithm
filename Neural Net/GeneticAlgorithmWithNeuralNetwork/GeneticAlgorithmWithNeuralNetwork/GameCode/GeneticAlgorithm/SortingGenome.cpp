#include "SortingGenome.hpp"

#include "Engine/Core/UtilityFunctions.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "GeneticAlgorithmDefines.hpp"
#include "Engine/Math/RandomNumber.hpp"


//----------------------------------------------------------------------------------
SortingGenome::SortingGenome(const std::vector<float> &startingDNA, int generation)
	: Genome(startingDNA, generation)
	, m_longestStreak(0)
	, m_totalFitness(0)
{

}

//----------------------------------------------------------------------------------
SortingGenome::~SortingGenome()
{

}

//----------------------------------------------------------------------------------
SortingGenome::SortingGenome(const SortingGenome* toCopy)
{
	m_fitness = toCopy->m_fitness;
	m_generation = toCopy->m_generation;
	m_DNA = toCopy->m_DNA;
	m_longestStreak = toCopy->m_longestStreak;
	m_totalFitness = toCopy->m_totalFitness;
}

//----------------------------------------------------------------------------------
void SortingGenome::mutate()
{
	//Insertion Mutation

	if (RandomNumber::getRandomFloatZeroToOne() > MUTATION_RATE)
	{
		return;
	}

	float percentOfChromosomesToMutate = .05f;
	float numberOfChromosomePairsToMutate = percentOfChromosomesToMutate * (float) m_DNA.size();

	//Will move at least one chromosome
	UtilityFunctions::clampFloat(numberOfChromosomePairsToMutate, 1.f, (float) m_DNA.size() * .5f);

	//Begin Mutation
	for (int i = 0; i < numberOfChromosomePairsToMutate; ++i)
	{
		int firstIndex = RandomNumber::getRandomIntInRange(IntVec2(0, m_DNA.size() - 1));
		int secondIndex = RandomNumber::getRandomIntInRange(IntVec2(0, m_DNA.size() - 1));
		//Move element from firstIndex to secondIndex
		float elementToMove = m_DNA[firstIndex];
		m_DNA.erase(m_DNA.begin() + firstIndex);
		m_DNA.insert(m_DNA.begin() + secondIndex, elementToMove);
	}
}