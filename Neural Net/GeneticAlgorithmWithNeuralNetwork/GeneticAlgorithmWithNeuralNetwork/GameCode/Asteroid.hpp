#pragma once
#include "Entity.hpp"
#include <map>

using namespace std;
class Asteroid : public Entity
{
public:
	Asteroid(void);
	Asteroid(float x, float y);

	~Asteroid(void);

	void generateStartingPosition();
	virtual void renderEntity();
	virtual void updateEntity();
	unsigned int m_index;
};


extern unsigned int g_asteroidIndex;
