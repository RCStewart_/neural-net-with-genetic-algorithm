#include "TheGame.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Renderer/GLWrapper.hpp"

#include "Engine\Core\WindowsWrapper.hpp"

#include "Engine\Core\EngineCommon.hpp"
#include "Engine\Core\GameCommon.hpp"
#include "Engine\Font\FontGenerator.hpp"

#include "Engine/Math/Vec2.hpp"

#include <time.h>
#include <math.h>
#include <cassert>
#include <crtdbg.h>

#include "GeneticAlgorithm/NeuralNetGeneticAlgorithm.hpp"
#include "GeneticAlgorithm/GeneticAlgorithmDefines.hpp"

#include <iostream>
#include <sstream>

#include "Engine/Files/FileSystem.hpp"

TheGame* g_theGame = nullptr;

//---------------------------------------------------------------------------
TheGame::TheGame(void)
{
	g_theGame = this;
}


//----------------------------------------------------------------------------------
void TheGame::run()
{
	initializeGame();

	while(!m_isQuitting)
	{
		runFrame();
	}
}

//----------------------------------------------------------------------------------
void TheGame::runFrame()
{
	runMessagePump();
	processInput();

	const double deltaSeconds = 1.f / 60.f; //Use actual wait time later
	update( deltaSeconds );
	render();
	++m_currentIteration;
}

//----------------------------------------------------------------------------------
void TheGame::runMessagePump()
{
	WindowWrapperRunMessagePump();
}

//----------------------------------------------------------------------------------
bool TheGame::processInput()
{
	//Reset All
	if (WindowWrapperGetIsInputActiveAndConsume('R'))
	{
		resetAsteroids();
		for (int i = 0; i < (int) g_learningShips.size(); ++i)
			g_learningShips[i]->randomizeWeights();
	}

	//Generate Populate Based On Saved Genome
	if (WindowWrapperGetIsInputActiveAndConsume('G'))
	{
		resetAsteroids();
		FileBuffer data;
		g_theFileSystem->loadFile(m_bestShipFileName, data, false);

		if (data.size == 0)
		{
			return false;
		}

		std::string buffer = std::string((char*)data.buffer);
		std::stringstream ss = std::stringstream(buffer);
		std::string tempString = "\0";

		std::vector<float> weights;

		while (getline(ss, tempString, ' '))
		{
			if (std::stof(tempString))
			{
				weights.push_back(std::stof(tempString));
			}
		}
		
		Ship* loadedShip = new Ship(0);
		loadedShip->m_net->replaceWeights(weights);

		m_populationController->generatePopulationBasedOnShip(loadedShip);

		delete loadedShip;
	}

	//Load Last Saved Genome
	if (WindowWrapperGetIsInputActiveAndConsume('L'))
	{
		FileBuffer data;
		g_theFileSystem->loadFile(m_bestShipFileName, data, false);

		if (data.size == 0)
		{
			return false;
		}

		std::string buffer = std::string( (char*) data.buffer);
		std::stringstream ss = std::stringstream(buffer);
		std::string tempString = "\0";

		std::vector<float> weights;

		while (getline(ss, tempString, ' '))
		{
			if (std::stof(tempString))
			{			
				weights.push_back(std::stof(tempString));
			}
		}

		if (!m_populationController->m_currentBestShip)
		{
			m_populationController->m_currentBestShip = new Ship(m_populationController->m_generation);
		}

		m_populationController->m_currentBestShip->m_net->replaceWeights(weights);
	}

	//Save Current Best Genome
	if (WindowWrapperGetIsInputActiveAndConsume('S'))
	{
		if (m_populationController->m_currentBestShip)
		{
			ClearFile(m_bestShipFileName);

			std::vector<float> weights = m_populationController->m_currentBestShip->m_net->getWeights();

			for (int i = 0; i < weights.size(); ++i)
			{
				LogAppendPrintf(m_bestShipFileName, "%f", weights[i]);

				if (i != weights.size() -1)
				LogAppendPrintf(m_bestShipFileName, " ");
			}
			LogAppendPrintf(m_bestShipFileName, "\0");
		}
	}

	//Save Learning Data Progress
	if (WindowWrapperGetIsInputActiveAndConsume('D'))
	{
		m_populationController->saveData();
	}

	//Hide sensors
	if (WindowWrapperGetIsInputActiveAndConsume('H'))
	{
		g_hideSensers = !g_hideSensers;
	}

	//Free Fly Mode
	if (WindowWrapperGetIsInputActiveAndConsume('F'))
	{
		g_freeMode = !g_freeMode;
	}

	//End
	if (WindowWrapperIsInputActive(WINDOW_WRAPPER_INPUT_ESCAPE))
	{
		m_isQuitting = true;

		delete m_populationController;
		m_populationController = nullptr;
	}
	return true;
}

//----------------------------------------------------------------------------------------------
void TheGame::resetAsteroids()
{
	float startX = 7.f;
	m_asteroids.clear();
	int numOfAsteroidsToHave = 38;
	Vec2 newPosition = Vec2(startX, 10.f);

	Vec2 tileToPixelRatio = Vec2((g_maxScreenX / g_numOfTileUnitsX), (g_maxScreenY / g_numOfTileUnitsX));
	float incrementAmount = 16.5f;

	for (int i = 0; i < numOfAsteroidsToHave; ++i)
	{
		newPosition.x += incrementAmount;
		if (newPosition.x > g_numOfTileUnitsX)
		{
			newPosition.x = startX;
			newPosition.y += incrementAmount;
		}
		m_asteroids.push_back(Asteroid(newPosition.x, newPosition.y));
	}
}

//---------------------------------------------------------------------------
void TheGame::update( double deltaSeconds )
{
	UNUSED(deltaSeconds);

	if (m_currentIteration >= g_maxIterationsPerGeneration && !g_freeMode/*|| m_populationController->howManyShipsAreAlive() == 0*/)
	{
		m_currentIteration = 0;
		resetAsteroids();
		m_populationController->epoch();
	}

	//if (!g_freeMode)
	{
		for (int i = 0; i < (int)m_asteroids.size(); ++i)
		{
			m_asteroids[i].updateEntity();
		}
	}

	m_populationController->update();
	m_populationController->updateCollisions(m_asteroids);
}


//---------------------------------------------------------------------------
void TheGame::render()
{
	//GLWrapperSetUpInitialPerspectiveProjection();
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT);

	m_drawWith.renderWithDepth();
	
	//----------------------------------------------------------------------------------
	//m_temporaryPlayerShip->renderEntity( );
	for (int i = 0; i < m_asteroids.size(); ++i)
	{
		m_asteroids[i].renderEntity();
	}
	//----------------------------------------------------------------------------------

	m_populationController->render();

	//----------------------------------------------------------------------------------
	Vec2 drawLocation(10.f, 60.f);
	
	
	int currentGen			= 0;
	float percentOfCurrent	= 0;
	float fitnessOfCurrent	= 0;
	int generationOfHighest	= 0;
	float percentOfHighest	= 0;
	float highestFitness	= 0;

	m_font->drawText(" ", GENERATED_FONTS_BIT, 2.f, drawLocation, RGBAColors(255, 255, 255, 255), false);

	float maxDeltaY = 40.f;
	float deltaX = .01f;

	Vec3 position = Vec3(10.f, 10.f, 0.f);

	double timeNow = WindowWrapperGetCurrentTimeSeconds();
	static double previousTime = timeNow;
	double timeSinceLastFrame = timeNow - previousTime;
	while (timeSinceLastFrame < m_deltaTime)
	{
		timeNow = WindowWrapperGetCurrentTimeSeconds();
		timeSinceLastFrame = timeNow - previousTime;
	}

	previousTime = timeNow;

	GLWrapperSwapBuffers(g_displayDeviceContext);
}

//---------------------------------------------------------------------------
void TheGame::initializeGame()
{
	srand((unsigned int) time(NULL));

	m_currentIteration = 0;
	m_isQuitting = false;
	m_font = new FontGenerator();
	m_font->initialize();

	m_fps = 60;
	m_deltaTime = 1.0f / m_fps;

	resetAsteroids();

	m_populationController = new NeuralNetGeneticAlgorithm(g_crossoverRate, g_mutationRate, g_populationSize, g_chromozoneLength);
	m_bestShipFileName = "bestShip.txt";

	g_theFileSystem = new FileSystem(nullptr);
}

//---------------------------------------------------------------------------
TheGame::~TheGame(void)
{
	
}