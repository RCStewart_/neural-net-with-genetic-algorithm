#include "Ship.hpp"
#include <windows.h>
#include <gl/gl.h>
#include "Engine/Core/GameCommon.hpp"
#include <cmath>
#include "Engine/Renderer/DrawDebug.hpp"
#include "Engine/Math/Vec3.hpp"
#include "Sensor.hpp"
#include "NeuralNet/NeuralNet.hpp"

#include "Engine/Time/UtilitiesTime.hpp"
#include "Engine/Core/UtilityFunctions.hpp"
#include "Asteroid.hpp"

#include "GeneticAlgorithm/GeneticAlgorithmDefines.hpp"
#include "Engine/Math/RandomNumber.hpp"

using namespace std;

//----------------------------------------------------------------------------------
Ship::Ship(int generation)
{
	initialize(generation);
}

//----------------------------------------------------------------------------------------------
Ship::Ship(const Ship* toCopy, int generation)
{
	initialize(generation);
	if (toCopy)
	{
		delete m_net;

		m_net = new NeuralNet(
			toCopy->m_net->m_numberOfInputs,
			toCopy->m_net->m_numberOfOutputs,
			toCopy->m_net->m_numberOfHiddenLayers,
			toCopy->m_net->m_neuronsPerHiddenLayer,
			toCopy->m_net->getWeights());
	}
	else
	{
		
	}

	m_generation = generation;
}

//----------------------------------------------------------------------------------
Ship::Ship(float x, float y, int generation)
{
	initialize(generation);
	m_position = Vec2(x, y);
}

//----------------------------------------------------------------------------------
Ship::Ship(const std::vector<float>& weights, int generation)
{
	initialize(generation);
	m_net->replaceWeights(weights);
}

//----------------------------------------------------------------------------------
void Ship::modifyOrientationDegrees(float degrees)
{
	m_orientationDegrees += degrees;
}


//----------------------------------------------------------------------------------
void Ship::forwardAcceleration(float scalarAcceleration)
{
	Vec2 accelerationVector = m_drawPoints[0].rotateToDegrees(m_orientationDegrees);
	accelerationVector.normalize();
	accelerationVector.scalarMultiplication(scalarAcceleration);
	m_velocity.x = accelerationVector.x + m_velocity.x;
	m_velocity.y = accelerationVector.y + m_velocity.y;
}

//----------------------------------------------------------------------------------
void Ship::randomizeWeights()
{
	m_net->randomizeWeights();
	m_velocity = Vec2(0.001f, 0.f);
	m_isDead = false;
	m_startTime = GetAbsoluteTimeSeconds();
	m_orientationDegrees = m_velocity.getBaseOrientationDegrees();
	m_numberOfTurnsAlive = 0;
}

//----------------------------------------------------------------------------------
void Ship::updateEntity()
{
	m_totalSpeed += m_velocity.getMagnetude();
	
	float spin = 10.0f;
	float accelerationScalar = .1f * m_scale;
	double percentPower = 1.0;

	//double leftControl = 0.0;
	//double rightControl = 0.0;

	double	leftControlTurn = 0.0;
	double 	leftControlForward = 0.0;
	double 	rightControlTurn = 0.0;
	double 	rightControlForward = 0.0;

	std::vector<float> inputs;

	for (int sensorIndex = 0; sensorIndex < (int)m_sensors.size(); ++sensorIndex)
	{
		inputs.push_back(m_sensors[sensorIndex]->m_isColliding);
	}
	//inputs.push_back(m_velocity.getMagnetude());

	std::vector<float> outputs = m_net->update(inputs);

	double multiplier = 3.0;
	leftControlTurn = outputs[0] * multiplier;
	leftControlForward = outputs[1] * multiplier;
	rightControlTurn = outputs[2] * multiplier;
	rightControlForward = outputs[3] * multiplier;

	Vec2 leftAtOrientationTurn = Vec2(abs(m_leftOffset.x * leftControlTurn) * -1.f, m_leftOffset.y * leftControlTurn).rotateToDegrees(m_orientationDegrees);
	Vec2 leftAtOrientationForward = Vec2(abs(m_leftOffset.x * leftControlForward) * -1.f, m_leftOffset.y * leftControlForward).rotateToDegrees(m_orientationDegrees);
	Vec2 rightAtOrientationTurn = Vec2(abs(m_rightOffset.x * rightControlTurn), m_rightOffset.y * rightControlTurn).rotateToDegrees(m_orientationDegrees);
	Vec2 rightAtOrientationForward = Vec2(abs(m_rightOffset.x * rightControlForward), m_rightOffset.y * rightControlForward).rotateToDegrees(m_orientationDegrees);

	m_velocity = Vec2::addVectors(leftAtOrientationTurn, m_velocity);
	m_velocity = Vec2::addVectors(rightAtOrientationForward, m_velocity);
	m_velocity = Vec2::addVectors(leftAtOrientationTurn, m_velocity);
	m_velocity = Vec2::addVectors(rightAtOrientationForward, m_velocity);

	float lastOrientation = m_orientationDegrees;

	m_orientationDegrees = m_velocity.getBaseOrientationDegrees();

	Vec2 lastPosition = m_position;
	Entity::updateEntity();
	Vec2 newPosition = m_position;
	newPosition.scalarMultiplication(-1.f);

	if (!m_isDead)
	{
		m_distanceTraveled += abs(Vec2::addVectors(lastPosition, newPosition).getMagnetude())
			* (1.0 - min(abs(lastOrientation - m_orientationDegrees), abs(m_orientationDegrees - lastOrientation)) / 180.0)
			/** (1.0 - (double)m_numberOfTurnsAlive/(double) g_maxIterationsPerGeneration)*/;
		++m_numberOfTurnsAlive;
	}

	float maxVelocity = 1.0f;

	if (m_velocity.getMagnetude() >= maxVelocity)
	{
		m_velocity.normalize();
		m_velocity.scalarMultiplication(maxVelocity);
	}

	m_velocity.scalarMultiplication(.99f);
}

//----------------------------------------------------------------------------------
void Ship::renderEntity()
{
	double percentPower = 1.0;

	double controllerX = 0.0;
	double controllerY = 0.0;

	if (m_isDead)
	{
		glColor4f(1.f, 0.f, 0.f, 1.f);
	}
	{
		Entity::renderEntity();

		if (!g_hideSensers)
		{
			for (int i = 0; i < (int)m_sensors.size(); ++i)
			{
				m_sensors[i]->Render(this);
			}
		}

		glColor4f(1.f, 1.f, 1.f, 1.f);
	}
}

//----------------------------------------------------------------------------------
Ship::~Ship(void)
{
	delete m_net;

	for (int sensorIndex = 0; sensorIndex < (int)m_sensors.size(); ++sensorIndex)
	{
		delete m_sensors[sensorIndex];
	}
}

//----------------------------------------------------------------------------------
bool Ship::updateCollisions(const std::vector<Asteroid>& asteroids)
{
	for (int asteroidIndex = 0; asteroidIndex < (int)asteroids.size(); ++asteroidIndex)
	{
		if (UtilityFunctions::detectCircleCollision(m_radius * m_scale, m_position, asteroids[asteroidIndex].m_radius * asteroids[asteroidIndex].m_scale, asteroids[asteroidIndex].m_position))
		{
			m_isDead = true;
			m_timeAlive = GetAbsoluteTimeSeconds() - m_startTime;
			++m_numberOfTimesDied;
			return true;
		}
	}

	//if (!m_isDead)
	{
		for (int sensorIndex = 0; sensorIndex < (int)m_sensors.size(); ++sensorIndex)
		{
			m_sensors[sensorIndex]->updateCollisions(this, asteroids);
		}
	}
	return true;
}

//----------------------------------------------------------------------------------
void Ship::initialize(int generation)
{
	m_generation = generation;
	m_fitnessInRelationToGeneration = 0.0;
	m_overallFitness = 0.0;
	m_timeAlive = 0.0;
	m_distanceTraveled = 0.0;
	m_numberOfTurnsAlive = 0;
	m_sigmaFitness = 0.0f;
	m_totalSpeed = 0.0f;
	m_numberOfTimesDied = 0;

	Entity::Entity();

	m_net = nullptr;

	m_isDead = false;
	m_velocity = Vec2(.0f, 0.f);

	Vec2 tileToPixelRatio = Vec2((g_maxScreenX / g_numOfTileUnitsX), (g_maxScreenY / g_numOfTileUnitsX));

	m_position = Vec2(g_numOfTileUnitsX * .5f, g_numOfTileUnitsY * .5f);

	m_drawPoints.push_back(Vec2(.5f * tileToPixelRatio.x, 0.f));
	m_drawPoints.push_back(Vec2(-.5f * tileToPixelRatio.x, 1.0f * tileToPixelRatio.y));
	m_drawPoints.push_back(Vec2(-.5f * tileToPixelRatio.x, -1.0f * tileToPixelRatio.y));

	m_drawOrder.push_back(0);
	m_drawOrder.push_back(2);
	m_drawOrder.push_back(1);

	m_orientationDegrees = 0.f;
	m_radius = m_drawPoints[0].getMagnetude();

	for (int i = 0; i < (int)m_drawPoints.size(); ++i)
	{
		testingNewPoints.push_back(m_drawPoints[i].rotateToDegrees(-90.f));
	}

	for (int orientation = 0; orientation < 360; orientation += 90)
	{
		m_sensors.push_back(new Sensor(orientation, 18.0f, 5.0f));
	}

	m_sensors.push_back(new Sensor(0, 18.0f, 0.0f));
	m_sensors.push_back(new Sensor(30, 12.0f, 4.0f));
	m_sensors.push_back(new Sensor(330, 12.0f, 4.0f));

	int numberOfInputs = 7;
	int numberOfOutputs = 4;
	int numberOfHiddenLayers = 1;
	int numberOfNodesPerHiddenLayer = 16;

	m_net = new NeuralNet(numberOfInputs, numberOfOutputs, numberOfHiddenLayers, numberOfNodesPerHiddenLayer);

	m_leftOffset = Vec2(.0f, .1f);
	m_rightOffset = Vec2(.0f, -.1f);
	m_leftForward = Vec2(.1f, 0.f);
	m_rightForward = Vec2(.1f, 0.f);

	m_startTime = GetAbsoluteTimeSeconds();

	m_isDead = false;
	m_startPosition = m_position;
}

//----------------------------------------------------------------------------------
void Ship::mutate()
{
	////Hill Climb
	std::vector<float> chomosome = m_net->getWeights();
	for (int i = 0; i < chomosome.size(); ++i)
	{
		if (RandomNumber::getRandomFloatZeroToOne() < g_mutationRate)
		{
			chomosome[i] += RandomNumber::getRandomFloatZeroToOne() * g_maxMutationAmount;
		}
	}

	//Chance to jump
	if (RandomNumber::getRandomFloatZeroToOne() < g_mutationRate)
	{
		for (int i = 0; i < chomosome.size(); ++i)
		{
			if (RandomNumber::getRandomFloatZeroToOne() < g_mutationRate)
			{
				chomosome[i] *= -1.f;
			}
		}
	}


	//randomize weights if from a 0 weight
	if (RandomNumber::getRandomFloatZeroToOne() < g_mutationRate)
	{
		for (int i = 0; i < chomosome.size(); ++i)
		{
			if (RandomNumber::getRandomFloatZeroToOne() < g_mutationRate)
			{
				if (abs(chomosome[i] - 0.0f) >= .000001)
				{
					chomosome[i] = RandomNumber::getRandomFloatInRange(-1.f, 1.f);
				}
			}
		}
	}

	m_net->replaceWeights(chomosome);
}