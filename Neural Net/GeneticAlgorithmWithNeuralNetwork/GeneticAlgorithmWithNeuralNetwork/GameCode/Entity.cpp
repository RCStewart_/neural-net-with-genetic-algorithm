#include "Entity.hpp"
#include <windows.h>
#include <gl/gl.h>
#include <math.h>
#include "Engine/Core/GameCommon.hpp"

//----------------------------------------------------------------------------------
Entity::Entity(void)
{
	m_scale = .2f;
}

//----------------------------------------------------------------------------------
void Entity::updateEntity()
{
	m_position = Vec2::addVectors(m_position, m_velocity);
	if(m_position.x < -m_radius)
	{
		m_position.x = g_numOfTileUnitsX + m_radius;
	}
	if (m_position.x > g_numOfTileUnitsX + m_radius)
	{
		m_position.x = -m_radius;
	}
	if(m_position.y < - m_radius)
	{
		m_position.y = g_numOfTileUnitsY + m_radius;
	}
	if (m_position.y > g_numOfTileUnitsY + m_radius)
	{
		m_position.y = -m_radius;
	}
}

//----------------------------------------------------------------------------------
void Entity::renderEntity()
{
	for( int i = 0; i < (int) m_drawOrder.size(); i = i + 2 )
	{

		//Move the entity to the proper position
		glPushMatrix();
		glTranslatef(m_position.x, m_position.y, 0.f);

		//Set the new orientation
		glPushMatrix();
		glRotatef(m_orientationDegrees, 0.f, 0.f, 1.f);

		//Scale the entity to the proper size
		glPushMatrix();
		glScalef(m_scale, m_scale, m_scale);

		float x_start = m_drawPoints[m_drawOrder[i]].x;
		float y_start = m_drawPoints[m_drawOrder[i]].y;
		float x_end = 0.f;
		float y_end = 0.f;
		if (i + 1 == m_drawOrder.size())
		{
			x_end = m_drawPoints[m_drawOrder[0]].x;
			y_end = m_drawPoints[m_drawOrder[0]].y;
		}
		else
		{
			x_end = m_drawPoints[m_drawOrder[i + 1]].x;
			y_end = m_drawPoints[m_drawOrder[i + 1]].y;
		}

		glBegin( GL_LINES );
		{
			glVertex2f(x_start, y_start);
			glVertex2f(x_end, y_end);
		}

		glEnd();

		//glBegin(GL_LINE_LOOP);

		//const float DEG2RAD = 3.14159 / 180;

		//for (int i = 0; i < 360; i++)
		//{
		//	float degInRad = i*DEG2RAD;
		//	glVertex2f(cos(degInRad)*m_radius, sin(degInRad)*m_radius);
		//}

		//glEnd();

		glPopMatrix();
		glPopMatrix();
		glPopMatrix();
	}
}

//----------------------------------------------------------------------------------
Entity::~Entity(void)
{
}
