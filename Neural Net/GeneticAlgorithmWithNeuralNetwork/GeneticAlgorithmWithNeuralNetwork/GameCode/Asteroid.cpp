#include "Asteroid.hpp"

#include <stdlib.h>
#include <windows.h>
#include <gl/gl.h>

#include "Engine/Core/GameCommon.hpp"

unsigned int g_asteroidIndex = 0;

//----------------------------------------------------------------------------------
Asteroid::Asteroid(void)
{
	m_radius = 10.f;
	Entity::Entity();

	m_orientationDegrees = (float) (rand() % 360);

	generateStartingPosition();

	m_velocity = Vec2(1, 0);
	int velocityDirectionDegrees = rand() % 70 + 10;
	int quadrant = rand() % 4 + 1;
	quadrant *= 90;
	velocityDirectionDegrees += quadrant;
	m_velocity = m_velocity.rotateToDegrees((float) velocityDirectionDegrees);

	float velocityScalar = .1f;
	m_velocity.scalarMultiplication(velocityScalar);

	m_index = ++g_asteroidIndex;
}

//----------------------------------------------------------------------------------
Asteroid::Asteroid(float x, float y)
{
	m_radius = 10.f;
	Entity::Entity();

	m_orientationDegrees = (float)(rand() % 360);

	//generateStartingPosition();

	m_velocity = Vec2(1, 0);
	int velocityDirectionDegrees = rand() % 70 + 10;
	int quadrant = rand() % 4 + 1;
	quadrant *= 90;
	velocityDirectionDegrees += quadrant;
	m_velocity = m_velocity.rotateToDegrees((float)velocityDirectionDegrees);

	float velocityScalar = .1f;
	m_velocity.scalarMultiplication(velocityScalar);

	m_position = Vec2(x, y);
	m_index = ++g_asteroidIndex;
}

//----------------------------------------------------------------------------------
void Asteroid::renderEntity()
{
	glColor4f(.48f, .32f, .19f, 1.f);

	glPushMatrix();
	glTranslatef(m_position.x, m_position.y, 0.f);

	//Scale the entity to the proper size
	glPushMatrix();
	glScalef(m_scale, m_scale, m_scale);


	glBegin(GL_LINE_LOOP);

	const float DEG2RAD = 3.14159 / 180;

	for (int i = 0; i < 360; i++)
	{
		float degInRad = i*DEG2RAD;
		glVertex2f(cos(degInRad)*m_radius, sin(degInRad)*m_radius);
	}

	glEnd();

	glColor3f(1.f, 1.f, 1.f);

	glPopMatrix();
	glPopMatrix();

	glColor4f(1.f, 1.f, 1.f, 1.f);
}

//----------------------------------------------------------------------------------
void Asteroid::generateStartingPosition()
{
	Vec2 tileToPixelRatio = Vec2((g_maxScreenX / g_numOfTileUnitsX), (g_maxScreenY / g_numOfTileUnitsX));
	m_position = Vec2(rand() % (int)(g_numOfTileUnitsX), rand() % (int)(g_numOfTileUnitsY));//Vec2(tempX * tileToPixelRatio.x, tempY * tileToPixelRatio.y);
}

//----------------------------------------------------------------------------------
Asteroid::~Asteroid(void)
{
}

//----------------------------------------------------------------------------------
void Asteroid::updateEntity()
{
	Entity::updateEntity();
}
