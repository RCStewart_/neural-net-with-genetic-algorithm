#include "Engine/Math/Vec2.hpp"
#include <vector>

class Ship;
class Asteroid;

class Sensor
{
public:

	Sensor(float orientation, float radius, float distanceFromParent);
	~Sensor();

	void Render( const Ship* parentShip);
	//void Update( const Ship* parentShip);

	void updateCollisions(Ship* parentShip, const std::vector<Asteroid>& asteroids);

	//Zero is no collision, 1.0 is good collision, -1.0 is bad collision
	float m_isColliding;

	//float m_scale;
	float m_radius;
	float m_orientation;
	float m_distanceFromParent;
};