#pragma once
#include "Engine/Font/FontGenerator.hpp"

#include "GeneticAlgorithm/Genome.hpp"
#include "GeneticAlgorithm/GeneticAlgorithm.hpp"
#include "GeneticAlgorithm/GeneticAlgorithmDefines.hpp"
#include "Engine/Renderer/DrawDebug.hpp"
#include "Ship.hpp"
#include "Asteroid.hpp"
#include <string>

class NeuralNetGeneticAlgorithm;

class TheGame
{
public:
	TheGame(void);
	~TheGame(void);

	//Main Loop
	//----------------------------------------------------------------------------------
	void run();
	void runFrame();
	void runMessagePump();
	bool processInput();
	void update( double deltaSeconds );
	void render();

	//Setup
	//----------------------------------------------------------------------------------
	void initializeGame();

	void resetAsteroids();

	DrawDebug m_drawWith;

	FontGenerator* m_font;
	bool m_isQuitting;

	//How much time to spend gathering data
	float m_timePerEpoch;

	//Frames Per Seconds
	float m_fps;
	float m_deltaTime;

	Ship* m_temporaryPlayerShip;
	std::vector<Asteroid> m_asteroids;

	NeuralNetGeneticAlgorithm* m_populationController;
	
	int m_currentIteration;
	std::string m_bestShipFileName;
};

extern TheGame* g_theGame;