#pragma once
#include "Engine/Math/Vec2.hpp"
#include <vector>

class Entity
{
public:
	Entity(void);
	~Entity(void);

	virtual void renderEntity();
	virtual void updateEntity();

	Vec2 m_position;
	float m_orientationDegrees;
	Vec2 m_velocity;
	bool m_isDead;
	float m_radius;
	float m_scale;

	std::vector<Vec2> m_drawPoints;
	std::vector<int> m_drawOrder;
};

