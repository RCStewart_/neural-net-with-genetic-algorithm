#pragma once
#include "Entity.hpp"
#include "Engine/Math/Vec2.hpp"
#include <vector>
#include <set>

class Asteroid;
class Sensor;
class DrawDebug;
class NeuralNet;

class Ship : public Entity
{
public:
	Ship(int generation);
	Ship(float x, float y, int generation);
	Ship(const std::vector<float>& weights, int generation);
	Ship(const Ship* toCopy, int generation);
	~Ship(void);

	void modifyOrientationDegrees(float degrees);
	void forwardAcceleration(float scalarAcceleration);
	virtual void updateEntity();
	virtual void renderEntity();

	bool updateCollisions(const std::vector<Asteroid>& asteroids);
	void randomizeWeights();
	void initialize(int generation);

	void mutate();

	std::vector<Vec2> testingNewPoints;
	std::vector<Sensor*> m_sensors;

	int m_generation;
	double m_timeAlive;
	double m_distanceTraveled;
	double m_startTime;
	double m_fitnessInRelationToGeneration;
	double m_sigmaFitness;
	double m_overallFitness;
	double m_totalSpeed;
	int m_numberOfTimesDied;
	int m_numberOfTurnsAlive;

	Vec2 m_leftOffset;
	Vec2 m_rightOffset;
	Vec2 m_leftForward;
	Vec2 m_rightForward;

	Vec2 m_startPosition;

	NeuralNet* m_net;

	std::set<unsigned int> m_visitedAsteroidIDs;
};

