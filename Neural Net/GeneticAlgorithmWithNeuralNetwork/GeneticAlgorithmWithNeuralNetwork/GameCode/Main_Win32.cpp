#pragma once
#include <math.h>

#include <cassert>
#include <crtdbg.h>
#include "TheGame.hpp"

#include "Engine\Core\GameCommon.hpp"
#include "Engine\Core\EngineCommon.hpp"

#include "Engine/Core/WindowsWrapper.hpp"
//-----------------------------------------------------------------------------------------------
int WINAPI WinMain( HINSTANCE applicationInstanceHandle, HINSTANCE, LPSTR commandLineString, int )
{
	WindowWrapperSetProcessDPIAware();
	UNUSED( commandLineString );

	g_numOfTileUnitsX = 120.f;
	g_numOfTileUnitsY = 70.f;


	WindowWrapperCreateOpenGLOrthoWindow( applicationInstanceHandle );
	WindowWrapperInitialization();
	InitializeAdvancedOpenGLFunctions();

	g_theGame = new TheGame();

	g_theGame->run();
	delete g_theGame;

	WindowsWrapperFindMemoryLeaks();
	return 0;
}