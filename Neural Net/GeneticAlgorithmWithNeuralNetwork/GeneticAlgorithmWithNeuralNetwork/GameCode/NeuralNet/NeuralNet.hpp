#pragma once

#include <vector>
#include "Engine/Math/RandomNumber.hpp"

//----------------------------------------------------------------------------------
struct Neuron
{
	Neuron(int numberOfWeights)
		: m_numberOfWeights(numberOfWeights + 1)
	{
		for (int i = 0; i < (int)m_numberOfWeights; ++i)
		{
			m_weights.push_back(RandomNumber::getRandomFloatInRange(-1.f, 1.f));
		}
	};

	void randomizeWeights()
	{
		for (int i = 0; i < (int)m_numberOfWeights; ++i)
		{
			m_weights[i] = RandomNumber::getRandomFloatInRange(-1.f, 1.f);
		}
	};

	int m_numberOfWeights;
	std::vector<float> m_weights;

};

//----------------------------------------------------------------------------------
struct NeuronLayer
{
	int m_numberOfNeurons;

	std::vector<Neuron> m_neurons;
	NeuronLayer(int numberOfNeurons, int numberOfInputsPerNeuron)
		:m_numberOfNeurons(numberOfNeurons)
	{
		//Push back each neuron for the layer
		for (int neuronIndex = 0; neuronIndex < numberOfNeurons; ++neuronIndex)
		{
			m_neurons.push_back(Neuron(numberOfInputsPerNeuron));
		}
	};
};

//----------------------------------------------------------------------------------
class NeuralNet
{
public:
	NeuralNet( int numberOfInputs, int numberOfOutputs, int numberOfHiddenLayers, int neuronsPerHiddenLayer );
	NeuralNet( int numberOfInputs, int numberOfOutputs, int numberOfHiddenLayers, int neuronsPerHiddenLayer, std::vector<float>& weights);

	void initialize();
	std::vector<float> getWeights()const;
	int getNumberOfWeights()const;
	void replaceWeights(const std::vector<float> &weights);
	std::vector<float> update(std::vector<float> &inputs);

	void splitNetOnNeuronBoundaries(std::vector<std::vector<float>>& outSeparatedNeurons);
	//Generate S-Curve on output for output values that can vary
	float sigmoid(float input);

	void randomizeWeights();

	int m_numberOfInputs;
	int m_numberOfOutputs;
	int m_numberOfHiddenLayers;
	int m_neuronsPerHiddenLayer;
	std::vector<NeuronLayer> m_layers;

}; 
