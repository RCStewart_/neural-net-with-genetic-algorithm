#include "NeuralNet.hpp"

//----------------------------------------------------------------------------------
NeuralNet::NeuralNet(int numberOfInputs, int numberOfOutputs, int numberOfHiddenLayers, int neuronsPerHiddenLayer)
	: m_numberOfInputs(numberOfInputs)
	, m_numberOfOutputs(numberOfOutputs)
	, m_numberOfHiddenLayers(numberOfHiddenLayers)
	, m_neuronsPerHiddenLayer(neuronsPerHiddenLayer)
{
	initialize();
}

//----------------------------------------------------------------------------------
NeuralNet::NeuralNet(int numberOfInputs, int numberOfOutputs, int numberOfHiddenLayers, int neuronsPerHiddenLayer, std::vector<float>& weights)
	: m_numberOfInputs(numberOfInputs)
	, m_numberOfOutputs(numberOfOutputs)
	, m_numberOfHiddenLayers(numberOfHiddenLayers)
	, m_neuronsPerHiddenLayer(neuronsPerHiddenLayer)
{
	initialize();
	replaceWeights(weights);
}

//----------------------------------------------------------------------------------
void NeuralNet::initialize()
{
	//create the layers of the network
	if (m_numberOfHiddenLayers > 0)
	{
		m_layers.push_back(NeuronLayer(m_neuronsPerHiddenLayer, m_numberOfInputs));
		for (int i = 0; i < m_numberOfHiddenLayers - 1; ++i)
		{
			m_layers.push_back(NeuronLayer(m_neuronsPerHiddenLayer, m_neuronsPerHiddenLayer));
		}
		//create output layer
		m_layers.push_back(NeuronLayer(m_numberOfOutputs, m_neuronsPerHiddenLayer));
	}
	else
	{
		//create output layer
		m_layers.push_back(NeuronLayer(m_numberOfOutputs, m_numberOfInputs));
	}
}

//----------------------------------------------------------------------------------
std::vector<float> NeuralNet::getWeights() const
{
	std::vector<float> weightsToReturn;

	for (int layerIndex = 0; layerIndex < (int)m_layers.size(); ++layerIndex)
	{
		for (int neuronIndex = 0; neuronIndex < (int)m_layers[layerIndex].m_neurons.size(); ++neuronIndex)
		{
			for (int weightIndexInNeuron = 0; weightIndexInNeuron < (int) m_layers[layerIndex].m_neurons[neuronIndex].m_weights.size(); ++weightIndexInNeuron)
			{
				weightsToReturn.push_back(m_layers[layerIndex].m_neurons[neuronIndex].m_weights[weightIndexInNeuron]);
			}
		}
	}

	return weightsToReturn;
}

//----------------------------------------------------------------------------------
int NeuralNet::getNumberOfWeights() const
{
	int numberOfWeights = 0;

	for (int layerIndex = 0; layerIndex < (int)m_layers.size(); ++layerIndex)
	{
		for (int neuronIndex = 0; neuronIndex < (int)m_layers[layerIndex].m_neurons.size(); ++neuronIndex)
		{
			numberOfWeights += (int) m_layers[layerIndex].m_neurons[neuronIndex].m_weights.size();
		}
	}

	return numberOfWeights;
}

//----------------------------------------------------------------------------------
void NeuralNet::replaceWeights(const std::vector<float> &weights)
{
	int weightsIndex = 0;

	for (int layerIndex = 0; layerIndex < (int)m_layers.size(); ++layerIndex)
	{
		for (int neuronIndex = 0; neuronIndex < (int)m_layers[layerIndex].m_neurons.size(); ++neuronIndex)
		{
			for (int weightIndexInNeuron = 0; weightIndexInNeuron < (int)m_layers[layerIndex].m_neurons[neuronIndex].m_weights.size(); ++weightIndexInNeuron)
			{
				m_layers[layerIndex].m_neurons[neuronIndex].m_weights[weightIndexInNeuron] = weights[weightsIndex];
				++weightsIndex;
			}
		}
	}
}

//----------------------------------------------------------------------------------
std::vector<float> NeuralNet::update(std::vector<float> &inputs)
{
	std::vector<float> outputs;
	int currentInputsWeightIndex = 0;

	if (inputs.size() != m_numberOfInputs)
	{
		return outputs;
	}

	for (int i = 0; i < m_numberOfHiddenLayers + 1; ++i)
	{
		if (i > 0)
		{
			inputs = outputs;
		}
		outputs.clear();
		currentInputsWeightIndex = 0;

		for (int j = 0; j < m_layers[i].m_numberOfNeurons; ++j)
		{
			float inputSum = 0;
			int numberOfInputs = m_layers[i].m_neurons[j].m_numberOfWeights;
			
			//for each weight
			for (int k = 0; k < numberOfInputs - 1; ++k)
			{
				inputSum += m_layers[i].m_neurons[j].m_weights[k] * inputs[currentInputsWeightIndex++];
			}

			inputSum += m_layers[i].m_neurons[j].m_weights[numberOfInputs - 1] * -1.f;

			outputs.push_back(sigmoid(inputSum));
			currentInputsWeightIndex = 0;
		}
	}
	return outputs;
}


//----------------------------------------------------------------------------------
float NeuralNet::sigmoid(float input)
{
	float e = 2.7183;
	float p = 1.f;

	float a = input;

	return 1.f / (1.f + pow(e, -a));
}

//----------------------------------------------------------------------------------
void NeuralNet::randomizeWeights()
{
	for (int layerIndex = 0; layerIndex < (int)m_layers.size(); ++layerIndex)
	{
		for (int neuronIndex = 0; neuronIndex < (int) m_layers[layerIndex].m_neurons.size(); ++neuronIndex)
		{
			m_layers[layerIndex].m_neurons[neuronIndex].randomizeWeights();
		}
	}
}

//----------------------------------------------------------------------------------------------
void NeuralNet::splitNetOnNeuronBoundaries(std::vector<std::vector<float>>& outSeparatedNeurons)
{
	for (int layerIndex = 0; layerIndex < (int)m_layers.size(); ++layerIndex)
	{
		for (int neuronIndex = 0; neuronIndex < (int)m_layers[layerIndex].m_neurons.size(); ++neuronIndex)
		{
			outSeparatedNeurons.push_back(m_layers[layerIndex].m_neurons[neuronIndex].m_weights);
		}
	}
}